import {
	Component,
	ComponentType,
	Entity,
	World
} from './internal';

/** A system which runs the code for components. */
export abstract class System {
	/** Constructs the system. */
	constructor(world: World) {
		this._world = world;
	}

	/** Destroys the system. */
	destroy(): void {
	}

	protected get world(): World {
		return this._world;
	}

	/** Gets the component write dependencies. */
	get componentWriteDependencies(): readonly ComponentDependency[] {
		return this._componentWriteDependencies;
	}

	/** Gets the component read dependencies. */
	get componentReadDependencies(): readonly ComponentDependency[] {
		return this._componentReadDependencies;
	}

	/** Gets the component type write dependencies. */
	get componentTypeWriteDependencies(): readonly ComponentTypeDependency[] {
		return this._componentTypeWriteDependencies;
	}

	/** Gets the component type read dependencies. */
	get componentTypeReadDependencies(): readonly ComponentTypeDependency[] {
		return this._componentTypeReadDependencies;
	}

	/** Called by the world to update the system. */
	update(_deltaTime: number): void {
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  has been added to an entity. */
	onComponentAdded(_entity: Entity, _component: Component): void {
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  will be removed from an entity. */
	onComponentWillBeRemoved(_entity: Entity, _component: Component): void {
	}

	/** Returns true if this has a read dependency on the given component dependency. */
	hasComponentReadDependency(componentDependency: ComponentDependency): boolean {
		for (let i = 0, l = this._componentReadDependencies.length; i < l; i++) {
			const thisComponentReadDependency = this._componentReadDependencies[i];
			if (thisComponentReadDependency.entity === componentDependency.entity &&
				thisComponentReadDependency.component === componentDependency.component &&
				(thisComponentReadDependency.property === '' || componentDependency.property === '' ||
					thisComponentReadDependency.property === componentDependency.property)) {
				return true;
			}
		}
		for (let i = 0, l = this._componentTypeReadDependencies.length; i < l; i++) {
			const thisComponentTypeReadDependency = this._componentTypeReadDependencies[i];
			if (thisComponentTypeReadDependency.componentType === componentDependency.component.constructor &&
				(thisComponentTypeReadDependency.property === '' || componentDependency.property === '' ||
				thisComponentTypeReadDependency.property === componentDependency.property)) {
				return true;
			}
		}
		return false;
	}

	/** Returns true if this has a read dependency on the given component dependency. */
	hasComponentTypeReadDependency(componentTypeDependency: ComponentTypeDependency): boolean {
		for (let i = 0, l = this._componentReadDependencies.length; i < l; i++) {
			const thisComponentReadDependency = this._componentReadDependencies[i];
			if (thisComponentReadDependency.component.constructor === componentTypeDependency.componentType &&
				(thisComponentReadDependency.property === '' || componentTypeDependency.property === '' ||
					thisComponentReadDependency.property === componentTypeDependency.property)) {
				return true;
			}
		}
		for (let i = 0, l = this._componentTypeReadDependencies.length; i < l; i++) {
			const thisComponentTypeReadDependency = this._componentTypeReadDependencies[i];
			if (thisComponentTypeReadDependency.componentType === componentTypeDependency.componentType &&
				(thisComponentTypeReadDependency.property === '' || componentTypeDependency.property === '' ||
				thisComponentTypeReadDependency.property === componentTypeDependency.property)) {
				return true;
			}
		}
		return false;
	}

	/** Add a component dependency. */
	protected addComponentDependency(entity: Entity, component: Component, property: string | undefined, write: boolean): void {
		// Create the dependency object.
		const componentDependency = { entity, component, property };

		// Add it to the appropriate list.
		const list = write ? this._componentWriteDependencies : this._componentReadDependencies;
		list.push(componentDependency);
	}

	/** Remove a component dependency. */
	protected removeComponentDependency(entity: Entity, component: Component, property: string | undefined, write: boolean): void {
		// Go through each dependency and when a match is found, splice the component out.
		const list = write ? this._componentWriteDependencies : this._componentReadDependencies;
		for (let i = 0, l = list.length; i < l; i++) {
			const componentDependency = list[i];
			if (componentDependency.entity === entity && componentDependency.component === component && componentDependency.property === property) {
				list.splice(i, 1);
				return;
			}
		}
	}

	/** Add a component type dependency. */
	protected addComponentTypeDependency(componentType: ComponentType, property: string | undefined, write: boolean): void {
		// Create the dependency object.
		const componentTypeDependency = { componentType, property };

		// Add it to the appropriate list.
		const list = write ? this._componentTypeWriteDependencies : this._componentTypeReadDependencies;
		list.push(componentTypeDependency);
	}

	/** Remove a component type dependency. */
	protected removeComponentTypeDependency(componentType: ComponentType, property: string | undefined, write: boolean): void {
		// Go through each dependency and when a match is found, splice the component type out.
		const list = write ? this._componentTypeWriteDependencies : this._componentTypeReadDependencies;
		for (let i = 0, l = list.length; i < l; i++) {
			const componentDependency = list[i];
			if (componentDependency.componentType === componentType && componentDependency.property === property) {
				list.splice(i, 1);
				return;
			}
		}
	}

	/** The world. */
	private _world: World;

	/** The component write dependencies. */
	private _componentWriteDependencies: ComponentDependency[] = [];

	/** The component read dependencies. */
	private _componentReadDependencies: ComponentDependency[] = [];

	/** The component type write dependencies. */
	private _componentTypeWriteDependencies: ComponentTypeDependency[] = [];

	/** The component type read dependencies. */
	private _componentTypeReadDependencies: ComponentTypeDependency[] = [];
}

interface ComponentDependency {
	entity: Entity,
	component: Component,
	property: string | undefined
}

interface ComponentTypeDependency {
	componentType: ComponentType,
	property: string | undefined
}
