import {
	Engine,
	ItemList,
	ItemPool,
	Render
} from '../internal';
import {
	Component,
	ComponentType,
	Entity,
	System
} from './internal';

/** A world as an ECS framework. */
export class World {
	/** Constructor. */
	constructor(engine: Engine) {
		// Set the engine that contains this.
		this._engine = engine;

		// Create the render scene.
		this._scene = this._engine.render.scenes.getNew();

		// Setup the entities.
		this._entities = new ItemPool<Entity>(() => new Entity(this), (entity: Entity) => entity.destroy());

		// Setup the systems.
		this._systems = new ItemPool<System>((systemConstructor?: new (world: World) => System) => {
			if (systemConstructor !== undefined) {
				return new systemConstructor(this);
			}
			else {
				throw new Error('A subclass of System must be provided.');
			}
		}, (system: System) => {
			system.destroy();
		});

		// Setup the system component type subscriptions.
		this._systemsSubscribedToComponentTypes = new Map();
	}

	/** Destroys this. */
	destroy(): void {
		// Destroy the systems.
		for (let i = 0, l = this._systems.array.length; i < l; i++) {
			this._systems.array[i].destroy();
		}

		// Destroy the entities.
		for (let i = 0, l = this._entities.array.length; i < l; i++) {
			this._entities.array[i].destroy();
		}

		// Release the render scene.
		this._engine.render.scenes.release(this._scene);
	}

	/** Updates the world. Called once per frame. */
	update(deltaTime: number): void {
		// Sort
		this._systems.sort((a: System, b: System) => {
			// If they are the same, return false.
			if (a === b) {
				return false;
			}

			// If any of a's reads match any of b's writes, return true.
			const bWrites = b.componentWriteDependencies;
			for (let i = 0, l = bWrites.length; i < l; i++) {
				if (a.hasComponentReadDependency(bWrites[i])) {
					return true;
				}
			}

			// Otherwise return false.
			return false;
		});
		// Update the systems.
		const systemsArray = this._systems.array;
		for (let i = 0, l = systemsArray.length; i < l; i++) {
			systemsArray[i].update(deltaTime);
		}
	}

	/** Gets the engine that contains this. */
	get engine(): Engine {
		return this._engine;
	}

	/** Gets the scene. */
	get scene(): Render.Scene {
		return this._scene;
	}

	/** Gets the entities. */
	get entities(): ItemPool<Entity> {
		return this._entities;
	}

	/** Gets the systems. */
	get systems(): ItemPool<System> {
		return this._systems;
	}

	/** Subscribe to a component type. */
	subscribeToComponentType<Type extends Component>(system: System, componentType: ComponentType<Type>): void {
		// Get the list of subscribed systems, creating it if needed.
		let systems = this._systemsSubscribedToComponentTypes.get(componentType);
		if (systems === undefined) {
			systems = new ItemList();
			this._systemsSubscribedToComponentTypes.set(componentType, systems);
		}

		// Add the system to the list.
		if (!systems.has(system)) {
			systems.add(system);
		}

		// Let the system know about any components that already exist.
		for (let i = 0, l = this._entities.array.length; i < l; i++) {
			const entity = this._entities.array[i];
			const component = entity.getComponent(componentType);
			if (component !== undefined) {
				system.onComponentAdded(entity, component);
			}
		}
	}

	/** Unsubscribe from a component type. */
	unsubscribeToComponentType(system: System, componentType: ComponentType): void {
		// Get the list of subscribed systems, erroring if it doesn't exist.
		const systems = this._systemsSubscribedToComponentTypes.get(componentType);
		if (systems === undefined || !systems.has(system)) {
			throw new Error(`The system "${system.constructor.name}" is not subscribed to the component type "${componentType.name}".`);
		}

		// Remove the system from the list.
		systems.remove(system);

		// If the list is empty, remove it from the main subscribed systems mapping.
		if (systems.length === 0) {
			this._systemsSubscribedToComponentTypes.delete(componentType);
		}
	}

	/** Called by entity when a component has been added. @internal */
	onComponentAdded(entity: Entity, componentType: ComponentType): void {
		const systemList = this._systemsSubscribedToComponentTypes.get(componentType);
		if (systemList !== undefined) {
			const array = systemList.array;
			for (let i = 0, l = array.length; i < l; i++) {
				array[i].onComponentAdded(entity, entity.getComponent(componentType));
			}
		}
	}

	/** Called by entity when a component will be removed. @internal */
	onComponentWillBeRemoved(entity: Entity, componentType: ComponentType): void {
		const systemList = this._systemsSubscribedToComponentTypes.get(componentType);
		if (systemList !== undefined) {
			const array = systemList.array;
			for (let i = 0, l = array.length; i < l; i++) {
				array[i].onComponentWillBeRemoved(entity, entity.getComponent(componentType));
			}
		}
	}

	/** The entities. */
	private _entities: ItemPool<Entity>;

	/** The systems. */
	private _systems: ItemPool<System>;

	/** Systems subscribed to component type adds and removes. */
	private _systemsSubscribedToComponentTypes: Map<ComponentType, ItemList<System>>;

	/** The render scene for the world. */
	private _scene: Render.Scene;

	/** The engine that contains this. */
	private _engine: Engine;
}
