import {
	Component,
	Camera,
	System,
	World
} from '../internal';
import {
	ItemList,
	Transforms
} from '../../internal';
import { Entity } from '../entity';

export class UpdateCameraMatrices extends System {
	constructor(world: World) {
		super(world);

		// Setup the list of camera components.
		this._cameraComponents = new ItemList();

		// Add the read and write dependencies.
		this.addComponentTypeDependency(Camera, 'fov', false);
		this.addComponentTypeDependency(Camera, 'aspectRatio', false);
		this.addComponentTypeDependency(Camera, 'near', false);
		this.addComponentTypeDependency(Camera, 'far', false);
		this.addComponentTypeDependency(Camera, 'localToNDC', true);
		this.addComponentTypeDependency(Camera, 'ndcToLocal', true);

		// Subscribe the camera components.
		this.world.subscribeToComponentType(this, Camera);
	}

	/** Destroys the system. */
	override destroy(): void {
		// Unsubscribe from camera components.
		this.world.unsubscribeToComponentType(this, Camera);
	}

	/** Updates the system. */
	override update(): void {
		const array = this._cameraComponents.array;
		for (let i = 0, l = array.length; i < l; i++) {
			const camera = array[i];
			Transforms.localToNDCPerspective(camera.localToNDC, camera.fov, camera.aspectRatio, camera.near, camera.far, true);
			Transforms.ndcToLocalPerspective(camera.ndcToLocal, camera.fov, camera.aspectRatio, camera.near, camera.far, true);
		}
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  has been added to an entity. */
	override onComponentAdded(_entity: Entity, component: Component): void {
		this._cameraComponents.add(component as Camera);
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  will be removed from an entity. */
	override onComponentWillBeRemoved(_entity: Entity, component: Component): void {
		this._cameraComponents.remove(component as Camera);
	}

	/** The list of camera components. */
	private _cameraComponents: ItemList<Camera>;
}
