import {
	Component,
	Frame,
	System,
	World
} from '../internal';
import {
	ItemList,
	Transforms
} from '../../internal';
import { Entity } from '../entity';

export class UpdateFrameMatrices extends System {
	constructor(world: World) {
		super(world);

		// Setup the list of frame components.
		this._frameComponents = new ItemList();

		// Add the read and write dependencies.
		this.addComponentTypeDependency(Frame, 'position', false);
		this.addComponentTypeDependency(Frame, 'orientaton', false);
		this.addComponentTypeDependency(Frame, 'localToWorld', true);
		this.addComponentTypeDependency(Frame, 'worldToLocal', true);

		// Subscribe the frame components.
		this.world.subscribeToComponentType(this, Frame);
	}

	/** Destroys the system. */
	override destroy(): void {
		// Unsubscribe from frame components.
		this.world.unsubscribeToComponentType(this, Frame);
	}

	/** Updates the system. */
	override update(): void {
		const array = this._frameComponents.array;
		for (let i = 0, l = array.length; i < l; i++) {
			const frame = array[i];
			Transforms.localToWorld(frame.localToWorld, frame.position, frame.orientation);
			Transforms.worldToLocal(frame.worldToLocal, frame.position, frame.orientation);
		}
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  has been added to an entity. */
	override onComponentAdded(_entity: Entity, component: Component): void {
		this._frameComponents.add(component as Frame);
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  will be removed from an entity. */
	override onComponentWillBeRemoved(_entity: Entity, component: Component): void {
		this._frameComponents.remove(component as Frame);
	}

	/** The list of frame components. */
	private _frameComponents: ItemList<Frame>;
}
