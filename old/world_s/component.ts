/** A component, which can be anything with a set of string keys. */
export type Component = { [key: string]: any };

/** A type of a component. */
export type ComponentType<ComponentType extends Component = Component> = new () => ComponentType;
