import {
	Mat44,
	Quat,
	Vec3D
} from '../../internal';

export class Frame {
	position = new Vec3D();
	orientation = new Quat();
	localToWorld = new Mat44();
	worldToLocal = new Mat44();
}
