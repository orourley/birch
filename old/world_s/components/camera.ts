import {
	Mat44
} from '../../internal';

export class Camera {
	aspectRatio = 1;
	near = 1;
	far = 2;
	fov = 90;
	localToNDC = new Mat44();
	ndcToLocal = new Mat44();
}
