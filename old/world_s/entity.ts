import {
	Component,
	ComponentType,
	World
} from './internal';

export class Entity {
	/** Constructs the entity. */
	constructor(world: World) {
		// Save the world.
		this._world = world;
	}

	/** Destroys the entity. */
	destroy(): void {
		// Notify the world that these components are being removed.
		for (const componentName of Object.keys(this._components)) {
			this._world.onComponentWillBeRemoved(this, this._components[componentName].constructor as ComponentType);
		}
	}

	/** Gets a component of the given type. */
	getComponent<Type extends Component>(componentType: ComponentType<Type>): Type {
		return this._components[componentType.name] as Type;
	}

	/** Adds a component. */
	addComponent<Type extends Component>(componentType: ComponentType<Type>): Type {
		// Check if doesn't already exist.
		if (this._components[componentType.name] !== undefined) {
			throw new Error(`A component with type "${componentType.name}" already exists in the entity.`);
		}

		// Create and add the component.
		const component = new componentType();
		this._components[componentType.name] = component;

		// Notify the world that the new component has been added.
		this._world.onComponentAdded(this, componentType);

		// Return the component.
		return component;
	}

	/** Removes a component. */
	removeComponent(componentType: ComponentType): void {
		// Check that it exists.
		if (this._components[componentType.name] === undefined) {
			throw new Error(`No component with type "${componentType.name}" exists in the entity.`);
		}

		// Notify the world that the new component will be removed.
		this._world.onComponentWillBeRemoved(this, componentType);

		// Remove the component.
		delete this._components[componentType.name];
	}

	/** The world. */
	private _world: World;

	/** The components. */
	private _components: { [componentName: string]: Component } = {};
}
