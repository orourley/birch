import { Component, Entity } from '../internal';
import { Render } from '../../internal';

/** A simple component that contains an accessible model. */
export class ModelComponent extends Component {
	constructor(entity: Entity) {
		super(entity);

		// Add the model in the scene.
		this._model = this.entity.world.scene.models.getNew();
	}

	/** Destroys the model component. */
	override destroy(): void {
		// Remove the model from the scene.
		this.entity.world.scene.models.release(this._model);

		super.destroy();
	}

	/** Gets the model. */
	get model(): Render.Model {
		return this._model;
	}

	/** Gets the uniform group of the model. */
	get uniformGroup(): Render.UniformGroup {
		return this._model.uniformGroup;
	}

	/** The model. */
	private _model: Render.Model;
}
