import {
	Entity, System
} from './internal';

/** A type of a component. */
export type ComponentType<ComponentType extends Component = Component> = new (...args: any) => ComponentType;

/** A component. */
export class Component {

	/** The constructor. */
	constructor(entity: Entity, type: ComponentType, name: string) {
		this.entity = entity;
		this.type = type;
		this.name = name;
		this.changedSubscriptions = new Map();
	}

	/** Gets the entity that contains this. */
	getEntity(): Entity {
		return this.entity;
	}

	/** Gets the type. */
	getType(): ComponentType {
		return this.type;
	}

	/** Gets the name. */
	getName(): string {
		return this.name;
	}

	/** Called by subcomponents to notify subscribed systems when the component has changed. */
	protected sendChanged(property: number): void {
		const systems = this.changedSubscriptions.get(property);
		if (systems !== undefined) {
			for (let i = 0, l = systems.length; i < l; i++) {
				systems[i].__processComponentChangedEvent(this, property);
			}
		}
	}

	/** Called by System. Adds a system's subscription. */
	__addChangedSubscription(system: System, property: number): void {
		let systems = this.changedSubscriptions.get(property);
		if (systems === undefined) {
			systems = [];
			this.changedSubscriptions.set(property, systems);
		}
		systems.push(system);
	}

	/** Called by System. Removes a system's subscription. */
	__removeChangedSubscription(system: System, property: number): void {
		const systems = this.changedSubscriptions.get(property);
		if (systems !== undefined) {
			for (let i = 0, l = systems.length; i < l; i++) {
				if (systems[i] === system) {
					systems.splice(i, 1);
					if (systems.length === 0) {
						this.changedSubscriptions.delete(property);
					}
					break;
				}
			}
		}
	}

	/** The entity that contains this. */
	private entity: Entity;

	/** The type. */
	private type: ComponentType;

	/** The name. */
	private name: string;

	/** The properties to system subscriptions for when the component properties change. */
	private changedSubscriptions: Map<number, System[]>;
}
