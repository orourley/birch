import { Engine, ItemPool, Render } from '../../src/internal';
import { NamedTypedCollection } from '../../src/utils/named_typed_collection';
import { Component, ComponentType, Entity, Stage, System, SystemType } from './internal';

/** The world. */
export class World {

	/** Constructor. */
	constructor(engine: Engine) {

		// Set the engine that contains this.
		this.engine = engine;

		// Create the render scene.
		this.scene = this.engine.render.scenes.getNew();

		// Setup the entities.
		this.entities = new ItemPool<Entity>(() => new Entity(this), (entity: Entity) => entity.clearComponents());

		// Setup the entities to be removed list.
		this.entitiesToBeRemoved = new Set();

		// Setup the root stage.
		this.rootStage = new Stage('root');

		// Setup the systems.
		this.systems = new NamedTypedCollection('system');

		// Setup the systems to be removed list.
		this.systemsToBeRemoved = new Set();

		// Setup the component-type subscriptions.
		this.componentTypeSubscriptions = new Map();
	}

	/** Destroys this. */
	destroy(): void {

		// Clear the systems.
		this.systems.clear();

		// Destroy the entities.
		this.entities.destroy();

		// Release the render scene.
		this.engine.render.scenes.release(this.scene);
	}

	/** Adds an entity and returns it. */
	addEntity(): Entity {
		return this.entities.getNew();
	}

	/** Schedules an entity to be removed at the beginning of the next frame. */
	removeEntity(entity: Entity): void {
		this.entitiesToBeRemoved.add(entity);
	}

	/** Gets the root stage. */
	getRootStage(): Stage {
		return this.rootStage;
	}

	/** Gets the system of the given name. */
	getSystemByName<Type extends System>(name: string): Type | undefined {
		return this.systems.getItemByName(name);
	}

	/** Gets the systems of the given type. */
	getSystemsByType<Type extends System>(systemType: SystemType<Type>): ReadonlyArray<Type> | undefined {
		return this.systems.getItemsByType(systemType);
	}

	/** Adds a system. */
	addSystem<Type extends System>(systemType: SystemType<Type>, name?: string): Type {

		// Add the system.
		return this.systems.add(systemType, this, name);
	}

	/** Schedules a system to be removed at the beginning of the next frame. */
	removeSystem(system: System): void {
		this.systemsToBeRemoved.add(system);
	}

	/** Sends an event to subscribed systems for when a component of a specific type was added.
	 *  Only called by Entity. */
	sendComponentWasAddedEvent(entity: Entity, component: Component): void {
		const componentType = component.constructor as ComponentType;
		const systems = this.componentTypeSubscriptions.get(componentType);
		if (systems !== undefined) {
			for (let i = 0, l = systems.length; i < l; i++) {
				systems[i].__processComponentWasAddedEvent(entity, component);
			}
		}
	}

	/** Sends an event to subscribed systems for when a component of a specific type will be removed.
	 *  Only called by Entity. */
	sendComponentWillBeRemovedEvent(entity: Entity, component: Component): void {
		const componentType = component.constructor as ComponentType;
		const systems = this.componentTypeSubscriptions.get(componentType);
		if (systems !== undefined) {
			for (let i = 0, l = systems.length; i < l; i++) {
				systems[i].__processComponentWillBeRemovedEvent(entity, component);
			}
		}
	}

	/** Called only by System. Adds a system's subscription for component types being added and removed. */
	__addComponentTypeSubscription(system: System, componentType: ComponentType): void {
		let systems = this.componentTypeSubscriptions.get(componentType);
		if (systems === undefined) {
			systems = [];
			this.componentTypeSubscriptions.set(componentType, systems);
		}
		systems.push(system);
	}

	/** Called only by System. Removes a system's subscription for component types being added and removed. */
	__removeComponentTypeSubscription(system: System, componentType: ComponentType): void {
		const systems = this.componentTypeSubscriptions.get(componentType);
		if (systems !== undefined) {
			for (let i = 0, l = systems.length; i < l; i++) {
				if (systems[i] === system) {
					systems.splice(i, 1);
					if (systems.length === 0) {
						this.componentTypeSubscriptions.delete(componentType);
					}
					break;
				}
			}
		}
	}

	/** Updates the world. Called once per frame. */
	update(): void {

		// Remove any entities that are scheduled to be removed.
		if (this.entitiesToBeRemoved.size > 0) {
			for (const entity of this.entitiesToBeRemoved) {
				this.entities.release(entity);
			}
			this.entitiesToBeRemoved.clear();
		}

		// Remove any systems that are scheduled to be removed.
		if (this.systemsToBeRemoved.size > 0) {
			for (const system of this.systemsToBeRemoved) {
				this.systems.remove(system);
				system.destroy();
			}
			this.systemsToBeRemoved.clear();
		}

		// Update the stages.
		this.rootStage.update();
	}

	/** The render scene for the world. */
	private scene: Render.Scene;

	/** The engine that contains this. */
	private engine: Engine;

	/** The entities. */
	private entities: ItemPool<Entity>;

	/** The entities to be removed. This happens at the beginning of the frame. */
	private entitiesToBeRemoved: Set<Entity>;

	/** The stages. */
	private rootStage: Stage;

	/** The systems. */
	private systems: NamedTypedCollection<World, System>;

	/** The systems to be removed. This happens at the beginning of the frame. */
	private systemsToBeRemoved: Set<System>;

	/** The system subscriptions for when a component is added or removed. */
	private componentTypeSubscriptions: Map<ComponentType, System[]>;
}
