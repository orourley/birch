import {
	Component,
	FrameComponent,
	System,
	World
} from '../internal';
import {
	ItemList,
	Transforms
} from '../../../src/internal';
import { Entity } from '../entity';

export class UpdateFrameMatrices extends System {

	constructor(world: World) {
		super(world, UpdateFrameMatrices, 'updateFrameMatrices');

		// Setup the list of frame components.
		this._frameComponents = new ItemList();

		// Subscribe to frame components.
		this.addComponentTypeSubscription(FrameComponent);
	}

	/** Destroys the system. */
	override destroy(): void {

		// Call super.
		super.destroy();
	}

	/** Called by world to process a component-changed event. */
	override __processComponentChangedEvent(_component: Component, property: number): void {
		// TODO: I need to add the component to the changed list if the property is Position or Orientation.
		//   I'd like it to happen without garbage collection.
		//   Options: Pool array for adding,
	}

	/** Called by world to process a component-was-added event. */
	override __processComponentWasAddedEvent(_entity: Entity, component: Component): void {
		this.addComponentChangedSubscription(component, FrameComponent.Property.Position);
		this.addComponentChangedSubscription(component, FrameComponent.Property.Orientation);
		this._frameComponents.add(component as FrameComponent);
	}

	/** Called by world to a process a component-will-be-removed event. */
	override __processComponentWillBeRemovedEvent(_entity: Entity, component: Component): void {
		this.removeComponentChangedSubscription(component, FrameComponent.Property.Position);
		this.removeComponentChangedSubscription(component, FrameComponent.Property.Orientation);
		this._frameComponents.remove(component as FrameComponent);

		// TODO: I need to find the component in the changed list and remove it.
	}

	/** Updates the system. */
	override update(): void {
		const array = this._frameComponents.array;
		for (let i = 0, l = array.length; i < l; i++) {
			const frame = array[i];
			Transforms.localToWorld(frame.localToWorld, frame.position, frame.orientation);
			Transforms.worldToLocal(frame.worldToLocal, frame.position, frame.orientation);
		}
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  has been added to an entity. */
	override onComponentAdded(_entity: Entity, component: Component): void {
	}

	/** Called by world when a component of a type that this system is subscribed to
	 *  will be removed from an entity. */
	override onComponentWillBeRemoved(_entity: Entity, component: Component): void {
	}

	/** The list of frame components. */
	private _frameComponents: ItemList<FrameComponent>;

	/** The list of changed frame components. */
	private _changedFrameComponents: ItemList<FrameComponent>;
}
