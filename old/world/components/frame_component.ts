import { Mat44, Mat44ReadOnly, Quat, QuatReadOnly, Vec3D, Vec3DReadOnly } from '../../../src/internal';
import { Component } from '../internal';

export class FrameComponent extends Component {

	/** Gets the position. */
	getPosition(): Vec3DReadOnly {
		return this.position;
	}

	/** Sets the position. */
	setPosition(position: Vec3DReadOnly): void {
		this.position.copy(position);
		this.sendChanged(FrameComponent.Property.Position);
	}

	/** Gets the orientation. */
	getOrientation(): QuatReadOnly {
		return this.orientation;
	}

	/** Sets the orientation. */
	setOrientation(orientation: QuatReadOnly): void {
		this.orientation.copy(orientation);
		this.sendChanged(FrameComponent.Property.Orientation);
	}

	/** Gets the local-to-world matrix. */
	getLocalToWorld(): Mat44ReadOnly {
		return this.localToWorld;
	}

	/** Sets the local-to-world matrix. */
	setLocalToWorld(localToWorld: Mat44ReadOnly): void {
		this.localToWorld.copy(localToWorld);
		this.sendChanged(FrameComponent.Property.LocalToWorld);
	}

	/** Gets the world-to-local matrix. */
	getWorldToLocal(): Mat44ReadOnly {
		return this.worldToLocal;
	}

	/** Sets the world-to-local matrix. */
	setWorldToLocal(worldToLocal: Mat44ReadOnly): void {
		this.worldToLocal.copy(worldToLocal);
		this.sendChanged(FrameComponent.Property.WorldToLocal);
	}

	/** The position. */
	private position = new Vec3D();

	/** The orientation. */
	private orientation = new Quat();

	/** The local-to-world matrix. */
	private localToWorld = new Mat44();

	/** The world-to-local matrix. */
	private worldToLocal = new Mat44();
}

export namespace FrameComponent {
	export enum Property {
		Position,
		Orientation,
		LocalToWorld,
		WorldToLocal
	}
}
