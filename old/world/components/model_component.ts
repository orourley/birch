import { Component } from '../internal';

export class ModelComponent extends Component {

	/** Gets the model. */
	getModel(): string {
		return this.model;
	}

	/** Sets the model. */
	setModel(model: string): void {
		this.model = model;
		this.sendChanged();
	}

	/** The model. */
	private model: string = '';
}
