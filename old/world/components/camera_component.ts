import { Component } from '../internal';

export class CameraComponent extends Component {

	/** Gets the aspect ratio. */
	getAspectRatio(): number {
		return this.aspectRatio;
	}

	/** Sets the aspect ratio. */
	setAspectRatio(aspectRatio: number): void {
		this.aspectRatio = aspectRatio;
		this.sendChanged();
	}

	/** Gets the near plane distance. */
	getNear(): number {
		return this.near;
	}

	/** Sets the near plane distance. */
	setNear(near: number): void {
		this.near = near;
		this.sendChanged();
	}

	/** Gets the far plane distance. */
	getFar(): number {
		return this.far;
	}

	/** Sets the far plane distance. */
	setFar(far: number): void {
		this.far = far;
		this.sendChanged();
	}

	/** Gets the field of view. */
	getFov(): number {
		return this.fov;
	}

	/** Sets the field of view. */
	setFov(fov: number): void {
		this.fov = fov;
		this.sendChanged();
	}

	/** The aspect ratio. */
	private aspectRatio = 1;

	/** The near plane distance. */
	private near = 1;

	/** The far plane distance. */
	private far = 2;

	/** The field of view. */
	private fov = 90;
}
