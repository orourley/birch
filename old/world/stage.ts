import { System } from './internal';

/** A hierarchy of stages that contain the update order of systems. */
export class Stage {

	/** Constructs the stage. */
	constructor(name: string) {
		this.name = name;
	}

	/** Gets the name. */
	getName(): string {
		return this.name;
	}

	/** Gets the parent, or undefined if it is a root. */
	getParent(): Stage | undefined {
		return this.parent;
	}

	/** Gets the children. */
	getChildren(): readonly Stage[] {
		return this.children;
	}

	/** Gets the systems. */
	getSystems(): readonly System[] {
		return this.systems;
	}

	/** Adds a child stage. The name must be unique from the other children. */
	addChildStage(name: string, before: Stage): Stage {
		if (this.systems.length > 0) {
			throw new Error(`Cannot add the child stage ${name} to the stage ${this.name} because it already has systems.`);
		}
		for (let i = 0, l = this.children.length; i < l; i++) {
			if (name === this.children[i].name) {
				throw new Error(`Cannot add the child stage ${name} to the stage ${this.name} because it already has a child with the same name.`);
			}
		}
		for (let i = 0, l = this.children.length; i < l; i++) {
			if (before === this.children[i]) {
				this.children.splice(i, 0, new Stage(name));
				return this.children[i];
			}
		}
		throw new Error(`Cannot add the child stage to the stage ${this.name} because the before stage ${before.name} is not a child of the stage ${this.name}.`);
	}

	/** Removes a stage. */
	removeChildStage(stage: Stage): void {
		for (let i = 0, l = this.children.length; i < l; i++) {
			if (stage === this.children[i]) {
				this.children.splice(i, 1);
				return;
			}
		}
		throw new Error(`Cannot remove the child stage from the stage ${this.name} because it is not a child stage.`);
	}

	/** Called only by System. Adds a system. The name must be unique from the other systems. */
	__addSystem(system: System, before: System): void {
		if (this.systems === undefined) {
			throw new Error(`Cannot add the system ${system.getName()} to the stage ${this.name} because the stage has children.`);
		}
		for (let i = 0, l = this.systems.length; i < l; i++) {
			if (system === this.systems[i]) {
				throw new Error(`Cannot add the system ${system.getName()} to the stage ${this.name} because it already has the system.`);
			}
		}
		for (let i = 0, l = this.systems.length; i < l; i++) {
			if (before === this.systems[i]) {
				this.systems.splice(i, 0, before);
				return;
			}
		}
		throw new Error(`Cannot add the system ${system.getName()} to the stage ${this.name} because the before system ${before.getName()} is not a system in this stage.`);
	}

	/** Called only by System. Removes a system. */
	__removeSystem(system: System): void {
		for (let i = 0, l = this.systems.length; i < l; i++) {
			if (system === this.systems[i]) {
				this.systems.splice(i, 1);
				return;
			}
		}
		throw new Error(`Cannot remove the system ${system.getName()} from the stage ${this.name} because it is not a system in this stage.`);
	}

	/** Updates the systems, recursively. Children run before systems. */
	update(): void {
		if (this.children !== undefined) {
			for (let i = 0, l = this.children.length; i < l; i++) {
				this.children[i].update();
			}
		}
		else {
			for (let i = 0, l = this.systems!.length; i < l; i++) {
				this.systems![i].update();
			}
		}
	}

	/** The name. */
	private name: string;

	/** The parent stage. */
	private parent: Stage | undefined;

	/** The child stages. */
	private children: Stage[] = [];

	/** The systems to run. */
	private systems: System[] = [];
}
