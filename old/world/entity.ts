import { NamedTypedCollection } from '../../src/utils/named_typed_collection';
import {
	Component,
	ComponentType,
	World
} from './internal';

export class Entity {
	/** Constructs the entity. */
	constructor(world: World) {

		// Set the world that contains this.
		this.world = world;

		// Setup the components.
		this.components = new NamedTypedCollection('component');
	}

	/** Gets the world that contains this. */
	getWorld(): World {
		return this.world;
	}

	/** Gets the component of the given name. */
	getComponentByName<Type extends Component>(name: string): Type | undefined {
		return this.components.getItemByName(name);
	}

	/** Gets the components of the given type. */
	getComponentsByType<Type extends Component>(componentType: ComponentType<Type>): ReadonlyArray<Type> | undefined {
		return this.components.getItemsByType(componentType);
	}

	/** Adds a component. */
	addComponent<Type extends Component>(componentType: ComponentType<Type>, name?: string): Type {

		// Add the component.
		const component = this.components.add(componentType, this, name);

		// Notify the world that the new component has been added.
		this.world.sendComponentWasAddedEvent(this, component);

		// Return the component.
		return component;
	}

	/** Removes a component. */
	removeComponent(component: Component): void {

		I NEED TO MAKE THIS CALL SOMETHING IN WORLD
		IN WORLD THERE NEEDS TO BE A REMOVE COMPONENTS LOOP
		  THAT REMOVES ALL COMPONENTS AT THE BEGINNING OF THE FRAME.
		THEN NO SYSTEM WILL HAVE TO WORRY ABOUT THE CHANGED COMPONENTS LISTS
		  CONTAINING REMOVED ELEMENTS, SINCE THEY ARE CLEAR AT THE BEGINNING OF EACH FRAME.
		THIS ALSO MEANS I POSSIBLY NEED TO STANDARDIZE THE CHANGED ELEMENTS LISTS.

		// Notify the world that the component will be removed.
		if (this.components.has(component)) {
			this.world.sendComponentWillBeRemovedEvent(this, component);
		}

		// Remove the component.
		this.components.remove(component);
	}

	/** Clears all of the components. */
	clearComponents(): void {

		// Notify the world that these components are being removed.
		for (const component of this.components.getAllItemsIterator()) {
			this.world.sendComponentWillBeRemovedEvent(this, component);
		}

		// Clear the map.
		this.components.clear();
	}

	/** The world that contains this. */
	private world: World;

	/** The component name-type map. */
	private components: NamedTypedCollection<Entity, Component>;
}
