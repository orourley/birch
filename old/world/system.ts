import { Component, ComponentType, Entity, Stage, World } from './internal';

/** A type of a system. */
export type SystemType<SystemType extends System = System> = new (...args: any) => SystemType;

export class System {

	/** Constructor. */
	constructor(world: World, type: SystemType, name: string) {
		this.world = world;
		this.type = type;
		this.name = name;
		this.componentTypeSubscriptions = new Set();
		this.componentChangedSubscriptions = new Map();
	}

	/** Destroys the system. */
	destroy(): void {

		// Remove any component property changed subscriptions.
		for (const [component, properties] of this.componentChangedSubscriptions) {
			for (const property of properties) {
				component.__removeChangedSubscription(this, property);
			}
		}

		// Remove any component type subscriptions.
		for (const componentType of this.componentTypeSubscriptions) {
			this.world.__removeComponentTypeSubscription(this, componentType);
		}

		// Remove the stage, if any.
		if (this.stage !== undefined) {
			this.stage.__removeSystem(this);
		}
	}

	/** Gets the world that contains this. */
	getWorld(): World {
		return this.world;
	}

	/** Gets the type. */
	getType(): SystemType {
		return this.type;
	}

	/** Gets the name. */
	getName(): string {
		return this.name;
	}

	/** Gets the stage in which this updates. */
	getStage(): Stage | undefined {
		return this.stage;
	}

	/** Sets the stage in which this updates. Undefined removes it from any stage. */
	setStage(stage: Stage, before: System): void {
		if (this.stage !== undefined) {
			this.stage.__removeSystem(this);
		}
		this.stage = stage;
		if (this.stage !== undefined) {
			this.stage.__addSystem(this, before);
		}
	}

	/** Adds a subscription for component types being added and removed. */
	addComponentTypeSubscription(componentType: ComponentType): void {

		// If it isn't already subscribed,
		if (!this.componentTypeSubscriptions.has(componentType)) {

			// Call the world's add function.
			this.world.__addComponentTypeSubscription(this, componentType);

			// Add it to this component type subscriptions list.
			this.componentTypeSubscriptions.add(componentType);
		}
	}

	/** Removes a subscription for component types being added and removed. */
	removeComponentTypeSubscription(componentType: ComponentType): void {

		// If it is indeed subscribed,
		if (this.componentTypeSubscriptions.has(componentType)) {

			// Remove it from this component type subscriptions list.
			this.componentTypeSubscriptions.delete(componentType);

			// Call the world's remove function.
			this.world.__removeComponentTypeSubscription(this, componentType);
		}
	}

	/** Adds a subscription for when a component property has changed. */
	addComponentChangedSubscription(component: Component, property: number): void {

		// If it isn't already subscribed,
		if (!this.componentChangedSubscriptions.has(component)) {

			// Call the component's add function.
			component.__addChangedSubscription(this, property);

			// Add it to this change subscriptions list.
			let properties = this.componentChangedSubscriptions.get(component);
			if (properties === undefined) {
				properties = new Set();
				this.componentChangedSubscriptions.set(component, properties);
			}
			properties.add(property);
		}
	}

	/** Removes a subscription for when a component property has changed. */
	removeComponentChangedSubscription(component: Component, property: number): void {

		// If it is indeed subscribed,
		if (this.componentChangedSubscriptions.has(component)) {

			// Remove it from this changed subscriptions list.
			const properties = this.componentChangedSubscriptions.get(component);
			if (properties !== undefined) {
				if (properties.delete(property) && properties.size === 0) {
					this.componentChangedSubscriptions.delete(component);
				}
			}

			// Call the component's remove function.
			component.__removeChangedSubscription(this, property);
		}
	}

	/** Called by world to process a component-changed event.
	 *  Subsystems can implement this. */
	__processComponentChangedEvent(_component: Component, _property: number): void {
	}

	/** Called by world to process a component-was-added event.
	 *  Subsystems can implement this. */
	__processComponentWasAddedEvent(_entity: Entity, _component: Component): void {
	}

	/** Called by world to a process a component-will-be-removed event.
	 *  Subsystems can implement this. */
	__processComponentWillBeRemovedEvent(_entity: Entity, _component: Component): void {
	}

	/** Called by world to update the system.
	 *  Subsystems can implement this. */
	update(): void {
	}

	/** The world that contains this. */
	private world: World;

	/** The type. */
	private type: SystemType;

	/** The name. */
	private name: string;

	/** The stage in which this updates. */
	private stage: Stage | undefined;

	/** The current component type subscriptions. */
	private componentTypeSubscriptions: Set<ComponentType>;

	/** The current component property changed subscriptions. */
	private componentChangedSubscriptions: Map<Component, Set<number>>;
}
