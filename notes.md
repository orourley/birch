# ECS

## DESIGN DECISIONS

* GOOD I will have multiple components of the same type in an entity, designated by name. Each component will be named, and if no name is given, it will default to a kebab-case version of its type.
* ---- Components will do nothing except have data. If there is something more complex, like models, the state needs to be in a system.
* Systems need to know when components have changed, so components need to send events to subscribed components when they have changed.
* Systems need to know when a component has been added or removed, so the entity will send an event to the systems.
* Systems order will be manually specified, but grouped in stages (which can be nested).

### Deciding on:

* System Ordering
  * There will be a specific ordering of systems with ordered stages.
    * Pro: Fast, no run-time processing.
    * Con: Manual ordering of systems requires more thought.
  * There will be a dependency graph for systems that write to a component or read from a component.
    * Pro: Automatic ordering of systems.
    * Con: Could be slow if sorting isLess function becomes slow.
    * Con: Some systems need to be manually ordered if they both write and read the same thing.

## Questions

I'd like to go with the ECS model because of the componentization it offers. Beyond that, there are a few questions:

### Frame use case for internal "dirty" code.

In the `Frame` component, the `localToWorld` property needs to be updated when the `position` or `rotation` properties are changed. Specifically, it needs to be updated at latest when it is read, since that is the only time anything outside of the component needs to know its value.

If I wanted to have no code in the Frame component, and only properties, the only other place for code is in the systems. There would have to be some mechanism whereby when the position property is changed, an event is published (by the system that modifies it or the component itself) that some other system subscribes to and updates the localToWorld property.

But this causes a performance issue, because many things may change the position property in a single frame. I don't want to update the localToWorld property every single on every write, only when localToWorld is read.

2022 Jan: I've decided that the performance issues caused by repeatedly writing and sending out 'changed' events will be minimal, since many thing's don't actually change a single property repeatedly in one frame.

From outside the Frame component, localToWorld is a readonly property and there's no set function that is ever called by a system, so that it appears the component is purely readonly data.

### Get/Set property functions in the component class. Readonly classes.

I've tried to make component have generic `get` and `set` functions that have the property names as parameters. The problem is that I lose readonly typing for Object properties. I can do primitive objects just fine, but if I do a 'get' function, I need the object to be readonly. I can do this with the Vector3/Vector3Readonly types, but not for generic types.

I have discovered the improvement, the deep ReadOnly type, but it works only on raw objects (and nested properties) and does not affect functions. This means that something like `get<Vector3>('position').copy(otherV)` won't cause an error, which is wrong.

So right now TypeScript offers no way to make a type truly const and generic `get` and `set` functions can't work unless I make every component property as class without methods, which isn't an option for the math functions.

Well, I said it wasn't an option for the math functions, but I'm going to try it anyway. I'll make the math functions just bare types with the properties, and the functions on them will be in another library that uses function overloading.

I'm again having trouble. I'm making a Vector3 have an m[] property and then get x, y, z accessors. But when the Vector3 is passed into a function as Vector3 (non-ReadOnly), I still can't do `v.x = ` because it only has the accessor. And I can't add a `set` accessor because the ReadOnly type doesn't work with those. I could access the members via `m[0]`, but this is a bit of a pain.

I've tried making Vector3 inherit from Array<number>, but it doesn't work because the ReadOnly type drops the Vector3 type and it's effectively just an array... I've fixed this by modifying the ReadOnly type to consider arrays as objects, which do retain their type. Since functions are already omitted, it doesn't matter too much whether it is treated as an Array or Object.

Here are my conclusions:
* I can't use decorators for auto-making getters and setters. They may work, but they are an experimental feature, and I'd like to support regular features. They don't offer anything I can't get with extra code writing.
* I've tried using the ReadOnlyObject that omits any functions (only static functions). While this works, it isn't satisfactory, because I have to do things like Vec2D.norm(a), and it limits the kinds of classes I can create, especially ones that may be more complicated but still need a readonly option.
* I've tried using the ReadOnlyObject that uses a CanBeReadOnly class to limit the keys to just what's in the Vec2DReadOnly class. It partially works, and I'm going to see if I can continue fine-tuning it.

### Model use case for APIs outside of the ECS framework.

In the `Model` component, there is a model property is a more complex object outside of the ECS framework. It needs to be added to the renderer when the component is created, and removed when the component is destroyed.

Can I somehow move that code to a system?

## Handling interweaved dependencies in systems.

Let's say I have an align system and a position system. And then I have two entities in 2D space, A and B, with these components with descriptions and dependencies:

* A's external position component is dependent on some external data. (data system: writes a.position)
* B's forward component is 90 degrees more than A's position vector. (align-from-position system: b.writes forward, reads a.position)
* A's forward component twice more degrees than B's forward component. (align-from-forward system: writes a.forward, reads b.forward)
* B's position component is 5 units in "front" of A (depends on A's position and rotation). (place-in-front system: writes b.position, reads b.position & b.forward)

Now, let's say you have a few systems:

* A system that gets the external data to position A.
* A system that rotates a component to align with the position or rotation of another component.
* A system that positions a component in front of another.

This is a bit complex, but the main idea is that the components are deeply dependent on each other, and the dependencies can be in any direction, depending in their properties. This means that the order of the systems execution is dependent on the properties of the components themselves, so that there cannot be a fixed ordering to systems. There are two methods for dealing with this:

* Whenever a component is updated, an event is sent to subscribed systems and they execute their component changes. Then the events continue to be propagated and the right components are modified in the right order.
* There are no systems, but there are controllers that have update functions. The controllers also have dependencies based on their read and writing of data, and the order of execution of the update functions is sorted by some external system, and then executed.

With the events method, you have a lot of event objects moving around, and there is a significant performance overhead with all of the event passing. If there are things that run every frame, then every single modification of every property will trigger an event (because who knows who needs it?) and this is a lot of overhead.

With the controller update method, the large overhead is the dependency sorting method, but it should be quick if insertion sort is used, since dependencies don't change much frame-to-frame. Then each controller's update would be called in the right order, and no messages need to be passed around.

## Component Controllers

Let's call every component updater a controller. It would have a list of properties it reads, and a list of properties it writes.

### Frame component and localToWorld update controller.

In the Frame component example above, where you have position and rotation, there can be a localToWorld update controller. It would declare that it reads the position and rotation properties and writes to the localToWorld property.

Then any other controller that writes to that entity's position or rotation properties would be sorted to run before that entity's localToWorld update controller. All dependencies are resolved in the right order and the localToWorld update property is only written once per frame.

And anything that needs the localToWorld property would be sorted to run after the localToWorld update controller.

Cycles can be detected to, in case of dependencies that calls endless loops.

### Controllers don't have to be in an entity.

Controllers can really be the same thing as systems, but they may modify only a single entity's components. There can be other controllers that modify components of multiple, or even all, entities.

This implies that I can still have systems, but that there will be lots of them for individual entities, and there will be some that work on many entities at once (say a physics controller). I can still have the DAG topological sorting of the systems every frame.

It also means that the dependencies have to be more general. For instance, a controller can be dependent on a single entity's component, a single component type of all entities, or even a subset of all entities. It all depends on the controller.

### Cycles in the DAG are inevitable.

There will be controllers that both read and write to the same component. What does this mean for dependencies? Any other controller that reads from the component will need to be after, and any that writes to the component will need to be before. But what about two controllers that both read and write to the same component?

Controllers should have the ability to say that they are dependent on other controllers. This makes the controller dependency explicit, rather than implicit, and would override any other component dependencies.

Another example, are these two controllers:

* Controller 1 - reads the positions of all entities, writes to the level, updating dirt paths.
* Controller 2 - reads the positions of all entities, reads from the level to find walls for moving entities, writes the position of entities.

Controller 1 writes to the level and Controller 2 reads from the level, so Controller 1 must go first. But Controller 2 writes the position of the entities and Controller 1 reads the position of all entities, so Controller 2 should go first. How do we resolve this?

Let's say we want Controller 2 to go, and then Controller 1, since the dirt paths can just go at the end of a frame. In order for Controller 1 to go last, it can just omit the fact that it writes to the level. Or even better, it could somehow declare that it only writes to dirt blocks, so that Controller 2's reading of wall blocks doesn't trigger any dependency cycles.

## Multiple vs Single Components Per Entity

I think that multiple component types per entity are a requirement. Any entity might have more than one model or light, for instance. The question is, how are they implemented? I see two options:

* Multiple components of the same type, but with different ids.
* A single aggregate component that holds an array of items.

With multiple components of the same type, you need to refer to each component by id instead of type. This means that any systems or controllers need to use their id.

With a single aggregate component, there needs to be an array that the controllers or systems iterate over.

Either way doesn't matter as much, although the single aggregate component allows for all components to just be properties in a JavaScript object.

# Design Decisions Based on the Notes

* Entities will be just JavaScript objects, as in `{}`.
* Each component will be unique property in the object.
  * This implies the uniqueness of component types within an entity.
  * If there needs to be more than one of a component (models, lights, etc), the property type needs to be the component type or an array of the component types: `Type | Type[]`.
* The scene will have a large list of controllers and entities.
* Each controller will have dependents and dependencies. Items in the list can be:
  * other controllers. ??? possibly, still think about it.
  * a specific component on a specific entity.
  * a specific component type.

# I'm running into an optimization problem related to the frame update issue.

With the current setup, systems always run in a certain order based on their dependencies and components are just pure data.

This means that a frame-update-matrices system has no way of knowing if a component's position or orientation has been changed by some other system.

If I have 1000 entities, and only 1 of them moves their position every frame, I don't want to be running the frame-update-matrices calcs on every frame component, just the one that changed.

This means that I can't have the components be pure data, as that provides no way of signaling that the position changed. There has to be some sort of event, no matter what. But how is this event organized?

There doesn't need to be an event if there is no system reading a certain property.

Perhaps there can be a mapping of component-types to a list of events, something like:

  Map<ComponentType, [Systems, Event]> events

The systems would be those subscribed to the component types and the events would be the events generated this frame.

And a component can have a setState function that can be used like

  getProperty('position')
  setProperty('position', newPosition)

The setState function would also send an event with the entity, component, and property to the world. The world would add it to the events list (it would be a pool so that garbage isn't generated).

Then, any systems that are subscribed would trigger and process each event in the list.
