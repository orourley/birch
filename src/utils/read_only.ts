/** A primitive, which is always readonly. */
type ReadOnlyPrimitive = undefined | null | boolean | string | number | ((...args: unknown[]) => void);

/** A deep readonly type.
 *  To use it, create two classes, a read-only one (AReadOnly) and a writable one (A).
 *  For the read-only one, it must extend the CanBeReadOnly<AReadOnly> class, using itself as the template parameter.
 *  Then you can use ReadOnly<A> and it'll automatically use the correct read-only type (AReadOnly).
 */
export type ReadOnly<T> =
	T extends ReadOnlyPrimitive ? T :
		T extends (infer U)[] ? ReadOnlyArray<U> :
			T extends Map<infer K, infer V> ? ReadOnlyMap<K, V> :
				T extends Set<infer M> ? ReadOnlySet<M> :
					T extends CanBeReadOnly ? ReadOnlyObject<T> :
						never;

export type ReadOnlyArray<T> = readonly ReadOnly<T>[];
export type ReadOnlyMap<K, V> = ReadonlyMap<ReadOnly<K>, ReadOnly<V>>;
export type ReadOnlySet<T> = ReadonlySet<ReadOnly<T>>;

/** Make every key readonly, and only grab the keys that are in the ReadOnlySymbol type list. */
type ReadOnlyObject<T extends CanBeReadOnly> = { readonly [K in T[typeof ReadonlySymbol]]: ReadOnly<T[K]> };

/** A symbol that's used to reference the list of keys in the read-only class. */
const ReadonlySymbol = Symbol('Readonly');

/** A base class that is used to list the keys in the read-only class. */
export class CanBeReadOnly {
	[ReadonlySymbol]!: keyof unknown;
}
