import { FastSet, Vec3D, type Vec3DReadOnly } from '../internal';

export class SpatialGrid<Value> {

	/** Constructs the spatial grid. */
	constructor(minBound: Vec3DReadOnly, maxBound: Vec3DReadOnly, use2D: boolean) {

		// Save the min and max bounds.
		this._minBound.copy(minBound);
		this._maxBound.copy(maxBound);

		// Set up the number of rows.
		this._use2D = use2D;
		if (this._use2D) {
			this._numRows.set(16, 16, 1);
		}
		else {
			this._numRows.set(16, 16, 16);
		}

		// Set up the cells.
		for (let i = 0, l = this._numRows.x * this._numRows.y * this._numRows.z; i < l; i++) {
			this._cells.push(new FastSet());
		}
	}

	/** Updates the values with those in the bounds, adding any new values and removing any old ones. */
	updateValuesInList(_values: FastSet<Value>, _minBounds: Vec3D, _maxBounds: Vec3D) {
	}

	/** Adds a value at the position. */
	add(_value: Value, position: Vec3DReadOnly) {
		const cell = Vec3D.pool.get();
		for (let i = 0; i < 3; i++) {
			cell.array[i] = (position.array[i]! - this._minBound.array[i]!) / (this._maxBound.array[i]! - this._minBound.array[i]!) / this._numRows.array[i]!;
			if (cell.array[i]! < 0 || cell.array[i]! >= this._numRows.array[i]!) {
				throw new Error('Value out of bounds.');
			}
		}
		Vec3D.pool.release(cell);
	}

	/** Removes a value from the position. */
	remove(_value: Value, _position: Vec3DReadOnly) {
	}

	/** The minimum that a position can be. */
	private _minBound = new Vec3D();

	/** The maximum that a position can be. */
	private _maxBound = new Vec3D();

	/** Whether or not we're using a 2D grid. */
	private _use2D: boolean;

	/** The number of rows in the grid. */
	private _numRows = new Vec3D();

	// /** The total number of values in all cells. */
	// private _numValues = 0;

	private _cells: FastSet<Value>[] = [];
}
