import { type Component, FastSet } from '../internal';

type ComplexObjectReadOnly<TypeReadOnly> = {
	isNaN: () => boolean;
	equals: (value: TypeReadOnly) => boolean;
};

/** An object with a copy function. */
type ComplexObject<TypeReadOnly> = TypeReadOnly & ComplexObjectReadOnly<TypeReadOnly> & {
	copy: (value: TypeReadOnly) => void;
};

/** An object without a copy function. */
type WithoutCopyAndEquals = number | string | boolean | undefined | {
	isNaN?: undefined;
	copy?: undefined;
	equals?: undefined;
};

/** The base class for the callback value. */
class CallbackValueBase<ComponentType extends Component, ValueType extends ValueTypeReadOnly, ValueTypeReadOnly = ValueType> {

	protected readonly component: ComponentType;

	protected value: ValueType;

	constructor(component: ComponentType, startingValue: ValueType) {
		this.component = component;
		this.value = startingValue;
	}

	/** Adds a callback to be called when the value is set. */
	addCallback(callback: (component: ComponentType, value: ValueTypeReadOnly) => void) {
		this._callbacks.add(callback);
	}

	/** Removes a callback. */
	removeCallback(callback: (component: ComponentType, value: ValueTypeReadOnly) => void) {
		this._callbacks.remove(callback);
	}

	/** Calls the callbacks. */
	protected _call(value: ValueTypeReadOnly) {
		const array = this._callbacks.getArray();
		for (let i = 0, l = array.length; i < l; i++) {
			array[i]!(this.component, value);
		}
	}

	private _callbacks = new FastSet<(component: ComponentType, value: ValueTypeReadOnly) => void>();
}

/** A class with a get/set value that triggers callbacks when modified. This version requires a copy method.
 *  You can also specify a ValueTypeReadOnly class from which the ValueType extends. */
export class CallbackValueCopy<ComponentType extends Component, ValueType extends ComplexObject<ValueTypeReadOnly>,
	ValueTypeReadOnly extends ComplexObjectReadOnly<ValueTypeReadOnly> = ValueType>
	extends CallbackValueBase<ComponentType, ValueType, ValueTypeReadOnly> {

	getValue(): ValueTypeReadOnly {
		return this.value;
	}

	setValue(value: ValueTypeReadOnly) {
		if (!this.value.equals(value) && (!this.value.isNaN() || !value.isNaN())) {
			this.value.copy(value);
			this._call(this.value);
		}
	}
}

/** A class with a get/set value that triggers callbacks when modified. This version requires no copy method.
 *  The V is there because TypeScript can't fully handle circular types yet. */
export class CallbackValue<ComponentType extends Component, ValueType extends ValueTypeReadOnly & WithoutCopyAndEquals, ValueTypeReadOnly = ValueType>
	extends CallbackValueBase<ComponentType, ValueType, ValueTypeReadOnly> {

	getValue(): ValueType {
		return this.value;
	}

	setValue(value: ValueType) {
		if (this.value !== value) {
			this.value = value;
			this._call(this.value);
		}
	}
}
