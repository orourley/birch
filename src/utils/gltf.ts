import { JsonHelper, type JsonObject, type JsonType } from '@orourley/pine-lib';
import { type VertexComponent, type Renderer, Texture, type Model, UrlUtils, type Mesh, type Downloader, type Scene } from '../internal';

/** A class to support loading and saving of GLTF 2.0 files. */
export class Gltf {

	/** Load a GLTF. */
	static async load(url: string, renderer: Renderer, scene: Scene, downloader: Downloader): Promise<Model> {

		// Do the download of the JSON.
		const gltfDownload = await downloader.getJson5(url);
		const gltfJson = gltfDownload.content;
		try {
			if (!JsonHelper.isObject(gltfJson)) {
				throw new Error('JSON is not an object.');
			}

			// Check version.
			if (!JsonHelper.isObject(gltfJson['asset'])) {
				throw new Error('Missing asset object property.');
			}
			if (!JsonHelper.isString(gltfJson['asset']['version'])) {
				throw new Error('Missing asset.version string property.');
			}
			if (gltfJson['asset']['version'] !== '2.0') {
				throw new Error('The GLTF version is not 2.0.');
			}

			// Buffers
			if (!JsonHelper.isArray(gltfJson['buffers'])) {
				throw new Error('Missing buffers array property.');
			}
			if (gltfJson['buffers'].length !== 1) {
				throw new Error('Only buffers array of length one currently supported.');
			}
			const buffers0 = gltfJson['buffers'][0];
			if (buffers0 === undefined || !JsonHelper.isObject(buffers0)) {
				throw new Error('buffers[0] must be an object.');
			}
			if (!JsonHelper.isString(buffers0['uri'])) {
				throw new Error('Missing buffers[0].uri string property.');
			}

			// Buffer Views (Different contiguous bytes into the buffers).
			const bufferViews: GltfJson.BufferView[] = [];
			if (!JsonHelper.isArray(gltfJson['bufferViews'])) {
				throw new Error('Missing bufferViews array property.');
			}
			for (let i = 0, l = gltfJson['bufferViews'].length; i < l; i++) {
				const bufferView = gltfJson['bufferViews'][i]!;
				try {
					GltfJson.validateBufferView(bufferView);
				}
				catch (e) {
					throw new Error(`In bufferViews[${i}]: ${e}`);
				}
				bufferViews.push(bufferView);
			}

			// Accessors (A typed view into a BufferView).
			const accessors: GltfJson.Accessor[] = [];
			if (!JsonHelper.isArray(gltfJson['accessors'])) {
				throw new Error('Missing accessors array property.');
			}
			for (let i = 0, l = gltfJson['accessors'].length; i < l; i++) {
				const accessor = gltfJson['accessors'][i]!;
				try {
					GltfJson.validateAccessor(accessor, bufferViews);
				}
				catch (e) {
					throw new Error(`In accessors[${i}]: ${e}`);
				}
				accessors.push(accessor);
			}

			// Mesh
			if (!JsonHelper.isArray(gltfJson['meshes'])) {
				throw new Error('Missing meshes array property.');
			}
			if (gltfJson['meshes'].length !== 1) {
				throw new Error('Only meshes array of length one currently supported.');
			}
			const meshes0 = gltfJson['meshes'][0];
			if (meshes0 === undefined || !JsonHelper.isObject(meshes0)) {
				throw new Error('meshes[0] must be an object.');
			}
			if (!JsonHelper.isArray(meshes0['primitives'])) {
				throw new Error('Missing meshes[0].primitives array property.');
			}
			if (meshes0['primitives'].length !== 1) {
				throw new Error('Only meshes[0].primitives array of length one currently supported.');
			}
			const primitive = meshes0['primitives'][0]!;
			GltfJson.validatePrimitive(primitive, accessors);

			// Images
			const images: GltfJson.Image[] = [];
			if (!JsonHelper.isArray(gltfJson['images']) && gltfJson['images'] !== undefined) {
				throw new Error('Missing images array or undefined property.');
			}
			if (gltfJson['images']) {
				for (let i = 0, l = gltfJson['images'].length; i < l; i++) {
					const image = gltfJson['images'][i]!;
					try {
						GltfJson.validateImage(image);
					}
					catch (e) {
						throw new Error(`in images[${i}], ${e}`);
					}
					images.push(image);
				}
			}

			// Samplers
			const samplers: GltfJson.Sampler[] = [];
			if (!JsonHelper.isArray(gltfJson['samplers']) && gltfJson['samplers'] !== undefined) {
				throw new Error('Missing samplers array property.');
			}
			if (gltfJson['samplers']) {
				for (let i = 0, l = gltfJson['samplers'].length; i < l; i++) {
					const sampler = gltfJson['samplers'][i]!;
					try {
						GltfJson.validateSampler(sampler);
					}
					catch (e) {
						throw new Error(`in samplers[${i}], ${e}`);
					}
					samplers.push(sampler);
				}
			}

			// Textures
			const textures: GltfJson.Texture[] = [];
			if (!JsonHelper.isArray(gltfJson['textures']) && gltfJson['textures'] !== undefined) {
				throw new Error('Missing textures array property.');
			}
			if (gltfJson['textures']) {
				for (let i = 0, l = gltfJson['textures'].length; i < l; i++) {
					const texture = gltfJson['textures'][i]!;
					try {
						GltfJson.validateTexture(texture, images, samplers);
					}
					catch (e) {
						throw new Error(`in textures[${i}], ${e}`);
					}
					textures.push(texture);
				}
			}

			// Materials
			const materials: GltfJson.Material[] = [];
			if (!JsonHelper.isArray(gltfJson['materials']) && gltfJson['materials'] !== undefined) {
				throw new Error('Missing materials array property.');
			}
			if (gltfJson['materials']) {
				if (gltfJson['materials'].length > 1) {
					throw new Error('Only materials array of length one currently supported.');
				}
				for (let i = 0, l = gltfJson['materials'].length; i < l; i++) {
					const material = gltfJson['materials'][i]!;
					try {
						GltfJson.validateMaterial(material, textures);
					}
					catch (e) {
						throw new Error(`in materials[${i}], ${e}`);
					}
					materials.push(material);
				}
			}

			// Here we start creating the resources.
			// The Promises array that will be waited for at the end.
			const promises: Promise<void>[] = [];

			// First create the model.
			const model = scene.models.create();

			// Create the mesh and set up the vertex format.
			const mesh: Mesh = renderer.meshes.create();
			model.setMesh(mesh);
			const vertexFormat: VertexComponent[][] = [];
			for (const [attributeName, accessorIndex] of Object.entries(primitive.attributes)) {
				const accessor = accessors[accessorIndex];
				if (!accessor) {
					throw new Error(`in primitives[0] attribute ${attributeName}, invalid accessor`);
				}

				// Create a vertex buffer for each unique buffer view.
				while (vertexFormat.length <= accessor.bufferView) {
					vertexFormat.push([]);
				}
				vertexFormat[accessor.bufferView]!.push({
					dimensions: accessor.vertexDimensions,
					type: accessor.vertexType,
					location: GltfJson.attributeTypesToLocations.get(attributeName)!
				});
			}
			mesh.setVertexFormat(vertexFormat);

			// Download the .bin file and then set the vertices.
			const bufferUrl = `${url.substring(0, url.lastIndexOf('/') + 1)}${buffers0['uri']}`;
			promises.push(downloader.getBinary(bufferUrl).then((bufferDownload) => {

				// Set the vertices.
				for (let i = 0, l = vertexFormat.length; i < l; i++) {
					const bufferView = bufferViews[i]!;
					mesh.setVertices(i, new Uint8Array(bufferDownload.content, bufferView.byteOffset, bufferView.byteLength), false);
				}

				// Set mesh indices.
				const indexAccessor = accessors[primitive.indices];
				if (!indexAccessor) {
					throw new Error('in primitives[0], invalid indices');
				}
				const indexBufferView = bufferViews[indexAccessor.bufferView];
				if (!indexBufferView) {
					throw new Error('in primitives[0].indices accessor, invalid bufferView');
				}
				mesh.setIndices(new Uint8Array(bufferDownload.content, indexBufferView.byteOffset, indexBufferView.byteLength), false);
			}));

			// Set up the uniform group.
			const uniformGroup = model.getUniformGroup();
			uniformGroup.setUpUniformBlock(`
				uniform model {
					highp mat4 localToWorld;
					mediump vec4 baseColorFactor;
				};`);

			// Set up the samplers and textures.
			uniformGroup.setUpSamplers(['baseColorTexture', 'normalTexture']);

			// Set base color factor.
			if (materials.length > 0) {
				uniformGroup.setUniform('baseColorFactor', materials[0]!.pbrMetallicRoughness.baseColorFactor);
			}
			else {
				uniformGroup.setUniform('baseColorFactor', [1, 1, 1, 1]);
			}

			// Set base color texture.
			let baseColorTexturePromise: Promise<Texture> | undefined;
			if (materials.length > 0 && materials[0]!.pbrMetallicRoughness.baseColorTexture !== undefined) {
				const textureGltfIndex = materials[0]!.pbrMetallicRoughness.baseColorTexture.index;
				const textureGltf = textures[textureGltfIndex];
				if (!textureGltf) {
					throw new Error('in materials[0], invalid base color texture index');
				}
				const imageGltf = images[textureGltf.source];
				if (!imageGltf) {
					throw new Error('in materials[0], invalid base color texture image index');
				}
				baseColorTexturePromise = this._getTexture(imageGltf.uri, url, renderer, downloader);
			}
			if (baseColorTexturePromise) {
				promises.push(baseColorTexturePromise.then((texture) => {
					uniformGroup.setTexture('baseColorTexture', texture);
				}));
			}

			// Set normal texture.
			let normalTexturePromise: Promise<Texture> | undefined;
			if (materials.length > 0 && materials[0]!.normalTexture !== undefined) {
				const textureGltfIndex = materials[0]!.normalTexture.index;
				const textureGltf = textures[textureGltfIndex];
				if (!textureGltf) {
					throw new Error('in materials[0], invalid normal texture index');
				}
				const imageGltf = images[textureGltf.source];
				if (!imageGltf) {
					throw new Error('in materials[0], invalid normal texture image index');
				}
				normalTexturePromise = this._getTexture(imageGltf.uri, url, renderer, downloader);
			}
			if (normalTexturePromise) {
				promises.push(normalTexturePromise.then((texture) => {
					uniformGroup.setTexture('normalTexture', texture);
				}));
			}

			// Wait for all of the promises to complete.
			await Promise.allSettled(promises);

			return model;
		}
		catch (e) {
			throw new Error(`While parsing ${url}: ${e}`);
		}
	}

	/** Gets a texture. */
	private static async _getTexture(uri: string, gltfUrl: string, renderer: Renderer, downloader: Downloader): Promise<Texture> {
		const textureUrl = UrlUtils.getFullUrlFromUri(uri, gltfUrl);
		const texture = renderer.textures.create();
		const download = await downloader.getBinary(textureUrl);
		await texture.loadSource(download.content, undefined, undefined, Texture.Format.RGBA);
		return texture;
	}
}

export namespace Gltf {
	export type UsedResources = {
		textureNames: string[];
	};
}

namespace GltfJson {

	export const accessorComponentTypeToType = new Map([
		[5120, 'byte'],
		[5121, 'ubyte'],
		[5122, 'short'],
		[5123, 'ushort'],
		[5125, 'uint'],
		[5126, 'float']
	]);

	export const accessorTypeToDimensions = new Map([
		['SCALAR', 1],
		['VEC2', 2],
		['VEC3', 3],
		['VEC4', 4],
		['MAT2', 4],
		['MAT3', 9],
		['MAT4', 16]
	]);

	export interface BufferView {
		buffer: number;
		byteOffset: number;
		byteLength: number;
		byteStride?: number;
	}

	export function validateBufferView(json: JsonType): asserts json is JsonObject & BufferView {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isNumber(json['buffer'])) {
			throw new Error('buffer must be a number property.');
		}
		if (json['buffer'] !== 0) {
			throw new Error('buffer must be 0.');
		}
		if (!JsonHelper.isNumber(json['byteLength'])) {
			throw new Error('byteLength must be a number property.');
		}
		if (!JsonHelper.isNumber(json['byteOffset']) && json['byteOffset'] !== undefined) {
			throw new Error('byteOffset must be a number or undefined property.');
		}
		json['byteOffset'] ??= 0;
		if (!JsonHelper.isNumber(json['byteStride']) && json['byteStride'] !== undefined) {
			throw new Error('byteStride must be a number or undefined property.');
		}
	}

	export interface Accessor {
		bufferView: number;
		byteOffset: number;
		componentType: number;
		count: number;
		type: string;
		vertexType: VertexComponent['type'];
		vertexDimensions: number;
	}

	export function validateAccessor(json: JsonType, bufferViews: BufferView[]): asserts json is JsonObject & Accessor {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isNumber(json['bufferView'])) {
			throw new Error('bufferView must be a number property (sparse accessors not supported).');
		}
		if (bufferViews[json['bufferView']] === undefined) {
			throw new Error('bufferView must point to a valid buffer.');
		}
		if (!JsonHelper.isNumber(json['byteOffset']) && json['byteOffset'] !== undefined) {
			throw new Error('byteOffset must be number or undefined property.');
		}
		json['byteOffset'] ??= 0;
		if (!JsonHelper.isNumber(json['count'])) {
			throw new Error('count must be a number property.');
		}
		if (!JsonHelper.isNumber(json['componentType'])) {
			throw new Error('componentType must be a number property.');
		}
		const vertexType = accessorComponentTypeToType.get(json['componentType']);
		if (vertexType === undefined) {
			throw new Error('componentType is an unsupported value.');
		}
		json['vertexType'] = vertexType;
		if (!JsonHelper.isString(json['type'])) {
			throw new Error('type must be a string property.');
		}
		const vertexDimensions = accessorTypeToDimensions.get(json['type']);
		if (vertexDimensions === undefined) {
			throw new Error('type is an unsupported value.');
		}
		json['vertexDimensions'] = vertexDimensions;
	}

	export type AttributeTypes =
		'POSITION' | 'NORMAL' | 'TANGENT' |
		'TEXCOORD_0' | 'TEXCOORD_1' | 'TEXCOORD_2' | 'TEXCOORD_3' |
		'COLOR_0' | 'COLOR_1' | 'COLOR_2' | 'COLOR_3';

	export const attributeTypesToLocations = new Map([
		['POSITION', 0],
		['NORMAL', 1],
		['TANGENT', 2],
		['TEXCOORD_0', 3],
		['TEXCOORD_1', 4],
		['TEXCOORD_2', 5],
		['TEXCOORD_3', 6],
		['COLOR_0', 7],
		['COLOR_1', 8],
		['COLOR_2', 9],
		['COLOR_3', 10]
	]);

	export interface Primitive {
		attributes: Record<AttributeTypes, number>;
		indices: number;
		material: number;
	}

	export function validateAttribute(key: string, accessor: Accessor): asserts key is AttributeTypes {
		if (key === 'POSITION') {
			if (accessor.vertexDimensions !== 3) {
				throw new Error(`The accessor for attributes[${key}] must use 3 components.`);
			}
			if (accessor.vertexType !== 'float') {
				throw new Error(`The accessor for attributes[${key}] must use floats.`);
			}
		}
		else if (key === 'NORMAL') {
			if (accessor.vertexDimensions !== 3) {
				throw new Error(`The accessor for attributes[${key}] must use 3 components.`);
			}
			if (accessor.vertexType !== 'float') {
				throw new Error(`The accessor for attributes[${key}] must use floats.`);
			}
		}
		else if (key === 'TANGENT') {
			if (accessor.vertexDimensions !== 4) {
				throw new Error(`The accessor for attributes[${key}] must use 4 components.`);
			}
			if (accessor.vertexType !== 'float') {
				throw new Error(`The accessor for attributes[${key}] must use floats.`);
			}
		}
		else if (key.startsWith('TEXCOORD_')) {
			if (accessor.vertexDimensions !== 2) {
				throw new Error(`The accessor for attributes[${key}] must use 2 components.`);
			}
			if (!['float', 'ubyte', 'ushort'].includes(accessor.vertexType)) {
				throw new Error(`The accessor for attributes[${key}] must use floats, ubytes, or ushorts.`);
			}
		}
		else if (key.startsWith('COLOR_')) {
			if (accessor.vertexDimensions !== 3 && accessor.vertexDimensions !== 4) {
				throw new Error(`The accessor for attributes[${key}] must use 3 or 4 components.`);
			}
			if (!['float', 'ubyte', 'ushort'].includes(accessor.vertexType)) {
				throw new Error(`The accessor for attributes[${key}] must use floats, ubytes, or ushorts.`);
			}
		}
		else {
			throw new Error(`The attribute[${key}] is unsupported.`);
		}
	}

	export function validatePrimitive(json: JsonType, accessors: Accessor[]): asserts json is JsonObject & Primitive {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isObject(json['attributes'])) {
			throw new Error('attributes must be an object.');
		}
		for (const key of Object.keys(json['attributes'])) {
			const accessorIndex = json['attributes'][key];
			if (typeof accessorIndex !== 'number') {
				throw new Error(`attributes[${key}] must be a number.`);
			}
			const accessor = accessors[accessorIndex];
			if (accessor === undefined) {
				throw new Error(`attributes[${key}] must point to a valid accessor.`);
			}
			validateAttribute(key, accessor);
		}
		if (!JsonHelper.isNumber(json['indices'])) {
			throw new Error('indices must be a number property.');
		}
		const accessor = accessors[json['indices']];
		if (accessor === undefined) {
			throw new Error('indices must point to a valid accessor.');
		}
		if (!JsonHelper.isNumber(json['material']) && json['material'] !== undefined) {
			throw new Error('material must be a number or undefined property.');
		}
	}

	export interface Image {
		uri: string;
	}

	export function validateImage(json: JsonType): asserts json is JsonObject & Image {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isString(json['uri'])) {
			throw new Error('uri must be a string property (bufferView property not currently supported).');
		}
	}

	export interface Sampler {
		magFilter: number;
		minFilter: number;
		wrapS: number;
		wrapT: number;
	}

	export function validateSampler(json: JsonType): asserts json is JsonObject & Sampler {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isNumber(json['magFilter']) && json['magFilter'] !== undefined) {
			throw new Error('magFilter must be a number or undefined property.');
		}
		json['magFilter'] ??= 9729; /* Linear filtering */
		if (!JsonHelper.isNumber(json['minFilter']) && json['minFilter'] !== undefined) {
			throw new Error('minFilter must be a number or undefined property.');
		}
		json['minFilter'] ??= 9987; /* Linear-mipmap-linear filtering */
		if (!JsonHelper.isNumber(json['wrapS']) && json['wrapS'] !== undefined) {
			throw new Error('wrapS must be a number or undefined property.');
		}
		json['wrapS'] ??= 10497; /* Repeat wrapping */
		if (!JsonHelper.isNumber(json['wrapT']) && json['wrapT'] !== undefined) {
			throw new Error('wrapT must be a number or undefined property.');
		}
		json['wrapT'] ??= 10497; /* Repeat wrapping */
	}

	export interface Texture {
		source: number;
		sampler?: number;
	}

	export function validateTexture(json: JsonType, images: Image[], samplers: Sampler[]): asserts json is JsonObject & Texture {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isNumber(json['source'])) {
			throw new Error('source must be a number property.');
		}
		if (images[json['source']] === undefined) {
			throw new Error('source must point to a valid image.');
		}
		if (!JsonHelper.isNumber(json['sampler']) && json['sampler'] !== undefined) {
			throw new Error('sampler must be a number or undefined property.');
		}
		if (json['sampler'] !== undefined) {
			if (samplers[json['sampler']] === undefined) {
				throw new Error('sampler must point to a valid sampler.');
			}
		}
	}

	export interface TextureInfo {
		index: number;
	}

	export interface Material {
		pbrMetallicRoughness: {
			baseColorFactor: number[];
			metallicFactor: number;
			roughnessFactor: number;
			baseColorTexture?: TextureInfo;
			metallicRoughnessTexture?: TextureInfo;
		};
		normalTexture?: TextureInfo;
	}

	export function validateMaterial(json: JsonType, textures: Texture[]): asserts json is JsonObject & Material {
		if (!JsonHelper.isObject(json)) {
			throw new Error('must be an object.');
		}
		if (!JsonHelper.isObject(json['pbrMetallicRoughness'])) {
			throw new Error('pbrMetallicRoughness must be an object property.');
		}
		const pbrMetallicRoughness = json['pbrMetallicRoughness'];
		if (pbrMetallicRoughness['baseColorFactor'] !== undefined) {
			if (!JsonHelper.isArrayOfNumbers(pbrMetallicRoughness['baseColorFactor']) || pbrMetallicRoughness['baseColorFactor'].length !== 4) {
				throw new Error('pbrMetallicRoughness.baseColorFactor must be a number[4] or undefined property.');
			}
		}
		else {
			pbrMetallicRoughness['baseColorFactor'] = [1, 1, 1, 1];
		}
		if (!JsonHelper.isNumber(pbrMetallicRoughness['metallicFactor']) && pbrMetallicRoughness['metallicFactor'] !== undefined) {
			throw new Error('pbrMetallicRoughness.metallicFactor must be a number or undefined property.');
		}
		pbrMetallicRoughness['metallicFactor'] ??= 1;
		if (!JsonHelper.isNumber(pbrMetallicRoughness['roughnessFactor']) && pbrMetallicRoughness['roughnessFactor'] !== undefined) {
			throw new Error('pbrMetallicRoughness.roughnessFactor must be a number or undefined property.');
		}
		pbrMetallicRoughness['roughnessFactor'] ??= 1;
		if (!JsonHelper.isObject(pbrMetallicRoughness['baseColorTexture']) && pbrMetallicRoughness['baseColorTexture'] !== undefined) {
			throw new Error('pbrMetallicRoughness.baseColorTexture must be an object or undefined property.');
		}
		if (pbrMetallicRoughness['baseColorTexture'] !== undefined) {
			const baseColorTexture = pbrMetallicRoughness['baseColorTexture'];
			if (!JsonHelper.isNumber(baseColorTexture['index'])) {
				throw new Error('pbrMetallicRoughness.baseColorTexture.index must be a number property.');
			}
			if (textures[baseColorTexture['index']] === undefined) {
				throw new Error('pbrMetallicRoughness.baseColorTexture.index must point to a valid texture.');
			}
		}
		if (!JsonHelper.isObject(pbrMetallicRoughness['metallicRoughnessTexture']) && pbrMetallicRoughness['metallicRoughnessTexture'] !== undefined) {
			throw new Error('pbrMetallicRoughness.metallicRoughnessTexture must be an object or undefined property.');
		}
		if (pbrMetallicRoughness['metallicRoughnessTexture'] !== undefined) {
			const metallicRoughnessTexture = pbrMetallicRoughness['metallicRoughnessTexture'];
			if (!JsonHelper.isNumber(metallicRoughnessTexture['index'])) {
				throw new Error('pbrMetallicRoughness.metallicRoughnessTexture.index must be a number property.');
			}
			if (textures[metallicRoughnessTexture['index']] === undefined) {
				throw new Error('pbrMetallicRoughness.metallicRoughnessTexture.index must point to a valid texture.');
			}
		}
		if (!JsonHelper.isObject(json['normalTexture']) && json['normalTexture'] !== undefined) {
			throw new Error('normalTexture must be an object or undefined property.');
		}
		if (json['normalTexture'] !== undefined) {
			const normalTexture = json['normalTexture'];
			if (!JsonHelper.isNumber(normalTexture['index'])) {
				throw new Error('normalTexture.index must be a number property.');
			}
			if (textures[normalTexture['index']] === undefined) {
				throw new Error('normalTexture.index must point to a valid texture.');
			}
		}
	}
}
