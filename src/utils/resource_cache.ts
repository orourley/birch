import { Counted } from './counted';

/** A cache is a set of named resources with use counts that are automatically created and destroyed. */
export class ResourceCache<Type> {

	/** Constructs the cache.
	 * @param params.typeName - A name for the resource for better error output.
	 * @param params.create - The function called to create a new resource.
	 * @param params.destroy - The function called to destroy a resource. */
	constructor(params: {
		typeName: string;
		create: () => Type;
		destroy: (resource: Type) => void;
	}) {
		this._typeName = params.typeName;
		this._createFunction = params.create;
		this._destroyFunction = params.destroy;
	}

	/** @internal Destroys the resource cache. Throws an error if there are still resources being used. Called by Renderer, Scene. */
	zzDestroy() {
		if (this._resources.size > 0) {
			let names = '';
			for (const name of this._resources.keys()) {
				names += `${names !== '' ? ', ' : ''}"${name}"`;
			}
			throw new Error(`There are still ${this._resources.size} used resources in the cache. They are: ${names}`);
		}
	}

	/** Returns true if a resource by the name exists. */
	has(name: string): boolean {
		return this._resources.get(name) !== undefined;
	}

	/** Gets a resource. */
	get(name: string): Type {
		// Get the resource.
		const countedResource = this._resources.get(name);
		if (!countedResource) {
			throw new Error(`Error getting a ${this._typeName} with the name ${name}: The name does not exist.`);
		}
		// Inc the count and return it.
		countedResource.incCount();
		return countedResource.value;
	}

	/** Creates and returns a new resource. It needs to be released at some point. */
	create(name: string): Type {
		// Check if the resource already exists.
		if (this._resources.has(name)) {
			throw new Error(`Error creating a ${this._typeName} with the name ${name}: The name already exists.`);
		}
		// Create the resource, set the entry, and return it.
		const resource = this._createFunction();
		this._resources.set(name, new Counted(resource));
		return resource;
	}

	/** Releases a resource. If it is no longer used, it will be destroyed. */
	release(name: string) {
		// Get the resource.
		const countedResource = this._resources.get(name);
		if (countedResource === undefined) {
			throw new Error(`Error releasing a ${this._typeName} with the name ${name}: The name does not exist.`);
		}
		// Dec the count and destroy the resource if no longer used.
		countedResource.decCount();
		if (countedResource.getCount() === 0) {
			this._destroyFunction(countedResource.value);
		}
	}

	/** The mapping of names to created resources and their use counts. */
	private _resources = new Map<string, Counted<Type>>();

	/** A name for the resource for better error output. */
	private _typeName: string;

	/**  The function called to create a new resource. */
	private _createFunction: () => Type;

	/** The function called to destroy a resource. */
	private _destroyFunction: (resource: Type) => void;
}
