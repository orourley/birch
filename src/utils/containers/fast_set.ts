import { Sort } from '@orourley/pine-lib';

/** A readonly set for items. */
export interface FastSetReadOnly<ItemReadOnly> {

	/** Returns true if the item is in the set. O(1). */
	has: (item: ItemReadOnly) => boolean;

	/** Returns the item array that can be iterated over (but not modified). */
	getArray: () => readonly ItemReadOnly[];

	/** Returns the ordering: 'unordered' means the order doesn't matter, 'ordered' means to keep
	 *  the same order as first to last added (except for moveBefore calls),
	 *  and 'sorted' makes the array stay sorted. O(1). */
	getOrdering: () => 'unordered' | 'ordered' | 'sorted';
}

/** A set for items. */
export class FastSet<Item extends ItemReadOnly, ItemReadOnly = Item> implements FastSetReadOnly<ItemReadOnly> {

	/** The constructor. Can take an *iterable*. */
	constructor(iterable?: Iterable<Item>) {

		// Add the iterable items.
		if (iterable !== undefined) {
			for (const item of iterable) {
				this._itemsToIndices.set(item, this._items.length);
				this._items.push(item);
			}
		}
	}

	/** Returns the item array that can be iterated over (but not modified). */
	getArray(): readonly Item[] {
		return this._items;
	}

	/** Returns true if the item is in the set. O(1). */
	has(item: ItemReadOnly): boolean {
		return this._itemsToIndices.has(item);
	}

	/** Adds an item. Returns true if it didn't already exist. O(log n) if sorted, otherwise O(1). */
	add(item: Item): boolean {

		// The item is already in the set, do nothing.
		if (this._itemsToIndices.has(item)) {
			return false;
		}

		// Add the item and set the new index.
		let index: number;
		if (this._ordering === 'sorted') {
			index = Sort.add(item, this._items, this._isLess!);
		}
		else {
			index = this._items.push(item) - 1;
		}
		this._itemsToIndices.set(item, index);

		// Return true, since the item didn't exist.
		return true;
	}

	/** Removes an item. Returns true if it existed. O(1) if unordered, otherwise O(n). */
	remove(item: ItemReadOnly): boolean {

		// Get the index of the item.
		const index = this._itemsToIndices.get(item);

		// The item is already not in the set, do nothing.
		if (index === undefined) {
			return false;
		}

		// Remove from the item-to-index mapping.
		this._itemsToIndices.delete(item);

		// If the ordering is 'ordered' or 'sorted', we have to adjust the rest of the indices.
		if (this._ordering !== 'unordered') {
			this._items.splice(index, 1);
			for (let i = index, l = this._items.length; i < l; i++) {
				this._itemsToIndices.set(this._items[i]!, i);
			}
		}
		// Otherwise we just move the last element to this index, since order doesn't matter.
		else {
			if (index < this._items.length - 1) {
				this._items[index] = this._items.pop()!;
				this._itemsToIndices.set(this._items[index], index);
			}
			else {
				this._items.pop();
			}
		}

		// Return true since the item existed.
		return true;
	}

	/** Deletes all of the items. O(n). */
	clear() {
		this._items = [];
		this._itemsToIndices.clear();
	}

	/** Returns the ordering: 'unordered' means the order doesn't matter, 'ordered' means to keep
	 *  the same order as first to last added (except for moveBefore calls),
	 *  and 'sorted' makes the array stay sorted. O(1). */
	getOrdering(): 'unordered' | 'ordered' | 'sorted' {
		return this._ordering;
	}

	/** Set the ordering: 'unordered' means the order doesn't matter, 'ordered' means to keep
	 *  the same order as first to last added (except for moveBefore calls),
	 *  and 'sorted' makes the array stay sorted using the provided isLess function.
	 *  Defaults to 'unordered'. O(n) If sorting for the first time, otherwise O(1).
	 *  It uses insertion sort. */
	setOrdering(ordering: 'unordered' | 'ordered' | 'sorted', isLess?: (lhs: ItemReadOnly, rhs: ItemReadOnly) => boolean) {

		// If we're setting this to sorted, we need to sort the items.
		if (this._ordering !== ordering && ordering === 'sorted') {
			if (!isLess) {
				throw new Error("If ordering is 'sorted', an isLess function must be provided.");
			}
			this._isLess = isLess;
			Sort.sort(this._items, this._isLess, (_array, i, j) => {
				this._itemsToIndices.set(this._items[i]!, i);
				this._itemsToIndices.set(this._items[j]!, j);
			});
		}
		this._ordering = ordering;
	}

	/** Move one item before another, or to the end if beforeItem is undefined.
	 *  Throws an error if ordering is not 'ordered'.  O(n) */
	moveBefore(item: ItemReadOnly, beforeItem?: ItemReadOnly) {

		// Make sure we're using 'ordered' ordering.
		if (this._ordering !== 'ordered') {
			throw new Error("Items can only be moved if ordering is 'ordered'.");
		}

		// Get the index of the item.
		const index = this._itemsToIndices.get(item);
		if (index === undefined) {
			throw new Error('The item is not in the set.');
		}

		// Get the index of the beforeItem.
		const beforeIndex = beforeItem !== undefined ? this._itemsToIndices.get(beforeItem) : this._items.length;
		if (beforeIndex === undefined) {
			throw new Error('The beforeItem is not in the set.');
		}

		// Remove the item.
		const removedItem = this._items.splice(index, 1)[0]!;

		// Place the item into its new position, accounting for if it was before or after the beforeItem.
		// Also adjust the indices inbetween the old and new positions.
		if (index < beforeIndex) {
			this._items.splice(beforeIndex - 1, 0, removedItem);
			for (let i = index; i < beforeIndex; i++) {
				this._itemsToIndices.set(this._items[i]!, i);
			}
		}
		else {
			this._items.splice(beforeIndex, 0, removedItem);
			for (let i = beforeIndex; i <= index; i++) {
				this._itemsToIndices.set(this._items[i]!, i);
			}
		}
	}

	/** The item properties may have changed, so this resorts them. Throws an error if ordering is not 'sorted'. O(n). */
	resort() {

		// Make sure we're using 'sorted' ordering.
		if (this._ordering !== 'sorted') {
			throw new Error("Items can only resorted if ordering is 'sorted'.");
		}

		// Sort the array.
		Sort.sort(this._items, this._isLess!, (_array, i, j) => {
			this._itemsToIndices.set(this._items[i]!, i);
			this._itemsToIndices.set(this._items[j]!, j);
		});
	}

	/** The list of items. */
	private _items: Item[] = [];

	/** The item-to-index mapping. */
	private _itemsToIndices = new Map<ItemReadOnly, number>();

	/** The ordering. */
	private _ordering: 'unordered' | 'ordered' | 'sorted' = 'unordered';

	/** An isLess function if the items are kept sorted. If undefined, the set is unsorted. */
	private _isLess: ((lhs: ItemReadOnly, rhs: ItemReadOnly) => boolean) | undefined;
}
