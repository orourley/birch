import { FastSet } from '../../internal';

/** Factory container for items. */
export class Items<Item extends ItemReadOnly, ItemReadOnly = Item> {

	/** Constructor. See Items class for ordering and isLess. */
	constructor(createItem: () => Item, destroyItem: (item: Item) => void) {

		// Save the create and destroy functions.
		this._createItem = createItem;
		this._destroyItem = destroyItem;
	}

	/** Destroys the items. */
	zzDestroy() {
		for (let i = 0, l = this._items.getArray().length; i < l; i++) {
			this._destroyItem(this._items.getArray()[i]!);
		}
		this._items.clear();
	}

	/** Returns the item array that can be iterated over (but not modified). */
	getArray(): readonly Item[] {
		return this._items.getArray();
	}

	/** Returns true if this has the item. O(1). */
	has(item: ItemReadOnly): boolean {
		return this._items.has(item);
	}

	/** Creates a new item. */
	create(): Item {
		const item = this._createItem();
		this._items.add(item);
		return item;
	}

	/** Destroys an item. O(1) if unordered, otherwise O(n). */
	destroy(item: Item) {
		this._items.remove(item);
		this._destroyItem(item);
	}

	/** Returns the ordering: 'unordered' means the order doesn't matter, 'ordered' means to keep
	 *  the same order as first to last added (except for moveBefore calls),
	 *  and 'sorted' makes the array stay sorted. O(1). */
	getOrdering(): 'unordered' | 'ordered' | 'sorted' {
		return this._items.getOrdering();
	}

	/** Set the ordering: 'unordered' means the order doesn't matter, 'ordered' means to keep
	 *  the same order as first to last added (except for moveBefore calls),
	 *  and 'sorted' makes the array stay sorted using the provided isLess function.
	 *  Defaults to 'unordered'. O(n) If sorting for the first time, otherwise O(1).
	 *  It uses insertion sort. */
	setOrdering(ordering: 'unordered' | 'ordered' | 'sorted', isLess?: (lhs: ItemReadOnly, rhs: ItemReadOnly) => boolean) {
		this._items.setOrdering(ordering, isLess);
	}

	/** Move one item before another, or to the end if beforeItem is undefined.
	 *  Throws an error if ordering is not 'ordered'.  O(n) */
	moveBefore(item: ItemReadOnly, beforeItem?: ItemReadOnly) {
		this._items.moveBefore(item, beforeItem);
	}

	/** The item properties may have changed, so this resorts them. Throws an error if ordering is not 'sorted'. O(n). */
	resort() {
		this._items.resort();
	}

	/** The set of items. */
	private _items = new FastSet<Item, ItemReadOnly>();

	/** The create item function. */
	private _createItem: () => Item;

	/** The destroy item function. */
	private _destroyItem: (item: Item) => void;
}
