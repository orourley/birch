import { FastSet } from './fast_set';

/** A readonly map for key value entries. No GC iteration. */
export interface FastMapReadOnly<KeyReadOnly, ValueReadOnly> {

	/** Returns true if the *key* is in the map. O(1). */
	has: (key: KeyReadOnly) => boolean;

	/** Returns the *value* of the *key*. O(1). */
	get: (key: KeyReadOnly) => ValueReadOnly | undefined;

	/** Returns the entry array that can be iterated over (but not modified). */
	getArray: () => readonly FastMap.Entry<KeyReadOnly, ValueReadOnly>[];
}

/** A map for key value entries. No GC iteration. */
export class FastMap<Key extends KeyReadOnly, Value extends ValueReadOnly, KeyReadOnly = Key, ValueReadOnly = Value>
implements FastMapReadOnly<KeyReadOnly, ValueReadOnly> {

	/** The constructor. Takes an *iterable*. */
	constructor(iterable?: Iterable<[Key, Value]>) {

		if (iterable !== undefined) {
			for (const entry of iterable) {
				this.set(entry[0], entry[1]);
			}
		}
	}

	/** Returns the entry array that can be iterated over (but not modified). */
	getArray(): readonly FastMap.Entry<Key, Value>[] {
		return this._keyList.getArray();
	}

	/** Returns true if the *key* is in the map. O(1). */
	has(key: KeyReadOnly): boolean {
		return this._keyMap.has(key);
	}

	/** Returns the *value* of the *key*. O(1). */
	get(key: KeyReadOnly): Value | undefined {
		const entry = this._keyMap.get(key);
		if (entry !== undefined) {
			return entry.value;
		}
		else {
			return undefined;
		}
	}

	/** Sets the *key* to the *value*. O(1). */
	set(key: Key, value: Value) {
		const entry = this._keyMap.get(key);
		if (entry === undefined) {
			const newEntry = new FastMap.EntryInternal(key, value);
			this._keyMap.set(key, newEntry);
			this._keyList.add(newEntry);
		}
		else {
			entry.value = value;
		}
	}

	/** Removes the *key*. Returns true if the *key* was in the map. O(1). */
	remove(key: KeyReadOnly): boolean {
		const entry = this._keyMap.get(key);
		if (!entry) {
			return false;
		}
		this._keyMap.delete(key);
		this._keyList.remove(entry);
		return true;
	}

	/** Deletes all values. O(n) */
	clear() {
		this._keyMap.clear();
		this._keyList.clear();
	}

	/** The key as a map, for referring to by key. */
	private _keyMap = new Map<KeyReadOnly, FastMap.EntryInternal<Key, Value>>();

	/** The set of items. */
	private _keyList = new FastSet<FastMap.EntryInternal<Key, Value>, FastMap.EntryInternal<KeyReadOnly, ValueReadOnly>>();
}

export namespace FastMap {

	/** An entry in the fast map. */
	export interface Entry<Key, Value> {

		/** The key. */
		readonly key: Key;

		/** The value. */
		readonly value: Value;
	}

	/** An entry in the fast map. This is the internal version. */
	export class EntryInternal<Key, Value> implements Entry<Key, Value> {

		/** The key. */
		key: Key;

		/** The value. */
		value: Value;

		constructor(key: Key, value: Value) {
			this.key = key;
			this.value = value;
		}
	}
}
