import { FastSet } from '../../internal';

/** A set of callbacks that can take any number of params (all same format). */
export class CallbacksAsync<T extends (...arg0: unknown[]) => Promise<void>> extends FastSet<(...args: Parameters<T>) => Promise<void>> {

	/** Calls each of the callbacks. */
	async call(...args: Parameters<T>): Promise<void> {
		if (this._calling) {
			throw new Error('You cannot call callbacks from within those same callbacks.');
		}
		this._calling = true;
		const promises = [];
		const array = this.getArray();
		for (let i = 0, l = array.length; i < l; i++) {
			promises.push(array[i]!(...args));
		}
		await Promise.all(promises);
		this._calling = false;
	}

	// Whether or not we're calling the callbacks. This is to prevent callbacks from calling these same callbacks.
	private _calling: boolean = false;
}
