import { FastSet } from '../../internal';

/** A set of callbacks that can take any number of params (all same format). */
export class Callbacks<T extends (...args: Parameters<T>) => void> extends FastSet<(...args: Parameters<T>) => void> {

	/** Calls each of the callbacks. */
	call(...args: Parameters<T>) {
		if (this._calling) {
			throw new Error('You cannot call callbacks from within those same callbacks.');
		}
		this._calling = true;
		const array = this.getArray();
		for (let i = 0, l = array.length; i < l; i++) {
			array[i]!(...args);
		}
		this._calling = false;
	}

	// Whether or not we're calling the callbacks. This is to prevent callbacks from calling these same callbacks.
	private _calling: boolean = false;
}
