/** Gets random names of hex strings. */
export class RandomName {

	/** Get a random hex string. Defaults to 16 digits for 64-bits. */
	static get(length: number = 16): string {
		let s = '';
		const c = '0123456789abcdef';
		const values = crypto.getRandomValues(new Uint8Array(Math.ceil(length / 2)));
		for (let i = 0; i < length; i++) {
			if (i % 2 === 0) {
				s += c[values[i / 2]! % 16];
			}
			else {
				// eslint-disable-next-line no-bitwise
				s += c[(values[(i - 1) / 2]! >> 4) % 16];
			}
		}
		return s;
	}
}
