import { CollectionBase } from './collection_base';
import { FastMap } from './containers/fast_map';
import { FastOrderedSet, type FastOrderedSetReadOnly } from './fast_ordered_set';

/** A typed collection of optionally named items, with user supplied creation and destruction functions. */
export class CollectionTyped<Item extends Record<string, unknown>> extends CollectionBase<Item> {

	/** Constructs *this*. */
	constructor(createItem: (type: new (...args: unknown[]) => Item, name: string | undefined) => Item,
		destroyItem: (item: Item) => void, postCreateItem?: (item: Item) => void) {
		super(destroyItem);
		this._createItem = createItem;
		this._postCreateItem = postCreateItem;
	}

	/** Creates a new item with an optional name. */
	create<Type extends Item>(type: new (...args: unknown[]) => Type, name?: string): Type {
		// Create the new item.
		const newItem = this._createItem(type, name) as Type;
		// Add it to the collection.
		this.addItem(newItem, name);
		// Add it to the types to items mapping.
		let ancestorType = type;
		while (true) {
			let itemsOfType = this._typesToItems.get(ancestorType);
			if (itemsOfType === undefined) {
				itemsOfType = new FastOrderedSet();
				this._typesToItems.set(ancestorType, itemsOfType);
			}
			itemsOfType.add(newItem);
			ancestorType = Object.getPrototypeOf(ancestorType);
			if (ancestorType === Function.prototype) {
				break;
			}
		}
		// Call the post-create function.
		if (this._postCreateItem) {
			this._postCreateItem(newItem);
		}
		// Return it.
		return newItem;
	}

	/** Destroys an item. Takes the item or its name. */
	override destroy(nameOrItem: string | Item | undefined): boolean {
		// Get the item, if from a name.
		let item: Item | undefined;
		if (typeof nameOrItem === 'string') {
			item = this.get(nameOrItem);
		}
		else {
			item = nameOrItem;
		}
		// Destroy the item.
		const hasItem = super.destroy(item);
		if (!hasItem) {
			return false;
		}
		// Remove all of its ancestors from the typesToItems list.
		let ancestorType = item!.constructor as new () => Item;
		while (true) {
			const itemsOfType = this._typesToItems.get(ancestorType)!;
			itemsOfType.remove(item!);
			ancestorType = Object.getPrototypeOf(ancestorType);
			if (ancestorType === Function.prototype) {
				break;
			}
		}
		return true;
	}

	/** Gets the first item of the given *type*. */
	getFirstOfType<Type extends Item>(type: new (...args: unknown[]) => Type): Type | undefined {
		return this._typesToItems.get(type)?.getIndex(0) as (Type | undefined);
	}

	/** Gets the items of the given *type*. Returns undefined if there are none. */
	getAllOfType<Type extends Item>(type: new (...args: unknown[]) => Type): FastOrderedSetReadOnly<Type> | undefined {
		return this._typesToItems.get(type) as FastOrderedSetReadOnly<Type> | undefined;
	}

	/** The create item function. */
	private _createItem: (type: new (...args: unknown[]) => Item, name: string | undefined) => Item;

	/** The function called after an item has been created and added to the collection. */
	private _postCreateItem: ((item: Item) => void) | undefined;

	/** The mapping from types to items. */
	private _typesToItems = new FastMap<new (...args: unknown[]) => Item, FastOrderedSet<Item>>();
}
