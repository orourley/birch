/** A class for getting unique IDs. */
export class UniqueId {

	/** Gets an ID from the list of free IDs. */
	static get(): number {
		const id = this._freeIds.length > 0 ? this._freeIds.pop()! : this._numUsedIds;
		this._numUsedIds += 1;
		return id;
	}

	/** Releases the ID to the list of free IDs. */
	static release(id: number) {
		this._freeIds.push(id);
		this._numUsedIds -= 1;
	}

	/** The list of free ids that are gaps in the list. */
	private static readonly _freeIds: number[] = [];

	/** The total number of used ids. */
	private static _numUsedIds: number = 0;
}

/** An object meant to be extended to provide a unique ID for the lifetime of the object. */
export class ObjectWithUniqueId {

	/** The unique id. */
	readonly id: number;

	/** Constructs the object. */
	constructor() {
		// Get a unique id for the shader.
		this.id = UniqueId.get();
	}

	/** Destroys the object. */
	protected destroy() {
		UniqueId.release(this.id);
	}
}
