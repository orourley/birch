import { FastIterable, FastIterableBase } from './fast_iterable';

export class FastOrderedSetReadOnly<Value> extends FastIterableBase<Value> {

	/** The head node. */
	protected head: FastOrderedSetReadOnly.Node<Value> | undefined = undefined;

	/** A mapping from values to nodes for easier access. */
	protected valuesToNodes = new Map<Value, FastOrderedSetReadOnly.Node<Value>>();

	/** Gets the number of values. */
	size(): number {
		return this.valuesToNodes.size;
	}

	/** Gets the *i*th value. Note that this takes O(i) time. */
	getIndex(i: number): Value | undefined {
		let node = this.head;
		while (i >= 1 && node !== undefined) {
			node = node.next;
			i--;
		}
		if (node !== undefined) {
			return node.value;
		}
		else {
			return undefined;
		}
	}

	/** Returns true if the *value* is in the set. O(1). */
	has(value: Value): boolean {
		return this.valuesToNodes.has(value);
	}

	/** Gets the list of all values. They must be string-able. O(n). */
	override toString(): string {
		let s = '[';
		let n = this.head;
		while (n !== undefined) {
			if (s !== '[') {
				s += ', ';
			}
			s += n.value;
			n = n.next;
		}
		return `${s}]`;
	}

	/** Creates a new iterator. */
	protected _createNewIterator(): FastOrderedSetReadOnly.Iterator<Value> {
		return new FastOrderedSetReadOnly.Iterator(this._getHead.bind(this));
	}

	/** Gets the head. */
	private _getHead(): FastOrderedSetReadOnly.Node<Value> | undefined {
		return this.head;
	}
}

export namespace FastOrderedSetReadOnly {

	export class Iterator<Value> extends FastIterable.FastIterator<Value> {

		/** The constructor. */
		constructor(getHead: () => Node<Value> | undefined) {
			super();
			this._getHead = getHead;
		}

		reset(): boolean {
			this.node = this._getHead();
			if (this.node !== undefined) {
				this.value = this.node.value;
				return true;
			}
			return false;
		}

		increment(): boolean {
			if (this.node !== undefined && this.node.next !== undefined) {
				this.node = this.node.next;
				this.value = this.node.value;
				return true;
			}
			return false;
		}

		finish() {
		}

		private _getHead: () => Node<Value> | undefined;

		private node: Node<Value> | undefined = undefined;
	}

	export class Node<Value> {
		prev: Node<Value> | undefined = undefined;

		next: Node<Value> | undefined = undefined;

		value: Value;

		constructor(value: Value) {
			this.value = value;
		}
	}
}

export class FastOrderedSet<Value> extends FastOrderedSetReadOnly<Value> {

	/** Constructs the FastList. Takes an *iterable*. */
	constructor(iterable?: Iterable<Value>) {
		super();
		if (iterable !== undefined) {
			for (const value of iterable) {
				this.add(value);
			}
		}
	}

	/** Adds a node with the *value* placed before the *before* node or at the end. If the value already exists, it re-adds it. O(1) */
	add(value: Value, before?: Value) {
		if (this.valuesToNodes.has(value)) {
			this.remove(value);
		}
		const newNode = new FastOrderedSet.Node<Value>(value);
		if (before !== undefined) {
			const beforeNode = this.valuesToNodes.get(before);
			if (beforeNode === undefined) {
				throw new Error(`The value ${before} was not found.`);
			}
			if (beforeNode.prev !== undefined) { // Not the head node.
				newNode.prev = beforeNode.prev;
				beforeNode.prev.next = newNode;
			}
			else { // beforeNode is the head
				this.head = newNode;
			}
			newNode.next = beforeNode;
			beforeNode.prev = newNode;
		}
		else {
			if (this._tail !== undefined) {
				this._tail.next = newNode;
				newNode.prev = this._tail;
			}
			else {
				this.head = newNode;
			}
			this._tail = newNode;
		}
		this.valuesToNodes.set(value, newNode);
	}

	/** Removes the *value*. Returns true if the *value* was in the set. O(1). */
	remove(value: Value): boolean {
		const node = this.valuesToNodes.get(value);
		if (node === undefined) {
			return false;
		}
		if (node.prev !== undefined) {
			node.prev.next = node.next;
		}
		else {
			this.head = node.next;
		}
		if (node.next !== undefined) {
			node.next.prev = node.prev;
		}
		else {
			this._tail = node.prev;
		}
		node.prev = undefined;
		node.next = undefined;
		this.valuesToNodes.delete(value);
		return true;
	}

	/** Deletes all values. O(1) */
	clear() {
		this.valuesToNodes.clear();
		this.head = undefined;
		this._tail = undefined;
	}

	/** Sorts the values for iteration based on the *isLess* function. Uses insertion sort. */
	sort(isLess: (a: Value, b: Value) => boolean) {
		if (this.head === undefined) {
			return;
		}
		let n = this.head.next;
		while (n !== undefined) {
			let m = n.prev;
			while (m !== undefined && isLess(n.value, m.value)) {
				const t = m.value;
				m.value = m.next!.value;
				m.next!.value = t;
				this.valuesToNodes.set(m.value, m);
				this.valuesToNodes.set(m.next!.value, m.next!);
				m = m.prev;
			}
			n = n.next;
		}
	}

	/** The tail node. */
	private _tail: FastOrderedSetReadOnly.Node<Value> | undefined = undefined;
}
