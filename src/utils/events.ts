/** A base class that produces events. */
export abstract class EventSource {

	/** The event sent when the event source has been destroyed.
	 *  The event source has already been unsubscribed. */
	static readonly Destroyed = Symbol('Destroyed');

	/** Destroys the event source. */
	destroy() {
		this.sendEvent(EventSource.Destroyed);
	}

	/** Subscribes an event sink to a event source's events.
	 *  If event is undefined, it subscribes to all events.
	 *  @internal */
	zzSubscribeToEvents(eventSink: EventSink, event?: symbol) {
		this._subscriptions.push(new Subscription(eventSink, event));
	}

	/** Unsubscribes an event sink from a event source's events.
	 *  If event is undefined, it will unsubscribe the event sink from all events.
	 *  @internal */
	zzUnsubscribeFromEvents(eventSink: EventSink, event?: symbol) {
		for (let i = 0, l = this._subscriptions.length; i < l; i++) {
			const subscription = this._subscriptions[i]!;
			if (subscription.eventSink === eventSink && (subscription.event === event || event === undefined)) {
				this._subscriptions.splice(i, 1);
				break;
			}
		}
	}

	/** Sends an event to all subscribed event sinks. */
	protected sendEvent(event: symbol) {
		for (const subscription of this._subscriptions) {
			if (subscription.event === undefined || subscription.event === event) {
				subscription.eventSink.processEventBase(this, event);
			}
		}
	}

	/** Subscribed event sinks. */
	private _subscriptions: Subscription[] = [];
}

/** A subscription in an event source. */
class Subscription {

	/** The event sink to which to subscribe. */
	eventSink: EventSink;

	/** The event to which to subscribe. */
	event: symbol | undefined;

	/** Constructs the subscription with an event sink and event. */
	constructor(eventSink: EventSink, event: symbol | undefined) {
		this.eventSink = eventSink;
		this.event = event;
	}
}

/** A base class that consumes events. */
export abstract class EventSink {

	/** Called when an event sink receives an event to which it was subscribed. */
	abstract processEvent(eventSource: EventSource, event: symbol): void;

	/** Destroys the event sink. */
	destroy() {
		// Unsubscribe from all events.
		for (const eventSource of this._subscribedEventSources) {
			eventSource.zzUnsubscribeFromEvents(this);
		}
	}

	/** Called when an event sink receives an event to which it was subscribed. */
	processEventBase(eventSource: EventSource, event: symbol) {
		if (event === EventSource.Destroyed) {
			this._subscribedEventSources.delete(eventSource);
		}
		this.processEvent(eventSource, event);
	}

	/** Subscribes to an EventSource's events. */
	protected subscribeToEvents(eventSource: EventSource, event?: symbol) {
		if (!this._subscribedEventSources.has(eventSource)) {
			eventSource.zzSubscribeToEvents(this, event);
			this._subscribedEventSources.add(eventSource);
		}
	}

	/** Unsubscribes from an EventSource's events. */
	protected unsubscribeFromEvents(eventSource: EventSource, event?: symbol) {
		if (this._subscribedEventSources.has(eventSource)) {
			eventSource.zzUnsubscribeFromEvents(this, event);
			this._subscribedEventSources.delete(eventSource);
		}
	}

	/** The set of subscribed EventSources. */
	private _subscribedEventSources = new Set<EventSource>();
}
