// import { ResourceCache } from './resource_cache';

// /** A resource loader for preloading and swapping out cached resources. */
// export class ResourceLoader<Type> {
// 	constructor(params: {
// 			resourceCache: ResourceCache<Type>,
// 			nameToUrlFunction: (name: string) => string }) {
// 		// Save the params.
// 		this._resourceCache = params.resourceCache;
// 		this._nameToUrlFunction = params.nameToUrlFunction;
// 	}

// 	/** Loads a list of resource names. */
// 	async loadResources(names: string[]): Promise<void> {
// 		const loadingPromises: Promise<Type>[] = [];
// 		for (let i = 0; i < names.length; i++) {
// 			// Get the name and url.
// 			const name = names[i];
// 			const url = this._nameToUrlFunction(name);
// 			// If it's already loading,
// 			if (this._resourcesStillLoading.has(name)) {
// 				// Add it to the list of promises to wait on.
// 				loadingPromises.push(this._resourcesStillLoading.get(name)!);
// 			}
// 			// If it isn't yet loaded or loading,
// 			else if (!this._namesToResources.has(name)) {
// 				// Start the loading process.
// 				const loadingPromise = this._resourceCache.loadResource(url).then((resource: Type) => {
// 					this._resourcesStillLoading.delete(name);
// 					this._namesToResources.set(name, resource);
// 					return resource;
// 				});
// 				// Add it to the loading list.
// 				this._resourcesStillLoading.set(name, loadingPromise);
// 				// Add it to the list of promises to wait on.
// 				loadingPromises.push(loadingPromise);
// 			}
// 		}
// 		await Promise.all(loadingPromises);
// 	}

// 	/** Unloads a list of resource names. */
// 	unloadResources(names: string[]) {
// 		for (let i = 0; i < names.length; i++) {
// 			const name = names[i];
// 			const resource = this._namesToResources.get(name);
// 			// If the resource is loaded,
// 			if (resource !== undefined) {
// 				// Make sure it isn't being used anywhere else.
// 				const useCount = this._resourceCache.getUseCount(resource);
// 				if (useCount > 1) {
// 					throw new Error(`Resource with name ${name} is still in use ${useCount - 1} times.`);
// 				}
// 				// Unload the resource.
// 				this._resourceCache.releaseResource(resource);
// 				// Remove it from the loaded list.
// 				this._namesToResources.delete(name);
// 			}
// 			// If the resource is still loading, continue the loading, and unload it afterward.
// 			else if (this._resourcesStillLoading.has(name)) {
// 				// When it finishes loading, get the resulting resource and unload it.
// 				this._resourcesStillLoading.get(name)!.then((resource: Type) => {
// 					// Make sure it isn't being used anywhere else.
// 					const useCount = this._resourceCache.getUseCount(resource);
// 					if (useCount > 1) {
// 						throw new Error(`Resource with name ${name} is still in use ${useCount - 1} times.`);
// 					}
// 					// Unload the resource.
// 					this._resourceCache.releaseResource(resource);
// 					// Remove it from the loaded list.
// 					this._namesToResources.delete(name);
// 				});
// 				// Remove it from the loading list immediately so that we don't try to unload it more than once.
// 				this._resourcesStillLoading.delete(name);
// 			}
// 		}
// 	}

// 	/** Gets whether the cache has the named resource. */
// 	isLoaded(name: string): boolean {
// 		return this._namesToResources.has(name);
// 	}

// 	/** The cache. */
// 	private _resourceCache: ResourceCache<Type>;

// 	/** The name to url function. */
// 	private _nameToUrlFunction: (name: string) => string;

// 	/** The mapping of names to promises still loading. */
// 	private _resourcesStillLoading: Map<string, Promise<Type>> = new Map();

// 	/** The mapping of names to resources that are loaded. */
// 	private _namesToResources: Map<string, Type> = new Map();
// }
