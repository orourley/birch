// /** A type of a item. */
// export type Type<Item, Parent extends object> = new (parent: Parent, type: Type<Item, Parent>, name: string | undefined) => Item;

// /** An interface required for all item classes. */
// interface ItemInterface<ItemType, Parent extends object> {

// 	/** Gets the type. */
// 	getType(): Type<ItemType, Parent>;

// 	/** Gets the name. */
// 	getName(): string;
// }

// /** A name-type mapping with adding and removing of items. */
// export class NamedTypedCollection<ItemBase extends ItemInterface<ItemBase, Parent>, Parent extends object> {

// 	/** Constructs the name-type map. The constructor is a string used for nicer error messages. */
// 	constructor(parent: object, typeAsString: string) {
// 		this.parent = parent;
// 		this.typeAsString = typeAsString;
// 	}

// 	/** Gets the item of the given name. */
// 	getItemByName<Item extends ItemBase>(name: string): Item | undefined {
// 		return this.itemsByName.get(name) as Item | undefined;
// 	}

// 	/** Gets the items of the given type. */
// 	getItemsByType<ItemType extends Type<ItemBase, Parent>>(itemType: ItemType): ReadonlyArray<ItemType> | undefined {
// 		return this.itemsByType.get(itemType) as (ReadonlyArray<ItemType> | undefined);
// 	}

// 	/** Gets an iterator to all of the items. */
// 	getAllItemsIterator(): IterableIterator<ItemBase> {
// 		return this.itemsByName.values();
// 	}

// 	/** Returns true if the this has the item. */
// 	has(item: ItemBase): boolean {
// 		return this.itemsByName.has(item.getName());
// 	}

// 	/** Adds an item. Throws if an item with the name exists. */
// 	add<ItemType extends Type<ItemBase, Parent>>(itemType: ItemType, parent: Parent, name: string | undefined) {

// 		// If no name is given, use the kebab-case of the type.
// 		if (name === undefined) {
// 			name = itemType.name.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);
// 		}

// 		// Check if the name doesn't already exist.
// 		if (this.itemsByName.has(name)) {
// 			throw new Error(`A ${this.typeAsString} with name "${name}" already exists.`);
// 		}

// 		// Check if the name doesn't already exist in the pending adds.
// 		for (let i = 0, l = this.pendingAdds.length; i < l; i++) {
// 			if (this.pendingAdds[i].name === name) {
// 				throw new Error(`A ${this.typeAsString} with name "${name}" already exists in the pending additions.`);
// 			}
// 		}

// 		// Add it to the pending adds.
// 		this.pendingAdds.push({
// 			name,
// 			type
// 		});

// 		// Create the item.
// 		const item = new itemType(parent, itemType, name);

// 		// Add the item to the name-item map.
// 		this.itemsByName.set(name, item);

// 		// Add the item to the type-items map.
// 		let itemsOfType = this.itemsByType.get(itemType);
// 		if (itemsOfType === undefined) {
// 			itemsOfType = [];
// 			this.itemsByType.set(itemType, itemsOfType);
// 		}
// 		itemsOfType.push(item);

// 		// Return the item.
// 		return item;
// 	}

// 	/** Removes an item. Throws if the item does not exist. */
// 	remove(item: ItemBase) {

// 		// Remove the item from the name-item map.
// 		const name = item.getName();
// 		if (!this.itemsByName.delete(name)) {
// 			throw new Error(`No ${this.typeAsString} with the name "${name}" exists.`);
// 		}

// 		// Remove the item from the type-item map.
// 		const itemType = item.getType();
// 		const itemsOfType = this.itemsByType.get(itemType) as ItemBase[];
// 		for (let i = 0, l = itemsOfType.length; i < l; i++) {
// 			if (itemsOfType[i].getName() === name) {
// 				itemsOfType.splice(i, 1);
// 				if (itemsOfType.length === 0) {
// 					this.itemsByType.delete(itemType);
// 				}
// 				break;
// 			}
// 		}
// 	}

// 	/** Clears all of the items. */
// 	clear() {
// 		this.itemsByName.clear();
// 		this.itemsByType.clear();
// 	}

// 	getPendingAdds(): ReadonlyArray<{ name: string, type: Type<ItemBase, Parent>}> {
// 		return this.pendingAdds;
// 	}

// 	getPendingRemoves(): ItemBase[] {
// 		return this.pendingRemoves;
// 	}

// 	/** Processes all of the pending adds and removes. */
// 	processAddsAndRemoves() {

// 		// Process the removes.
// 		for (let i = 0, l = this.pendingRemoves.length; i < l; i++) {
// 			const item = this.pendingRemoves[i];

// 			// Remove the item from the name-item map.
// 			const name = item.getName();
// 			this.itemsByName.delete(name);

// 			// Remove the item from the type-item map.
// 			const itemType = item.getType();
// 			const itemsOfType = this.itemsByType.get(itemType) as ItemBase[];
// 			for (let i = 0, l = itemsOfType.length; i < l; i++) {
// 				if (itemsOfType[i] === item) {
// 					itemsOfType.splice(i, 1);
// 					if (itemsOfType.length === 0) {
// 						this.itemsByType.delete(itemType);
// 					}
// 					break;
// 				}
// 			}
// 		}
// 		this.pendingRemoves = [];

// 		// Process the adds.
// 		for (let i = 0, l = this.pendingAdds.length; i < l; i++) {
// 			const entry = this.pendingAdds[i];
// 			const name = entry.name;
// 			const type = entry.type;

// 			// Create the item.
// 			const item = new entry.type(this.parent, type, name);

// 			// Add the item to the name-item map.
// 			this.itemsByName.set(name, item);

// 			// Add the item to the type-items map.
// 			let itemsOfType = this.itemsByType.get(type);
// 			if (itemsOfType === undefined) {
// 				itemsOfType = [];
// 				this.itemsByType.set(type, itemsOfType);
// 			}
// 			itemsOfType.push(item);
// 		}
// 		this.pendingAdds = [];
// 	}

// 	/** The parent of the items. */
// 	private parent: object;

// 	/** The type as a string, used for exception throwing. */
// 	private typeAsString: string;

// 	/** The items keyed by name. */
// 	private itemsByName: Map<string, ItemBase> = new Map();

// 	/** The items keyed by type (there may be multiple). */
// 	private itemsByType: Map<Type<ItemBase, Parent>, ItemBase[]> = new Map();

// 	/** The pending adds, by name, type, and item. */
// 	private pendingAdds: Array<{ name: string, type: Type<ItemBase, Parent>}> = [];

// 	/** The pending removes, by name. */
// 	private pendingRemoves: ItemBase[] = [];
// }
