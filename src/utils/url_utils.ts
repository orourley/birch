export class UrlUtils {

	/** Gets a full URL from a uri, which may be relative or not, using a baseUrl as a relative reference. */
	static getFullUrlFromUri(uri: string, baseUrl: string): string {
		const protocolPat = /^\w+:\/\//u;
		if (protocolPat.test(uri)) { // already absolute url
			return uri;
		}
		else if (uri.startsWith('//')) { // protocol-relative uri
			const result = protocolPat.exec(baseUrl);
			if (result === null) {
				throw new Error(`${baseUrl} is not an absolute URL.`);
			}
			return result[0] + uri.substring(2);
		}
		else if (uri.startsWith('/')) { // domain-relative uri
			const protocolAndHostPat = /^\w+:\/\/[^/]*/u;
			const result = protocolAndHostPat.exec(baseUrl);
			if (result === null) {
				throw new Error(`${baseUrl} is not an absolute URL.`);
			}
			return result[0] + uri.substring(1);
		}
		else { // folder-relative uri
			return baseUrl.substring(0, baseUrl.lastIndexOf('/') + 1) + uri;
		}
	}
}
