/** An interface for creating iterables that don't generate garbage. */
export interface FastIterable<Value> extends Iterable<Value> {

	/** Returns an iterator. */
	[Symbol.iterator]: () => FastIterable.FastIterator<Value>;
}

/** A base class for ordered containers that have iterators that get reused. */
export abstract class FastIterableBase<Value> implements FastIterable<Value> {

	/** Creates a new iterator. */
	protected abstract _createNewIterator(): FastIterable.FastIterator<Value>;

	/** Returns an iterator. */
	[Symbol.iterator](): FastIterable.FastIterator<Value> {

		// Find an existing iterator that isn't iterating and return it.
		let iterator: FastIterable.FastIterator<Value> | undefined;
		for (let i = 0, l = this._iterators.length; i < l; i++) {
			if (!this._iterators[i]!.zzIterating) {
				iterator = this._iterators[i];
				break;
			}
		}

		// Didn't find an existing non-iterating iterator, so create a new one.
		if (iterator === undefined) {
			iterator = this._createNewIterator();
			this._iterators.push(iterator);
		}

		// Reset the iterator to the beginning for looping.
		iterator.zzIterating = true;
		iterator.done = true;

		// Return the iterator.
		return iterator;
	}

	/** The list of created iterators. It should only ever get as big as the number of nested loops. */
	private _iterators: FastIterable.FastIterator<Value>[] = [];
}

export namespace FastIterable {

	/** The ordered iterator. */
	export abstract class FastIterator<Value> implements Iterator<Value> {

		/** The value of the iterator. Force it to have Value type, since it will always have a value during iterations. */
		value!: Value;

		/** If true, the iterator has reached the end of the loop. */
		done: boolean = true;

		/** If true, the iterator is currently going through a loop. */
		zzIterating: boolean = false;

		/** Resets the iterator to the beginning, setting the new value. Returns true if there was a beginning element. */
		abstract reset(): boolean;

		/** Increments the iterator, setting the new value. Returns false if it could not increment. */
		abstract increment(): boolean;

		/** Close up what is necessary for the iterator. */
		abstract finish(): void;

		/** Increments the iterator. */
		next(): this {

			// If done is true, it means the iterator has yet to start.
			if (this.done) {
				this.zzIterating = true;
				this.done = false;
				const hasFirstElement = this.reset();
				if (!hasFirstElement) {
					this.zzIterating = false;
					this.done = true;
				}
			}
			// Otherwise, it's in the middle of iterating.
			else {
				this.done = !this.increment();
				if (this.done) {
					this.zzIterating = false;
					this.value = undefined!;
					this.finish();
				}
			}

			// Return the iterator.
			return this;
		}

		/** Close up the iterator. */
		return(): this {
			this.zzIterating = false;
			this.done = true;
			this.value = undefined!;
			this.finish();
			return this;
		}
	}
}
