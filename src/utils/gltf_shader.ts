export const gltfShader = `
[properties]
blending = "none"
depthTest = "less"

[attributeLocations]
position = 0
normal = 1
tangent = 2
uv = 3

[code]
vertex = """
#version 300 es

in vec3 position;
in vec3 normal;
in vec2 uv;

uniform stage {
	mat4 worldToCamera;
	mat4 cameraToNdc;
};
uniform model {
	mat4 localToWorld;
};

void main() {
	gl_Position = cameraToNdc * worldToCamera * localToWorld * vec4(position, 1.0);
}
"""

fragment = """
#version 300 es
precision mediump float;

out vec4 outColor;

void main() {
	outColor = vec4(0.0, 0.58, 0.86, 1.0);
}
"""
`;
