/** A reference counted value. */
export class Counted<Value> {

	/** The value. */
	readonly value: Value;

	/** Constructs the class. */
	constructor(value: Value) {
		this.value = value;
		this.count = 1;
	}

	/** Gets the count. */
	getCount(): number {
		return this.count;
	}

	/** Increments the count. */
	incCount() {
		this.count += 1;
	}

	/** Decrements the count. */
	decCount() {
		this.count -= 1;
	}

	/** The count. */
	private count: number;
}
