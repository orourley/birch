export class Num {

	/** The number that when multiplied converts from radians to degrees. */
	static readonly RadToDeg = 57.29577951308232;

	/** The number that when multiplied converts from degrees to radians. */
	static readonly DegToRad = 0.017453292519943295;

	/** Two pi. */
	static readonly TwoPi = 2 * Math.PI;

	/** Returns *x* clamped between *a* and *b*. */
	static clamp(x: number, a: number, b: number): number {
		return x < a
			? a
			: b < x ? b : x;
	}

	/** Returns *x* clamped between 0 and 1. */
	static clamp01(x: number): number {
		return x < 0
			? 0
			: x > 1 ? 1 : x;
	}

	/** Returns the point *x* between *a* and *b* as if they were a cycle. */
	static wrap(x: number, a: number, b: number): number {
		let phase = (x - a) % (b - a);
		if (phase < 0) {
			phase += b - a;
		}
		return a + phase;
	}

	/** Returns the angle *a* between 0 and 2 * PI as if they were a cycle. */
	static wrapAngle(a: number): number {
		let phase = a % this.TwoPi;
		if (phase < 0) {
			phase += this.TwoPi;
		}
		return phase;
	}

	/** Returns the *u* lerped value between *a* and *b*. */
	static lerp(a: number, b: number, u: number): number {
		return a === b ? a : (a * (1 - u) + b * u);
	}

	/** Linearly interpolates between two angles in radians a and b, following the shortest route. */
	static lerpAngle(a: number, b: number, u: number): number {
		a = this.wrapAngle(a);
		b = this.wrapAngle(b);
		if (b - a > Math.PI) {
			a += 2 * Math.PI;
		}
		if (a - b > Math.PI) {
			b += 2 * Math.PI;
		}
		return this.wrapAngle(this.lerp(a, b, u));
	}

	/** Returns true if *a* is a power of two. */
	static isPow2(a: number): boolean {
		return Math.log2(a) % 1 === 0;
	}

	/** Returns the next power of two that is <= *a*. */
	static floorPow2(a: number): number {
		return 2 ** Math.floor(Math.log2(a));
	}

	/** Returns the next power of two that is >= *a*. */
	static ceilPow2(a: number): number {
		return 2 ** Math.ceil(Math.log2(a));
	}

	/** Returns the angle to get from *a* to *b* in the shortest direction. */
	static angleDiff(a: number, b: number): number {
		const at = this.wrap(a, 0, this.TwoPi);
		const bt = this.wrap(b, 0, this.TwoPi);
		if (at - bt > Math.PI) {
			return bt - at + this.TwoPi;
		}
		else if (bt - at > Math.PI) {
			return bt - at - this.TwoPi;
		}
		else {
			return bt - at;
		}
	}
}
