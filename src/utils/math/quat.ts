/* eslint-disable es-x/no-accessor-properties */

import { Pool, Vec3D, type Vec3DReadOnly } from '../../internal';

/** A quaternion, read-only. */
export interface QuatReadOnly {

	/** Gets the w component. */
	get w(): number;

	/** Gets the x component. */
	get x(): number;

	/** Gets the y component. */
	get y(): number;

	/** Gets the z component. */
	get z(): number;

	/** Gets *this* as an array. */
	get array(): readonly [number, number, number, number];

	/** Returns true if *this* equals *q*. */
	equals: (q: QuatReadOnly) => boolean;

	/** Returns true if at least component of *this* is NaN. */
	isNaN: () => boolean;

	/** Gets the angle of the rotation that *this* represents. */
	angle: () => number;

	/** Gets the dot product between *this* and *q*. */
	dot: (q: QuatReadOnly) => number;

	/** Gets the square norm of *this*. */
	normSq: () => number;

	/** Gets the norm of *this*. */
	norm: () => number;

	/** Returns the angle in radians between *this* and quaternion *q*. */
	angleBetween: (q: QuatReadOnly) => number;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** A quaternion. */
export class Quat implements QuatReadOnly {

	/** The identity quaternion. */
	static readonly Identity: QuatReadOnly = new Quat(1, 0, 0, 0);

	/** A NaN quaternion. */
	static readonly NaN: QuatReadOnly = new Quat(NaN, NaN, NaN, NaN);

	/** Pool for temporary quaternions. */
	static readonly pool = new Pool(Quat);

	/** The constructor. */
	constructor(w: number = 1, x: number = 0, y: number = 0, z: number = 0) {
		this._m = [w, x, y, z];
	}

	/** Gets the w component. */
	get w(): number {
		return this._m[0];
	}

	/** Sets the w component. */
	set w(w: number) {
		this._m[0] = w;
	}

	/** Gets the x component. */
	get x(): number {
		return this._m[1];
	}

	/** Sets the x component. */
	set x(x: number) {
		this._m[1] = x;
	}

	/** Gets the y component. */
	get y(): number {
		return this._m[2];
	}

	/** Sets the y component. */
	set y(y: number) {
		this._m[2] = y;
	}

	/** Gets the z component. */
	get z(): number {
		return this._m[3];
	}

	/** Sets the y component. */
	set z(z: number) {
		this._m[3] = z;
	}

	/** Gets *this* as a [w, x, y, z] array. */
	get array(): [number, number, number, number] {
		return this._m;
	}

	/** Returns true if *this* equals *q*. */
	equals(q: QuatReadOnly): boolean {
		return this._m[0] === q.w && this._m[1] === q.x && this._m[2] === q.y && this._m[3] === q.z;
	}

	/** Returns true if at least component of *this* is NaN. */
	isNaN(): boolean {
		return isNaN(this._m[0]) || isNaN(this._m[1]) || isNaN(this._m[2]) || isNaN(this._m[3]);
	}

	/** Sets *this* to *q*. */
	copy(q: QuatReadOnly) {
		this._m[0] = q.w;
		this._m[1] = q.x;
		this._m[2] = q.y;
		this._m[3] = q.z;
	}

	/** Sets *this* to *w*, *x*, *y*, and *z*. */
	set(w: number, x: number, y: number, z: number) {
		this._m[0] = w;
		this._m[1] = x;
		this._m[2] = y;
		this._m[3] = z;
	}

	/** Sets *this* to the quaternion represented by the rotation *angle* in radians abthis the *axis*, which should be normalized. */
	setFromAxisAngle(axis: Vec3DReadOnly, angle: number) {
		const cosHalfAngle = Math.cos(angle * 0.5);
		const sinHalfAngle = Math.sin(angle * 0.5);
		this._m[0] = cosHalfAngle;
		this._m[1] = sinHalfAngle * axis.x;
		this._m[2] = sinHalfAngle * axis.y;
		this._m[3] = sinHalfAngle * axis.z;
	}

	/** Sets *this* to the quaternion represented by the yaw (*z*), pitch (*x*), and roll (*y*) rotations.
	 *  The order of application is *x*, *z*, then *y*. */
	setFromEulerAngles(yaw: number, pitch: number, roll: number) {
		const cx = Math.cos(pitch * 0.5);
		const sx = Math.sin(pitch * 0.5);
		const cy = Math.cos(roll * 0.5);
		const sy = Math.sin(roll * 0.5);
		const cz = Math.cos(yaw * 0.5);
		const sz = Math.sin(yaw * 0.5);
		this._m[0] = cz * cx * cy - sz * sx * sy;
		this._m[1] = cz * sx * cy - sz * cx * sy;
		this._m[2] = sz * sx * cy + cz * cx * sy;
		this._m[3] = sz * cx * cy + cz * sx * sy;
	}

	/** Sets *this* to the quaternion represented by a rotation from *v1* to *v2*, which should both be normalized. */
	setFromVectorRotation(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		this._m[0] = 1 + v1.dot(v2);
		if (this._m[0] !== 0) {
			Quat.tempVec3D.cross(v1, v2);
		}
		// The vec3Ds v1 and v2 are exactly opposite, so any rotation axis perpendicular to v1 should work.
		else {
			Quat.tempVec3D.perp(v1);
		}
		this._m[1] = Quat.tempVec3D.x;
		this._m[2] = Quat.tempVec3D.y;
		this._m[3] = Quat.tempVec3D.z;
		this.normalize(this);
	}

	/** Sets *this* to the quaternion that represents an orientation frame given by at least two axes, the other being undefined.
	 *  The given axes must be orthonormal. */
	setFromAxes(xAxis: Vec3DReadOnly | undefined, yAxis: Vec3DReadOnly | undefined, zAxis: Vec3DReadOnly | undefined) {

		// Whichever axis is undefined, set that to the cross of the other two.
		let tempXAxis;
		let tempYAxis;
		let tempZAxis;
		if (!xAxis && yAxis && zAxis) {
			tempXAxis = Quat.tempVec3D;
			tempXAxis.cross(yAxis, zAxis);
			tempYAxis = yAxis;
			tempZAxis = zAxis;
		}
		else if (!yAxis && zAxis && xAxis) {
			tempXAxis = xAxis;
			tempYAxis = Quat.tempVec3D;
			tempYAxis.cross(zAxis, xAxis);
			tempZAxis = zAxis;
		}
		else if (!zAxis && xAxis && yAxis) {
			tempXAxis = xAxis;
			tempYAxis = yAxis;
			tempZAxis = Quat.tempVec3D;
			tempZAxis.cross(xAxis, yAxis);
		}
		else {
			throw new Error('Exactly two of the axes must be defined.');
		}

		// Do the math to set the quat.
		const tr = tempXAxis.x + tempYAxis.y + tempZAxis.z;
		if (tr > 0) {
			const S = Math.sqrt(tr + 1.0) * 2; // S = 4 * this._m[0]
			this._m[0] = 0.25 * S;
			this._m[1] = (tempYAxis.z - tempZAxis.y) / S;
			this._m[2] = (tempZAxis.x - tempXAxis.z) / S;
			this._m[3] = (tempXAxis.y - tempYAxis.x) / S;
		}
		else if ((tempXAxis.x > tempYAxis.y) && (tempXAxis.x > tempZAxis.z)) {
			const S = Math.sqrt(1 + tempXAxis.x - tempYAxis.y - tempZAxis.z) * 2; // S = 4 * this._m[1]
			this._m[0] = (tempYAxis.z - tempZAxis.y) / S;
			this._m[1] = 0.25 * S;
			this._m[2] = (tempYAxis.x + tempXAxis.y) / S;
			this._m[3] = (tempZAxis.x + tempXAxis.z) / S;
		}
		else if (tempYAxis.y > tempZAxis.z) {
			const S = Math.sqrt(1 + tempYAxis.y - tempZAxis.z - tempXAxis.x) * 2; // S = 4 * this._m[2]
			this._m[0] = (tempZAxis.x - tempXAxis.z) / S;
			this._m[1] = (tempYAxis.x + tempXAxis.y) / S;
			this._m[2] = 0.25 * S;
			this._m[3] = (tempZAxis.y + tempYAxis.z) / S;
		}
		else {
			const S = Math.sqrt(1 + tempZAxis.z - tempXAxis.x - tempYAxis.y) * 2; // S = 4 * this._m[3]
			this._m[0] = (tempXAxis.y - tempYAxis.x) / S;
			this._m[1] = (tempZAxis.x + tempXAxis.z) / S;
			this._m[2] = (tempZAxis.y + tempYAxis.z) / S;
			this._m[3] = 0.25 * S;
		}
	}

	/** Sets *this* to the inverse of *q*. */
	inverse(q: QuatReadOnly) {
		this._m[0] = q.w;
		this._m[1] = -q.x;
		this._m[2] = -q.y;
		this._m[3] = -q.z;
	}

	/** Sets this to *q1* * *q2*. */
	mult(q1: QuatReadOnly, q2: QuatReadOnly) {
		const w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;
		const x = q1.x * q2.w + q1.w * q2.x - q1.z * q2.y + q1.y * q2.z;
		const y = q1.y * q2.w + q1.z * q2.x + q1.w * q2.y - q1.x * q2.z;
		const z = q1.z * q2.w - q1.y * q2.x + q1.x * q2.y + q1.w * q2.z;
		this.set(w, x, y, z);
	}

	/** Sets *this* to (the inverse of quaternion *q1*) * quaternion *q2*. */
	multInverseL(q1: QuatReadOnly, q2: QuatReadOnly) {
		const w = q1.w * q2.w + q1.x * q2.x + q1.y * q2.y + q1.z * q2.z;
		const x = q1.w * q2.x - q1.x * q2.w - q1.y * q2.z + q1.z * q2.y;
		const y = q1.w * q2.y + q1.x * q2.z - q1.y * q2.w - q1.z * q2.x;
		const z = q1.w * q2.z - q1.x * q2.y + q1.y * q2.x - q1.z * q2.w;
		this.set(w, x, y, z);
	}

	/** Sets *this* to quaternion *q1* * (the inverse of quaternion *q2*). */
	multInverseR(q1: QuatReadOnly, q2: QuatReadOnly) {
		const w = +q1.w * q2.w + q1.x * q2.x + q1.y * q2.y + q1.z * q2.z;
		const x = -q1.w * q2.x + q1.x * q2.w - q1.y * q2.z + q1.z * q2.y;
		const y = -q1.w * q2.y + q1.x * q2.z + q1.y * q2.w - q1.z * q2.x;
		const z = -q1.w * q2.z - q1.x * q2.y + q1.y * q2.x + q1.z * q2.w;
		this.set(w, x, y, z);
	}

	/** Sets *this* to quaternion *q*, with its rotation angle multiplied by *s*. */
	scaleAngle(q: QuatReadOnly, s: number) {
		const halfAngle = Math.acos(q.w);
		const sinHalfAngle = Math.sin(halfAngle);
		if (sinHalfAngle === 0) {
			this.copy(q);
			return;
		}
		const sinHalfAngleB = Math.sin(halfAngle * s);
		this._m[0] = Math.cos(halfAngle * s);
		this._m[1] = q.x / sinHalfAngle * sinHalfAngleB;
		this._m[2] = q.y / sinHalfAngle * sinHalfAngleB;
		this._m[3] = q.z / sinHalfAngle * sinHalfAngleB;
	}

	/** Gets the angle of the rotation that *this* represents. */
	angle(): number {
		return Math.acos(this._m[0]) * 2;
	}

	/** Gets the dot product between *this* and *q*. */
	dot(q: QuatReadOnly): number {
		return this._m[0] * q.w + this._m[1] * q.x + this._m[2] * q.y + this._m[3] * q.z;
	}

	/** Gets the square norm of *this*. */
	normSq(): number {
		return this.dot(this);
	}

	/** Gets the norm of *this*. */
	norm(): number {
		return Math.sqrt(this.normSq());
	}

	/** Returns the angle in radians between *this* and quaternion *q*. */
	angleBetween(q: QuatReadOnly): number {
		return Math.acos(this._m[0] * q.w + this._m[1] * q.x + this._m[2] * q.y + this._m[3] * q.z) * 2.0;
	}

	/** Sets *this* to *q* normalized or zero. */
	normalize(q: QuatReadOnly) {
		const invNorm = 1 / q.norm();
		if (isFinite(invNorm)) {
			this._m[0] = q.w * invNorm;
			this._m[1] = q.x * invNorm;
			this._m[2] = q.y * invNorm;
			this._m[3] = q.z * invNorm;
		}
		else {
			this.set(0, 0, 0, 0);
		}
	}

	/** Sets *this* to be spherically interpolated between *q1* and *q2* by the factor *u*. The parameter *u* is not clamped. */
	slerp(q1: QuatReadOnly, q2: QuatReadOnly, u: number) {
		let dot = q1.w * q2.w + q1.x * q2.x + q1.y * q2.y + q1.z * q2.z;
		let f = 1;
		if (dot < 0.0) {
			f = -1;
			dot = -dot;
		}
		if (dot <= 0.9995) {
			const angle = Math.acos(dot);
			const c1 = f * Math.sin((1.0 - u) * angle) / Math.sin(angle);
			const c2 = Math.sin(u * angle) / Math.sin(angle);
			this._m[0] = c1 * q1.w + c2 * q2.w;
			this._m[1] = c1 * q1.x + c2 * q2.x;
			this._m[2] = c1 * q1.y + c2 * q2.y;
			this._m[3] = c1 * q1.z + c2 * q2.z;
		}
		else { // Too small, so lerp
			const c1 = f * (1.0 - u);
			const c2 = u;
			this._m[0] = c1 * q1.w + c2 * q2.w;
			this._m[1] = c1 * q1.x + c2 * q2.x;
			this._m[2] = c1 * q1.y + c2 * q2.y;
			this._m[3] = c1 * q1.z + c2 * q2.z;
			this.normalize(this);
		}
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `[${this._m[0]}, ${this._m[1]}, ${this._m[2]}, ${this._m[3]}]`;
	}

	/** The internal array. */
	private _m: [number, number, number, number];

	/** An internal temp vector for Quaternion. */
	private static readonly tempVec3D = new Vec3D();
}
