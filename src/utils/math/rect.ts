/* eslint-disable es-x/no-accessor-properties */

import { Num, Pool, Vec2D, type Vec2DReadOnly } from '../../internal';

/** A rectangle as [min, min + size), read-only. */
export interface RectReadOnly {

	/** Gets the min. */
	get min(): Vec2DReadOnly;

	/** Gets the size. */
	get size(): Vec2DReadOnly;

	/** Returns true if every component in *this* equals every component in *r*. */
	equals: (r: RectReadOnly) => boolean;

	/** Returns true if *this* contains the point *p*. */
	contains: (p: Vec2DReadOnly) => boolean;

	/** Returns true if *this* intersects with *r*. */
	intersects: (r: RectReadOnly) => boolean;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** A rectangle as [min, min + size). */
export class Rect implements RectReadOnly {

	/** Pool for temporary rectangles. */
	static readonly pool = new Pool(Rect);

	/** The constructor. */
	constructor(minX: number = 0, minY: number = 0, sizeX: number = 0, sizeY: number = 0) {
		this._min = new Vec2D(minX, minY);
		this._size = new Vec2D(sizeX, sizeY);
	}

	/** Gets the min. */
	get min(): Vec2D {
		return this._min;
	}

	/** Sets the min. */
	set min(min: Vec2DReadOnly) {
		this._min.copy(min);
	}

	/** Gets the size. */
	get size(): Vec2D {
		return this._size;
	}

	/** Sets the size. */
	set size(size: Vec2DReadOnly) {
		this._size.copy(size);
	}

	/** Returns true if every component in *this* equals every component in *r*. */
	equals(r: RectReadOnly): boolean {
		return this._min.x === r.min.x && this._min.y === r.min.y
			&& this._size.x === r.size.x && this._size.y === r.size.y;
	}

	/** Returns true if *this* contains the point *p*. */
	contains(p: Vec2DReadOnly): boolean {
		return this._min.x <= p.x && p.x < this._min.x + this._size.x
			&& this._min.y <= p.y && p.y < this._min.y + this._size.y;
	}

	/** Returns true if *this* intersects with *r*. */
	intersects(r: RectReadOnly): boolean {
		return this._min.x < r.min.x + r.size.x && r.min.x < this._min.x + this._size.x
			&& this._min.y < r.min.y + r.size.y && r.min.y < this._min.y + this._size.y;
	}

	/** Sets *this* to *r*. */
	copy(r: RectReadOnly) {
		this._min.copy(r.min);
		this._size.copy(r.size);
	}

	/** Sets *this* to the *min* and *max*. */
	set(minX: number, minY: number, sizeX: number, sizeY: number) {
		this._min.set(minX, minY);
		this._size.set(sizeX, sizeY);
	}

	/** Sets *this* to be the union of *r1* and *r2*. */
	union(r1: RectReadOnly, r2: RectReadOnly) {
		const minX = Math.min(r1.min.x, r2.min.x);
		this._size.x = Math.max(r1.min.x + r1.size.x, r2.min.x + r2.size.x) - minX;
		this._min.x = minX;
		const minY = Math.min(r1.min.y, r2.min.y);
		this._size.y = Math.max(r1.min.y + r1.size.y, r2.min.y + r2.size.y) - minY;
		this._min.y = minY;
	}

	/** Sets *this* to be the intersection of *r1* and *r2*. */
	intersection(r1: RectReadOnly, r2: RectReadOnly) {
		const minX = Math.max(r1.min.x, r2.min.x);
		this._size.x = Math.min(r1.min.x + r1.size.x, r2.min.x + r2.size.x) - minX;
		this._min.x = minX;
		const minY = Math.max(r1.min.y, r2.min.y);
		this._size.y = Math.min(r1.min.y + r1.size.y, r2.min.y + r2.size.y) - minY;
		this._min.y = minY;
	}

	/** Sets *this* to *r* extended to include *p*. If integral is true, it treats *r* and the *p* as integral numbers. */
	extend(r: RectReadOnly, p: Vec2DReadOnly, integral: boolean) {
		const minX = Math.min(r.min.x, p.x);
		this._size.x = Math.max(r.min.x + r.size.x, p.x + (integral ? 1 : 0)) - minX;
		this._min.x = minX;
		const minY = Math.min(r.min.y, p.y);
		this._size.y = Math.max(r.min.y + r.size.y, p.y + (integral ? 1 : 0)) - minY;
		this._min.y = minY;
	}

	/** Sets *this* to every component of *a* multplied by *b*. */
	mult(this: Rect, r: RectReadOnly, c: number) {
		this._min.mult(r.min, c);
		this._size.mult(r.size, c);
	}

	/** Sets *out* to the closest point within *this* to *p*. Returns true if *r* contains the point. */
	closest(out: Vec2D, p: Vec2DReadOnly, includeInside: boolean): boolean {
		const contains = this.contains(p);
		if (!includeInside && contains) {
			const centerX = this._min.x + 0.5 * this._size.x;
			const centerY = this._min.y + 0.5 * this._size.y;
			if (Math.abs(centerX - p.x) >= Math.abs(centerY - p.y)) {
				out.x = this._min.x + p.x < centerX ? 0 : this._size.x;
				out.y = p.y;
			}
			else {
				out.x = p.x;
				out.y = this._min.y + p.y < centerY ? 0 : this._size.y;
			}
		}
		else {
			out.x = Num.clamp(p.x, this._min.x, this._min.x + this._size.x);
			out.y = Num.clamp(p.y, this._min.y, this._min.y + this._size.y);
		}
		return contains;
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `{ min: ${this._min}, size: ${this._size} }`;
	}

	/** The minimum. */
	private _min: Vec2D;

	/** The size. */
	private _size: Vec2D;
}
