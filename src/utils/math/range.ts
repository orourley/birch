/* eslint-disable es-x/no-accessor-properties */

import { Num, Pool } from '../../internal';

/** A range as [min, min + size), read-only. */
export interface RangeReadOnly {

	/** Gets the min. */
	get min(): number;

	/** Gets the size. */
	get size(): number;

	/** Returns true if *this* equals *r*. */
	equals: (r: RangeReadOnly) => boolean;

	/** Returns true if *this* contains *p*. */
	contains: (p: number) => boolean;

	/** Returns *p* clamped to *this*. */
	clamp: (p: number) => number;

	/** Returns true if *this* intersects (not just touches) *r*. */
	intersects: (r: Range) => boolean;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** A range as [min, min + size). */
export class Range implements RangeReadOnly {

	/** An infinite range. */
	static readonly Infinite: RangeReadOnly = new Range(Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY);

	/** A -infinite range. */
	static readonly InfiniteNeg: RangeReadOnly = new Range(Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY);

	/** Pool for temporary intervals. */
	static readonly pool = new Pool(Range);

	/** The constructor. */
	constructor(min: number = 0, size: number = 0) {
		this._min = min;
		this._size = size;
	}

	/** Gets the min. */
	get min(): number {
		return this._min;
	}

	/** Sets the min. */
	set min(min: number) {
		this._min = min;
	}

	/** Gets the size. */
	get size(): number {
		return this._size;
	}

	/** Sets the max. */
	set size(size: number) {
		this._size = size;
	}

	/** Returns true if *this* equals *r*. */
	equals(r: RangeReadOnly): boolean {
		return this._min === r.min && this._size === r.size;
	}

	/** Returns true if *this* contains *p*. */
	contains(p: number): boolean {
		return this._min <= p && p < this._min + this._size;
	}

	/** Returns *p* clamped to *this*. */
	clamp(p: number): number {
		return Num.clamp(p, this._min, this._min + this._size);
	}

	/** Returns true if *this* intersects (not just touches) *r*. */
	intersects(r: Range): boolean {
		return this._min < r._min + r._size && r._min < this._min + this._size;
	}

	/** Sets *this* to *v*. */
	copy(r: RangeReadOnly) {
		this.min = r.min;
		this.size = r.size;
	}

	/** Sets *this* to *min* and *size*. */
	set(min: number, size: number) {
		this.min = min;
		this.size = size;
	}

	/** Sets *this* to the union of the intervals *r1* and *r2*. */
	union(r1: RangeReadOnly, r2: RangeReadOnly) {
		const min = Math.min(r1.min, r2.min);
		this.size = Math.max(r1.min + r1.size, r2.min + r2.size) - min;
		this.min = min;
	}

	/** Sets *this* to the intersection of the intervals *r1* and *r2*. */
	intersection(r1: RangeReadOnly, r2: RangeReadOnly) {
		const min = Math.max(r1.min, r2.min);
		this.size = Math.min(r1.min + r1.size, r2.min + r2.size) - min;
		this.min = min;
	}

	/** Sets *this* to *r* extended to include *p*. If integral is true, it treats *r* and the *p* as integral numbers. */
	extend(r: RangeReadOnly, p: number, integral: boolean) {
		const min = Math.min(r.min, p);
		this.size = Math.max(r.min + r.size, p + (integral ? 1 : 0)) - min;
		this.min = min;
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `{ min: ${this._min}, size: ${this._size} }`;
	}

	/** The minimum. */
	private _min: number;

	/** The size. */
	private _size: number;
}
