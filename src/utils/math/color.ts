/* eslint-disable es-x/no-accessor-properties */

import { Num, Pool } from '../../internal';

/** An RGBA color, read-only. */
export interface ColorReadOnly {

	/** Gets the red component. */
	get r(): number;

	/** Gets the green component. */
	get g(): number;

	/** Gets the blue component. */
	get b(): number;

	/** Gets the alpha component. */
	get a(): number;

	/** Gets *this* as an array. */
	get array(): readonly [number, number, number, number];

	/** Returns true if *this* equals *c*. */
	equals: (c: ColorReadOnly) => boolean;

	/** Returns true if at least one component of *this* is NaN. */
	isNaN: () => boolean;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** An RGBA color. */
export class Color implements ColorReadOnly {

	/** Clear. */
	static readonly Clear: ColorReadOnly = new Color(0, 0, 0, 0);

	/** Black. */
	static readonly Black: ColorReadOnly = new Color(0, 0, 0, 1);

	/** White. */
	static readonly White: ColorReadOnly = new Color(1, 1, 1, 1);

	/** Pink. */
	static readonly Pink: ColorReadOnly = new Color(1, 0.41, 0.71);

	/** A NaN color. */
	static readonly NaN: ColorReadOnly = new Color(NaN, NaN, NaN, NaN);

	/** Pool for temporary colors. */
	static readonly pool = new Pool(Color);

	/** The constructor. */
	constructor(r = 0, g = 0, b = 0, a = 0) {
		this._m = [r, g, b, a];
	}

	/** Gets the red component. */
	get r(): number {
		return this._m[0];
	}

	/** Sets the red component. */
	set r(r: number) {
		this._m[0] = r;
	}

	/** Gets the green component. */
	get g(): number {
		return this._m[1];
	}

	/** Sets the green component. */
	set g(g: number) {
		this._m[1] = g;
	}

	/** Gets the blue component. */
	get b(): number {
		return this._m[2];
	}

	/** Sets the blue component. */
	set b(b: number) {
		this._m[2] = b;
	}

	/** Gets the alpha component. */
	get a(): number {
		return this._m[3];
	}

	/** Sets the alpha component. */
	set a(a: number) {
		this._m[3] = a;
	}

	/** Gets *this* as an [r, g, b, a] array. */
	get array(): [number, number, number, number] {
		return this._m;
	}

	/** Returns true if *this* equals *c*. */
	equals(c: ColorReadOnly): boolean {
		return this._m[0] === c.r && this._m[1] === c.g && this._m[2] === c.b && this._m[3] === c.a;
	}

	/** Returns true if at least one component of *this* is NaN. */
	isNaN(): boolean {
		return isNaN(this._m[0]) || isNaN(this._m[1]) || isNaN(this._m[2]) || isNaN(this._m[3]);
	}

	/** Sets *this* to *c*. */
	copy(c: ColorReadOnly) {
		this._m[0] = c.r;
		this._m[1] = c.g;
		this._m[2] = c.b;
		this._m[3] = c.a;
	}

	/** Sets *this* to *r*, *g*, *b*, and *a*. */
	set(r: number, g: number, b: number, a: number) {
		this._m[0] = r;
		this._m[1] = g;
		this._m[2] = b;
		this._m[3] = a;
	}

	/** Sets *this* to *c1* + *c2*. */
	add(c1: ColorReadOnly, c2: ColorReadOnly) {
		this._m[0] = c1.r + c2.r;
		this._m[1] = c1.g + c2.g;
		this._m[2] = c1.b + c2.b;
		this._m[3] = c1.a + c2.a;
	}

	/** Sets *this* to *c1* - *c2*. */
	sub(c1: ColorReadOnly, c2: ColorReadOnly) {
		this._m[0] = c1.r - c2.r;
		this._m[1] = c1.g - c2.g;
		this._m[2] = c1.b - c2.b;
		this._m[3] = c1.a - c2.a;
	}

	/** Sets *this* to *c* * *s*. */
	mult(c: ColorReadOnly, s: number) {
		this._m[0] = c.r * s;
		this._m[1] = c.g * s;
		this._m[2] = c.b * s;
		this._m[2] = c.a * s;
	}

	/** Sets *this* to *c* / *s*. */
	div(c: ColorReadOnly, s: number) {
		this._m[0] = c.r / s;
		this._m[1] = c.g / s;
		this._m[2] = c.b / s;
		this._m[2] = c.a / s;
	}

	/** Sets *this* to *c1* scaled by *c2*, component-wise. */
	multV(c1: ColorReadOnly, c2: ColorReadOnly) {
		this._m[0] = c1.r * c2.r;
		this._m[1] = c1.g * c2.g;
		this._m[2] = c1.b * c2.b;
		this._m[3] = c1.a * c2.a;
	}

	/** Sets *this* to *c1* / *c2*, component-wise. */
	divV(c1: ColorReadOnly, c2: ColorReadOnly) {
		this._m[0] = c1.r / c2.r;
		this._m[1] = c1.g / c2.g;
		this._m[2] = c1.b / c2.b;
		this._m[3] = c1.a / c2.a;
	}

	/** Sets *this* to *s1* * *c1* + *s2* * *c2*. */
	addMult(s1: number, c1: ColorReadOnly, s2: number, c2: ColorReadOnly) {
		this._m[0] = s1 * c1.r + s2 * c2.r;
		this._m[1] = s1 * c1.g + s2 * c2.g;
		this._m[2] = s1 * c1.b + s2 * c2.b;
		this._m[3] = s1 * c1.a + s2 * c2.a;
	}

	/** Sets *this* to *c*, clamped between *min* and *max*. */
	clamp(c: ColorReadOnly, min: ColorReadOnly, max: ColorReadOnly) {
		this._m[0] = Num.clamp(c.r, min.r, max.r);
		this._m[1] = Num.clamp(c.g, min.g, max.g);
		this._m[2] = Num.clamp(c.b, min.b, max.b);
		this._m[3] = Num.clamp(c.a, min.a, max.a);
	}

	/** Sets *this* to the lerp between *c* and *d*, with lerp factor *u*. */
	lerp(c: ColorReadOnly, d: ColorReadOnly, u: number) {
		this._m[0] = Num.lerp(c.r, d.r, u);
		this._m[1] = Num.lerp(c.g, d.g, u);
		this._m[2] = Num.lerp(c.b, d.b, u);
		this._m[3] = Num.lerp(c.a, d.a, u);
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `[${this._m[0]}, ${this._m[1]}, ${this._m[2]}, ${this._m[3]}]`;
	}

	/** The internal array. */
	private _m: [number, number, number, number];
}
