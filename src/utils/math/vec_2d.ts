/* eslint-disable es-x/no-accessor-properties */

import { Num, Pool } from '../../internal';

/** A two-dimensional vector, read-only. */
export interface Vec2DReadOnly {

	/** Gets the x component. */
	get x(): number;

	/** Gets the y component. */
	get y(): number;

	/** Gets *this* as an array. */
	get array(): readonly [number, number];

	/** Returns true if *this* equals *v*. */
	equals: (v: Vec2DReadOnly) => boolean;

	/** Returns true if all components of *this* are zero. */
	isZero: () => boolean;

	/** Returns true if at least one component of *this* is NaN. */
	isNaN: () => boolean;

	/** Gets the dot product between *this* and *v*. */
	dot: (v: Vec2DReadOnly) => number;

	/** Gets the square norm of *this*. */
	normSq: () => number;

	/** Gets the norm of *this*. */
	norm: () => number;

	/** Gets the cross product between *this* and *v*, which is: sin(the angle in radians from *v* to *a*) X norm(*v*) X norm(*a*). */
	cross: (v: Vec2DReadOnly) => number;

	/** Gets the distance between *this* and *v*. */
	distance: (v: Vec2DReadOnly) => number;

	/** Gets the smallest angle in radians between *this* and *v*. */
	angle: (v: Vec2DReadOnly) => number;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** A two-dimensional vector. */
export class Vec2D implements Vec2DReadOnly {

	/** The zero vector. */
	static readonly Zero: Vec2DReadOnly = new Vec2D(0, 0);

	/** The one vector. */
	static readonly One: Vec2DReadOnly = new Vec2D(1, 1);

	/** The unit X vector. */
	static readonly UnitX: Vec2DReadOnly = new Vec2D(1, 0);

	/** The unit Y vector. */
	static readonly UnitY: Vec2DReadOnly = new Vec2D(0, 1);

	/** The unit negative X vector. */
	static readonly UnitNegX: Vec2DReadOnly = new Vec2D(-1, 0);

	/** The unit negative Y vector. */
	static readonly UnitNegY: Vec2DReadOnly = new Vec2D(0, -1);

	/** A NaN vector. */
	static readonly NaN: Vec2DReadOnly = new Vec2D(NaN, NaN);

	/** Pool for temporary Vec2Ds. */
	static readonly pool = new Pool(Vec2D);

	/** The constructor. */
	constructor(x: number = 0, y: number = 0) {
		this._m = [x, y];
	}

	/** Gets the x component. */
	get x(): number {
		return this._m[0];
	}

	/** Sets the x component. */
	set x(x: number) {
		this._m[0] = x;
	}

	/** Gets the y component. */
	get y(): number {
		return this._m[1];
	}

	/** Sets the y component. */
	set y(y: number) {
		this._m[1] = y;
	}

	/** Gets *this* as an [x, y] array. */
	get array(): [number, number] {
		return this._m;
	}

	/** Returns true if *this* equals *v*. */
	equals(v: Vec2DReadOnly): boolean {
		return this._m[0] === v.x && this.y === v.y;
	}

	/** Returns true if all components of *this* are zero. */
	isZero(): boolean {
		return this._m[0] === 0 && this._m[1] === 0;
	}

	/** Returns true if at least one component of *this* is NaN. */
	isNaN(): boolean {
		return isNaN(this._m[0]) || isNaN(this._m[1]);
	}

	/** Sets *this* to *v*. */
	copy(v: Vec2DReadOnly) {
		this._m[0] = v.x;
		this._m[1] = v.y;
	}

	/** Sets *this* to *x* and *y*. */
	set(x: number, y: number) {
		this._m[0] = x;
		this._m[1] = y;
	}

	/** Sets *this* to -*v*. */
	neg(v: Vec2DReadOnly) {
		this._m[0] = -v.x;
		this._m[1] = -v.y;
	}

	/** Sets *this* to *v1* + *v2*. */
	add(v1: Vec2DReadOnly, v2: Vec2DReadOnly) {
		this._m[0] = v1.x + v2.x;
		this._m[1] = v1.y + v2.y;
	}

	/** Sets *this* to *v1* - *v2*. */
	sub(v1: Vec2DReadOnly, v2: Vec2DReadOnly) {
		this._m[0] = v1.x - v2.x;
		this._m[1] = v1.y - v2.y;
	}

	/** Sets *this* to *v* * *s*. */
	mult(v: Vec2DReadOnly, s: number) {
		this._m[0] = v.x * s;
		this._m[1] = v.y * s;
	}

	/** Sets *this* to *v* / *s*. */
	div(v: Vec2DReadOnly, s: number) {
		this._m[0] = v.x / s;
		this._m[1] = v.y / s;
	}

	/** Sets *this* to *v1* * *v2*, component-wise. */
	multV(v1: Vec2DReadOnly, v2: Vec2DReadOnly) {
		this._m[0] = v1.x * v2.x;
		this._m[1] = v1.y * v2.y;
	}

	/** Sets *this* to *v1* / *v2*, component-wise. */
	divV(v1: Vec2DReadOnly, v2: Vec2DReadOnly) {
		this._m[0] = v1.x / v2.x;
		this._m[1] = v1.y / v2.y;
	}

	/** Sets *this* to *s1* * *v1* + *s2* * *v2*. */
	addMult(s1: number, v1: Vec2DReadOnly, s2: number, v2: Vec2DReadOnly) {
		this._m[0] = s1 * v1.x + s2 * v2.x;
		this._m[1] = s1 * v1.y + s2 * v2.y;
	}

	/** Gets the dot product between *this* and *v*. */
	dot(v: Vec2DReadOnly): number {
		return this._m[0] * v.x + this._m[1] * v.y;
	}

	/** Gets the square norm of *this*. */
	normSq(): number {
		return this.dot(this);
	}

	/** Gets the norm of *this*. */
	norm(): number {
		return Math.sqrt(this.normSq());
	}

	/** Gets the cross product between *this* and *v*, which is: sin(the angle in radians from *v* to *a*) X norm(*v*) X norm(*a*). */
	cross(v: Vec2DReadOnly): number {
		return this._m[0] * v.y - this._m[1] * v.x;
	}

	/** Gets the distance between *this* and *v*. */
	distance(v: Vec2DReadOnly): number {
		const x = this._m[0] - v.x;
		const y = this._m[1] - v.y;
		return Math.sqrt(x * x + y * y);
	}

	/** Gets the smallest angle in radians between *this* and *v*. */
	angle(v: Vec2DReadOnly): number {
		const norms = this.norm() * v.norm();
		if (norms > 0) {
			return Math.acos(Num.clamp(this.dot(v) / norms, -1.0, 1.0));
		}
		else {
			return 0;
		}
	}

	/** Sets *this* to *v* normalized or zero. */
	normalize(v: Vec2DReadOnly) {
		const invNorm = 1 / v.norm();
		if (isFinite(invNorm)) {
			this._m[0] = v.x * invNorm;
			this._m[1] = v.y * invNorm;
		}
		else {
			this._m[0] = 0;
			this._m[1] = 0;
		}
	}

	/** Sets *this* to have norm *n* in the same direction as *v*. */
	setNorm(v: Vec2DReadOnly, n: number) {
		const invNorm = 1 / v.norm();
		if (isFinite(invNorm)) {
			this._m[0] = v.x * (n * invNorm);
			this._m[1] = v.y * (n * invNorm);
		}
		else {
			this._m[0] = 0;
			this._m[1] = 0;
		}
	}

	/** Sets *this* to *v*, clamped between *min* and *max*. */
	clamp(v: Vec2DReadOnly, min: number, max: number) {
		this._m[0] = Num.clamp(v.x, min, max);
		this._m[1] = Num.clamp(v.y, min, max);
	}

	/** Sets *this* to *v*, clamped between *min* and *max*, component-wise. */
	clampV(v: Vec2DReadOnly, min: Vec2DReadOnly, max: Vec2DReadOnly) {
		this._m[0] = Num.clamp(v.x, min.x, max.x);
		this._m[1] = Num.clamp(v.y, min.y, max.y);
	}

	/** Sets *this* to the lerp between *v1* and *v2*, with lerp factor *u*. */
	lerp(v1: Vec2DReadOnly, v2: Vec2DReadOnly, u: number) {
		this._m[0] = Num.lerp(v1.x, v2.x, u);
		this._m[1] = Num.lerp(v1.y, v2.y, u);
	}

	/** Sets *this* to *v* rotated by *angle* radians. */
	rot(v: Vec2DReadOnly, angle: number) {
		const cosAngle = Math.cos(angle);
		const sinAngle = Math.sin(angle);
		const ax = v.x; // In case this is a.
		this._m[0] = v.x * cosAngle - v.y * sinAngle;
		this._m[1] = ax * sinAngle + v.y * cosAngle;
	}

	/** Sets *this* to *v* rotated by 90 degrees counter-clockwise. */
	rot90(v: Vec2DReadOnly) {
		const ax = v.x; // In case this is a.
		this._m[0] = -v.y;
		this._m[1] = ax;
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `[${this._m[0]}, ${this._m[1]}]`;
	}

	/** The internal array. */
	private _m: [number, number];
}
