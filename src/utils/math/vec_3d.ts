/* eslint-disable es-x/no-accessor-properties */

import { Num, Pool, type QuatReadOnly } from '../../internal';

/** A three-dimensional vector, read-only. */
export interface Vec3DReadOnly {

	/** Gets the x component. */
	get x(): number;

	/** Gets the y component. */
	get y(): number;

	/** Gets the z component. */
	get z(): number;

	/** Gets *this* as an array. */
	get array(): readonly [number, number, number];

	/** Returns true if *this* equals *v*. */
	equals: (v: Vec3DReadOnly) => boolean;

	/** Returns true if all components of *this* are zero. */
	isZero: () => boolean;

	/** Returns true if at least one component of *this* is NaN. */
	isNaN: () => boolean;

	/** Gets the dot product between *this* and *v*. */
	dot: (v: Vec3DReadOnly) => number;

	/** Gets the square norm of *this*. */
	normSq: () => number;

	/** Gets the norm of *this*. */
	norm: () => number;

	/** Gets the norm of just the x and y components. */
	normXY: () => number;

	/** Gets the distance between *this* and *v*. */
	distance: (v: Vec3DReadOnly) => number;

	/** Gets the smallest angle in radians between *this* and *v*. */
	angle: (v: Vec3DReadOnly) => number;

	/** Gets the angle in radians from *this* to *v* if they were projected on the plane formed by *axis*.
	 *  It can be positive or negative, based on the right-hand rule rotation around axis. */
	angleAroundAxis: (v: Vec3DReadOnly, axis: Vec3DReadOnly) => number;

	/** Gets *this* as a string. */
	toString: () => string;
}

/** A three-dimensional vector. */
export class Vec3D implements Vec3DReadOnly {

	/** The zero vector. */
	static readonly Zero: Vec3DReadOnly = new Vec3D(0, 0, 0);

	/** The one vector. */
	static readonly One: Vec3DReadOnly = new Vec3D(1, 1, 1);

	/** The unit X vector. */
	static readonly UnitX: Vec3DReadOnly = new Vec3D(1, 0, 0);

	/** The unit Y vector. */
	static readonly UnitY: Vec3DReadOnly = new Vec3D(0, 1, 0);

	/** The unit Z vector. */
	static readonly UnitZ: Vec3DReadOnly = new Vec3D(0, 0, 1);

	/** The unit negative X vector. */
	static readonly UnitNegX: Vec3DReadOnly = new Vec3D(-1, 0, 0);

	/** The unit negative Y vector. */
	static readonly UnitNegY: Vec3DReadOnly = new Vec3D(0, -1, 0);

	/** The unit negative Z vector. */
	static readonly UnitNegZ: Vec3DReadOnly = new Vec3D(0, 0, -1);

	/** A NaN vector. */
	static readonly NaN: Vec3DReadOnly = new Vec3D(NaN, NaN, NaN);

	/** Pool for temporary vectors. */
	static readonly pool = new Pool(Vec3D);

	/** The constructor. */
	constructor(x: number = 0, y: number = 0, z: number = 0) {
		this._m = [x, y, z];
	}

	/** Gets the x component. */
	get x(): number {
		return this._m[0];
	}

	/** Sets the x component. */
	set x(x: number) {
		this._m[0] = x;
	}

	/** Gets the y component. */
	get y(): number {
		return this._m[1];
	}

	/** Sets the y component. */
	set y(y: number) {
		this._m[1] = y;
	}

	/** Gets the z component. */
	get z(): number {
		return this._m[2];
	}

	/** Sets the y component. */
	set z(z: number) {
		this._m[2] = z;
	}

	/** Gets *this* as an [x, y, z] array. */
	get array(): [number, number, number] {
		return this._m;
	}

	/** Returns true if *this* equals *v*. */
	equals(v: Vec3DReadOnly): boolean {
		return this._m[0] === v.x && this._m[1] === v.y && this._m[2] === v.z;
	}

	/** Returns true if all components of *this* are zero. */
	isZero(): boolean {
		return this._m[0] === 0 && this._m[1] === 0 && this._m[2] === 0;
	}

	/** Returns true if at least one component of *this* is NaN. */
	isNaN(): boolean {
		return isNaN(this._m[0]) || isNaN(this._m[1]) || isNaN(this._m[2]);
	}

	/** Sets *this* to *v*. */
	copy(v: Vec3DReadOnly) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}

	/** Sets *this* to *x*, *y*, and *z*. */
	set(x: number, y: number, z: number) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/** Sets *this* to -*v*. */
	neg(v: Vec3DReadOnly) {
		this.x = -v.x;
		this.y = -v.y;
		this.z = -v.z;
	}

	/** Sets *this* to *v1* + *v2*. */
	add(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		this.x = v1.x + v2.x;
		this.y = v1.y + v2.y;
		this.z = v1.z + v2.z;
	}

	/** Sets *this* to *v1* - *v2*. */
	sub(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		this.x = v1.x - v2.x;
		this.y = v1.y - v2.y;
		this.z = v1.z - v2.z;
	}

	/** Sets *this* to *v* * *s*. */
	mult(v: Vec3DReadOnly, s: number) {
		this.x = v.x * s;
		this.y = v.y * s;
		this.z = v.z * s;
	}

	/** Sets *this* to *v* / *s*. */
	div(v: Vec3DReadOnly, s: number) {
		this.x = v.x / s;
		this.y = v.y / s;
		this.z = v.z / s;
	}

	/** Sets *this* to *v1* * *v2*, component-wise. */
	multV(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		this.x = v1.x * v2.x;
		this.y = v1.y * v2.y;
		this.z = v1.z * v2.z;
	}

	/** Sets *this* to *v1* / *v2*, component-wise. */
	divV(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		this.x = v1.x / v2.x;
		this.y = v1.y / v2.y;
		this.z = v1.z / v2.z;
	}

	/** Sets *this* to *s1* * *v1* + *s2* * *v2*. */
	addMult(s1: number, v1: Vec3DReadOnly, s2: number, v2: Vec3DReadOnly) {
		this.x = s1 * v1.x + s2 * v2.x;
		this.y = s1 * v1.y + s2 * v2.y;
		this.z = s1 * v1.z + s2 * v2.z;
	}

	/** Gets the dot product between *this* and *v*. */
	dot(v: Vec3DReadOnly): number {
		return this._m[0] * v.x + this._m[1] * v.y + this._m[2] * v.z;
	}

	/** Gets the square norm of *this*. */
	normSq(): number {
		return this.dot(this);
	}

	/** Gets the norm of *this*. */
	norm(): number {
		return Math.sqrt(this.normSq());
	}

	/** Gets the norm of just the x and y components. */
	normXY(): number {
		return Math.sqrt(this._m[0] * this._m[0] + this._m[1] * this._m[1]);
	}

	/** Gets the distance between *this* and *v*. */
	distance(v: Vec3DReadOnly): number {
		const x = this._m[0] - v.x;
		const y = this._m[1] - v.y;
		const z = this._m[2] - v.z;
		return Math.sqrt(x * x + y * y + z * z);
	}

	/** Gets the smallest angle in radians between *this* and *v*. */
	angle(v: Vec3DReadOnly): number {
		const norms = this.norm() * v.norm();
		if (norms > 0) {
			return Math.acos(Num.clamp(this.dot(v) / norms, -1.0, 1.0));
		}
		else {
			return 0;
		}
	}

	/** Gets the angle in radians from *this* to *v* if they were projected on the plane formed by *axis*.
	 *  It can be positive or negative, based on the right-hand rule rotation around axis. */
	angleAroundAxis(v: Vec3DReadOnly, axis: Vec3DReadOnly): number {
		const thisP = Vec3D.pool.get();
		const vP = Vec3D.pool.get();

		// Project the vectors to the plane of the axis.
		thisP.addMult(1, this, -this.dot(axis), axis);
		vP.addMult(1, v, -v.dot(axis), axis);

		// Get the angle.
		let angle = thisP.angle(vP);

		// Determine the sign of the angle.
		thisP.cross(thisP, vP);
		if (thisP.dot(axis) < 0) {
			angle *= -1;
		}

		// Return the angle.
		Vec3D.pool.release(thisP);
		Vec3D.pool.release(vP);
		return angle;
	}

	/** Sets *this* to *v* normalized or zero. */
	normalize(v: Vec3DReadOnly) {
		const invNorm = 1 / v.norm();
		if (isFinite(invNorm)) {
			this.x = v.x * invNorm;
			this.y = v.y * invNorm;
			this.z = v.z * invNorm;
		}
		else {
			this.x = 0;
			this.y = 0;
			this.z = 0;
		}
	}

	/** Sets *this* to have norm *n* in the same direction as *v*. */
	setNorm(v: Vec3DReadOnly, n: number) {
		const invNorm = 1 / v.norm();
		if (isFinite(invNorm)) {
			this.x = v.x * (n * invNorm);
			this.y = v.y * (n * invNorm);
			this.z = v.z * (n * invNorm);
		}
		else {
			this.x = 0;
			this.y = 0;
			this.z = 0;
		}
	}

	/** Sets *this* to *v*, clamped between *min* and *max*. */
	clamp(v: Vec3DReadOnly, min: number, max: number) {
		this.x = Num.clamp(v.x, min, max);
		this.y = Num.clamp(v.y, min, max);
		this.z = Num.clamp(v.z, min, max);
	}

	/** Sets *this* to *v*, clamped between *min* and *max*, component-wise. */
	clampV(v: Vec3DReadOnly, min: Vec3DReadOnly, max: Vec3DReadOnly) {
		this.x = Num.clamp(v.x, min.x, max.x);
		this.y = Num.clamp(v.y, min.y, max.y);
		this.z = Num.clamp(v.z, min.z, max.z);
	}

	/** Sets *this* to the lerp between *v1* and *v2*, with lerp factor *u*. */
	lerp(v1: Vec3DReadOnly, v2: Vec3DReadOnly, u: number) {
		this.x = Num.lerp(v1.x, v2.x, u);
		this.y = Num.lerp(v1.y, v2.y, u);
		this.z = Num.lerp(v1.z, v2.z, u);
	}

	/** Sets *this* to the slerp between *v1* and *v2*, with the lerp factor *u*. */
	slerp(v1: Vec3DReadOnly, v2: Vec3DReadOnly, u: number) {
		const v1Norm = v1.norm();
		const v2Norm = v2.norm();
		if (v1Norm > 0.0 && v2Norm > 0.0) {
			const dot = v1.dot(v2);
			const angle = Math.acos(Num.clamp(dot / (v1Norm * v2Norm), -1, +1));
			if (Math.abs(angle) > 0.001745327777) {
				this.addMult(1, v2, -dot / v2Norm, v1);
				this.normalize(this);
				this.addMult(Math.cos(u * angle), v1, Math.sin(u * angle), this);
			}
			// If less than 0.1 degree, just lerp.
			else {
				this.lerp(v1, v2, u);
			}
		}
		// If one of them is zero, just lerp.
		else {
			this.lerp(v1, v2, u);
		}
	}

	/** Sets *this* to the slerp between *v1* and *v2* around the unit *axis*, with the lerp factor *u*. */
	slerpAroundAxis(v1: Vec3DReadOnly, v2: Vec3DReadOnly, axis: Vec3DReadOnly, u: number) {

		// Project onto the plane represented by the axis, and make them units.
		const v1Proj = Vec3D.pool.get();
		const v2Proj = Vec3D.pool.get();
		const v1AlongAxis = axis.dot(v1);
		const v2AlongAxis = axis.dot(v2);
		v1Proj.addMult(1, v1, -v1AlongAxis, axis);
		v2Proj.addMult(1, v2, -v2AlongAxis, axis);
		const v1ProjNorm = v1Proj.norm();
		const v2ProjNorm = v2Proj.norm();
		if (v1ProjNorm > 0.0 && v2ProjNorm > 0.0) {
			v1Proj.div(v1Proj, v1ProjNorm);
			v2Proj.div(v2Proj, v2ProjNorm);

			// Do slerp on the projected vectors.
			const dot = v1Proj.dot(v2Proj);
			const angle = Math.acos(Num.clamp(dot, -1, +1));
			if (Math.abs(angle) > 0.001745327777) {
				this.addMult(1, v2Proj, -dot, v1Proj);
				this.normalize(this);
				this.addMult(Math.cos(u * angle), v1Proj, Math.sin(u * angle), this);
			}
			// If less than 0.1 degree, just lerp.
			else {
				this.lerp(v1Proj, v2Proj, u);
			}

			// Lerp radially.
			this.setNorm(this, Num.lerp(v1ProjNorm, v2ProjNorm, u));

			// Lerp axially.
			this.addMult(1, this, Num.lerp(v1AlongAxis, v2AlongAxis, u), axis);
		}
		// If one of them is zero, just lerp.
		else {
			this.lerp(v1, v2, u);
		}

		Vec3D.pool.release(v1Proj);
		Vec3D.pool.release(v2Proj);
	}

	/** Sets *this* to *v1* cross *v2*. */
	cross(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		const x = v1.y * v2.z - v1.z * v2.y;
		const y = v1.z * v2.x - v1.x * v2.z;
		const z = v1.x * v2.y - v1.y * v2.x;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/** Sets *this* to a vector that is perpendicular to *v*. It tries on the xy-plane first otherwise chooses the x-axis. */
	perp(v: Vec3DReadOnly) {
		if (v.y !== 0 || v.x !== 0) {
			this.set(-v.y, v.x, 0);
		}
		else {
			this.set(1, 0, 0);
		}
	}

	/** Sets *this* to *v* rotated by *q*. */
	rotate(q: QuatReadOnly, v: Vec3DReadOnly) {
		const x = 2 * (q.y * v.z - q.z * v.y);
		const y = 2 * (q.z * v.x - q.x * v.z);
		const z = 2 * (q.x * v.y - q.y * v.x);
		this.x = v.x + q.w * x + q.y * z - q.z * y;
		this.y = v.y + q.w * y + q.z * x - q.x * z;
		this.z = v.z + q.w * z + q.x * y - q.y * x;
		// From http://blog.molecular-matters.com/2013/05/24/a-faster-quaternion-vector-multiplication/
	}

	/** Sets *this* to *v* rotated by the inverse of *q*. */
	rotateInverse(q: QuatReadOnly, v: Vec3DReadOnly) {
		const x = 2 * (q.y * v.z - q.z * v.y);
		const y = 2 * (q.z * v.x - q.x * v.z);
		const z = 2 * (q.x * v.y - q.y * v.x);
		this.x = v.x - q.w * x + q.y * z - q.z * y;
		this.y = v.y - q.w * y + q.z * x - q.x * z;
		this.z = v.z - q.w * z + q.x * y - q.y * x;
	}

	/** Sets *this* to the axis *i* (0, 1, or 2) of the quaternion *q*. If the axis is undefined, it returns the axis of rotation. */
	setFromQuatAxis(q: QuatReadOnly, i: number | undefined) {
		if (i === undefined) {
			this.set(q.x, q.y, q.z);
			this.normalize(this);
		}
		else {
			i %= 3;
			const j = (i + 1) % 3;
			const k = (i + 2) % 3;
			const qArray = q.array;
			this._m[i] = q.w * q.w + qArray[1 + i]! * qArray[1 + i]! - qArray[1 + j]! * qArray[1 + j]! - qArray[1 + k]! * qArray[1 + k]!;
			this._m[j] = 2 * (qArray[1 + i]! * qArray[1 + j]! + q.w * qArray[1 + k]!);
			this._m[k] = 2 * (qArray[1 + i]! * qArray[1 + k]! - q.w * qArray[1 + j]!);
		}
	}

	/** Sets *this* to a unit vector in the plane of '*v1* cross *v2*' and normal to *v1*, such that '*this* dot *v2*' is positive. */
	setNormalTo(v1: Vec3DReadOnly, v2: Vec3DReadOnly) {
		const x = v2.x * (v1.y * v1.y + v1.z * v1.z) - v1.x * (v1.y * v2.y + v1.z * v2.z);
		const y = v2.y * (v1.z * v1.z + v1.x * v1.x) - v1.y * (v1.z * v2.z + v1.x * v2.x);
		const z = v2.z * (v1.x * v1.x + v1.y * v1.y) - v1.z * (v1.x * v2.x + v1.y * v2.y);
		this.x = x;
		this.y = y;
		this.z = z;
		this.normalize(this);
	}

	/** Gets *this* as a string. */
	toString(): string {
		return `[${this._m[0]}, ${this._m[1]}, ${this._m[2]}]`;
	}

	/** The internal array. */
	private _m: [number, number, number];
}
