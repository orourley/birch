/* eslint-disable es-x/no-accessor-properties */

import { Pool } from '../../internal';

/** A 4-by-4 matrix, read-only. */
export interface Mat44ReadOnly {

	/** Gets the component of *this* at the *row* and *column*. */
	get: (row: number, column: number) => number | undefined;

	/** Gets *this* as an array. */
	get array(): readonly number[];
}

/** A 4-by-4 matrix. */
export class Mat44 implements Mat44ReadOnly {

	/** The identity matrix. */
	static readonly Identity: Mat44ReadOnly = new Mat44(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	/** The identity matrix. */
	static readonly Zero: Mat44ReadOnly = new Mat44(
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0);

	/** Pool for temporary matrices. */
	static readonly pool = new Pool(Mat44);

	/** The constructor. */
	constructor(
		m00: number = 0, m01: number = 0, m02: number = 0, m03: number = 0,
		m10: number = 0, m11: number = 0, m12: number = 0, m13: number = 0,
		m20: number = 0, m21: number = 0, m22: number = 0, m23: number = 0,
		m30: number = 0, m31: number = 0, m32: number = 0, m33: number = 0) {
		this._m[0] = m00; this._m[4] = m01; this._m[8] = m02; this._m[12] = m03;
		this._m[1] = m10; this._m[5] = m11; this._m[9] = m12; this._m[13] = m13;
		this._m[2] = m20; this._m[6] = m21; this._m[10] = m22; this._m[14] = m23;
		this._m[3] = m30; this._m[7] = m31; this._m[11] = m32; this._m[15] = m33;
	}

	/** Gets the component of *this* at the *row* and *column*. */
	get(row: number, column: number): number | undefined {
		return this._m[column * 4 + row];
	}

	/** Gets *this* as an array. */
	get array(): number[] {
		return this._m;
	}

	/** Copies *m* to *this*. */
	copy(m: Mat44ReadOnly) {
		for (let i = 0; i < 16; i++) {
			this._m[i] = m.array[i]!;
		}
	}

	/** Sets the component of *this* at the *row* and *column* to *value*. */
	set(row: number, column: number, value: number) {
		this._m[column * 4 + row] = value;
	}

	/** Sets the components of *this* in row-major order. */
	setAll(m00: number, m01: number, m02: number, m03: number,
		m10: number, m11: number, m12: number, m13: number,
		m20: number, m21: number, m22: number, m23: number,
		m30: number, m31: number, m32: number, m33: number) {
		this._m[0] = m00; this._m[4] = m01; this._m[8] = m02; this._m[12] = m03;
		this._m[1] = m10; this._m[5] = m11; this._m[9] = m12; this._m[13] = m13;
		this._m[2] = m20; this._m[6] = m21; this._m[10] = m22; this._m[14] = m23;
		this._m[3] = m30; this._m[7] = m31; this._m[11] = m32; this._m[15] = m33;
	}

	/** Gets *this* as a string. */
	toString(): string {
		return '['
			+ `[${this._m[0]}, ${this._m[4]}, ${this._m[8]}, ${this._m[12]}], `
			+ `[${this._m[1]}, ${this._m[5]}, ${this._m[9]}, ${this._m[13]}], `
			+ `[${this._m[2]}, ${this._m[6]}, ${this._m[10]}, ${this._m[14]}], `
			+ `[${this._m[3]}, ${this._m[7]}, ${this._m[11]}, ${this._m[15]}]]`;
	}

	/** The internal array. */
	private _m: number[] = [
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0];
}
