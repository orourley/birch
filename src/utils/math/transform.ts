import { Mat44, type QuatReadOnly, type Vec3DReadOnly } from '../../internal';

export class Transforms {

	/** Sets *matrixOut* to a local-to-world matrix. */
	static localToWorld(matrixOut: Mat44, position: Vec3DReadOnly, orientation: QuatReadOnly) {
		this.orientation(matrixOut, orientation, false);
		matrixOut.set(0, 3, position.x);
		matrixOut.set(1, 3, position.y);
		matrixOut.set(2, 3, position.z);
	}

	/** Sets *matrixOut* to a world-to-local matrix. */
	static worldToLocal(matrixOut: Mat44, position: Vec3DReadOnly, orientation: QuatReadOnly) {
		const w2l = matrixOut; // Shorter variable name for brevity.
		this.orientation(w2l, orientation, true);
		w2l.set(0, 3, -position.x * w2l.get(0, 0)! - position.y * w2l.get(0, 1)! - position.z * w2l.get(0, 2)!);
		w2l.set(1, 3, -position.x * w2l.get(1, 0)! - position.y * w2l.get(1, 1)! - position.z * w2l.get(1, 2)!);
		w2l.set(2, 3, -position.x * w2l.get(2, 0)! - position.y * w2l.get(2, 1)! - position.z * w2l.get(2, 2)!);
	}

	/** Sets *matrixOut* to the local-to-ndc using a perspective projection.
	 *  It makes the most widest aspect ratio direction be the field of view.
	 *  If update is true, it sets up only the values that change with the params. */
	static localToNdcPerspective(matrixOut: Mat44, fov: number, aspectRatio: number, near: number, far: number, update: boolean = false) {
		if (!update) {
			matrixOut.copy(Mat44.Zero);
			matrixOut.set(3, 1, 1);
		}
		const tanHalfFOV = Math.tan(fov / 2);
		if (aspectRatio <= 1) {
			matrixOut.set(0, 0, 1 / (tanHalfFOV * aspectRatio));
			matrixOut.set(1, 2, 1 / tanHalfFOV);
		}
		else {
			matrixOut.set(0, 0, 1 / tanHalfFOV);
			matrixOut.set(1, 2, aspectRatio / tanHalfFOV);
		}
		if (isFinite(far)) {
			const fmnInv = 1 / (near - far);
			matrixOut.set(2, 1, (far + near) * fmnInv);
			matrixOut.set(2, 3, -(2 * near * far) * fmnInv);
		}
		else {
			matrixOut.set(2, 1, Number.EPSILON - 1);
			matrixOut.set(2, 3, (Number.EPSILON - 1) * 2 * near);
		}
	}

	/** Sets *matrixOut* to the ndc-to-local using a perspective projection.
	 *  It makes the most widest aspect ratio direction be the field of view. */
	static ndcToLocalPerspective(matrixOut: Mat44, fov: number, aspectRatio: number, near: number, far: number, update: boolean) {
		if (!update) {
			matrixOut.copy(Mat44.Zero);
			matrixOut.set(1, 3, 1);
		}
		const tanHalfFOV = Math.tan(fov / 2);
		if (aspectRatio <= 1) {
			matrixOut.set(0, 0, tanHalfFOV * aspectRatio);
			matrixOut.set(2, 1, tanHalfFOV);
		}
		else {
			matrixOut.set(0, 0, tanHalfFOV);
			matrixOut.set(2, 1, tanHalfFOV / aspectRatio);
		}
		if (isFinite(far)) {
			const nf2Inv = 1 / (2 * near * far);
			matrixOut.set(3, 2, (near - far) * nf2Inv);
			matrixOut.set(3, 3, -(far + near) * nf2Inv);
		}
		else {
			const n2Inv = 1 / (2 * near * (1 - Number.EPSILON));
			matrixOut.set(3, 2, n2Inv);
			matrixOut.set(3, 3, n2Inv);
		}
	}

	/** Sets *matrixOut* to the rotation represented by the quaternion. */
	private static orientation(matrixOut: Mat44, orientation: QuatReadOnly, inverse: boolean) {
		const xx = orientation.x * orientation.x;
		const xy = orientation.x * orientation.y;
		const xz = orientation.x * orientation.z;
		const xw = orientation.x * orientation.w * (inverse ? -1 : 1);
		const yy = orientation.y * orientation.y;
		const yz = orientation.y * orientation.z;
		const yw = orientation.y * orientation.w * (inverse ? -1 : 1);
		const zz = orientation.z * orientation.z;
		const zw = orientation.z * orientation.w * (inverse ? -1 : 1);
		matrixOut.setAll(
			1 - 2 * (yy + zz), 2 * (xy - zw), 2 * (xz + yw), 0,
			2 * (xy + zw), 1 - 2 * (xx + zz),	2 * (yz - xw), 0,
			2 * (xz - yw), 2 * (yz + xw), 1 - 2 * (xx + yy), 0,
			0, 0, 0, 1);
	}
}
