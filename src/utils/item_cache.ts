import { FastMap } from './containers/fast_map';

/** An item with a name. */
export abstract class NamedItem {
	readonly name: string;

	constructor(name: string) {
		this.name = name;
	}

	/** Returns the item as a name. */
	toString(): string {
		return `${this.constructor.name} "${this.name}"`;
	}
}

/** A cache of named items. */
export class ItemCache<Item extends NamedItem> {

	/** Constructs the cache. */
	constructor(typeName: string, createItem: (name: string) => Item, destroyItem: (item: Item) => void) {

		// Save the type name for error output.
		this._typeName = typeName;

		// Save the create and destroy functions.
		this._createItem = createItem;
		this._destroyItem = destroyItem;
	}

	/** Destroys the cache. */
	zzDestroy() {
		const array = this._namesToItems.getArray();
		for (let i = 0, l = array.length; i < l; i++) {
			this._destroyItem(array[i]!.value.item);
		}
	}

	/** Returns true if an item by the name exists. */
	has(name: string): boolean {
		return this._namesToItems.has(name);
	}

	/** Gets an existing item. Make sure to release it later. */
	get(name: string): Item {

		// Get the item.
		const itemEntry = this._namesToItems.get(name);
		if (itemEntry === undefined) {
			throw new Error(`There is no ${this._typeName} with the name '${name}'.`);
		}

		// Inc the count and return it.
		itemEntry.count += 1;
		return itemEntry.item;
	}

	/** Creates a new item. Make sure to release it later. */
	create(name: string): Item {

		// Make sure the name doesn't already exist.
		if (this._namesToItems.has(name)) {
			throw new Error(`There is already a ${this._typeName} with the name '${name}'.`);
		}

		// Create the item.
		const item = this._createItem(name);
		this._namesToItems.set(name, {
			item,
			count: 1
		});

		// Return it.
		return item;
	}

	/** Releases an item. If nothing else is using it, it is destroyed. */
	release(name: string) {

		// Get the item.
		const itemEntry = this._namesToItems.get(name);
		if (!itemEntry) {
			throw new Error(`There is no ${this._typeName} with the name '${name}'.`);
		}

		// Dec the count. If it is zero, destroy the item.
		itemEntry.count -= 1;
		if (itemEntry.count === 0) {
			this._destroyItem(itemEntry.item);
			this._namesToItems.remove(name);
		}
	}

	/** Creates a new mapping of all names to items. */
	getArray(): readonly FastMap.Entry<string, {
		readonly item: Item;
		readonly count: number;
	}>[] {
		return this._namesToItems.getArray();
	}

	/** A name for the type for error output. */
	private _typeName: string;

	/** The create item function. */
	private _createItem: (name: string) => Item;

	/** The destroy item function. */
	private _destroyItem: (item: Item) => void;

	/** The names to item entries map. */
	private _namesToItems = new FastMap<string, {
		item: Item;
		count: number;
	}>();
}
