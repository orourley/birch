import { FastMap } from './containers/fast_map';

/** A collection of optionally named items, with user supplied creation and destruction functions. Meant to be subclassed. */
export abstract class CollectionBase<Item> {

	/** Constructs *this*. */
	constructor(destroyItem: (item: Item) => void) {
		this._destroyItem = destroyItem;
	}

	/** Returns the entry array that can be iterated over (but not modified). */
	getArray() {
		return this._itemsToNames.getArray();
	}

	/** Destroys an item. Takes the item or its name. Returns true if an item was deleted. */
	destroy(nameOrItem: string | Item | undefined): boolean {
		// Destroy allows for undefined to make the API easier.
		if (nameOrItem === undefined) {
			return false;
		}
		// Get the item, if from a name.
		let item: Item;
		if (typeof nameOrItem === 'string') {
			const foundItem = this._namesToItems.get(nameOrItem);
			if (foundItem === undefined) {
				return false;
			}
			item = foundItem;
		}
		else {
			item = nameOrItem;
		}
		if (!this._itemsToNames.has(item)) {
			return false;
		}
		// Destroy the item.
		this._destroyItem(item);
		// If it has a name, delete it from the namesToItems map.
		const name = this._itemsToNames.get(item);
		if (name !== undefined) {
			this._namesToItems.delete(name);
		}
		// Delete it from the itemsToNames map.
		this._itemsToNames.remove(item);
		return true;
	}

	/** Clears all of the items. */
	clear() {
		const array = this._itemsToNames.getArray();
		for (let i = 0, l = array.length; i < l; i++) {
			this._destroyItem(array[i]!.key);
		}
		this._itemsToNames.clear();
		this._namesToItems.clear();
	}

	/** Gets the item with the *name*. Returns undefined if not found. */
	get(name: string): Item | undefined {
		return this._namesToItems.get(name);
	}

	/** Gets the name of the *item*. */
	getName(item: Item): string | undefined {
		return this._itemsToNames.get(item);
	}

	/** Gets the number of items. */
	numItems(): number {
		return this._itemsToNames.getArray().length;
	}

	/** Adds an item with an optional name to the collection.
	 * Only used by subclasses.
	 * If it already exists, it will destroy the item and throw an error. */
	protected addItem(newItem: Item, name?: string) {
		// If given a name, add it to the namesToItems map.
		if (name !== undefined) {
			if (this._namesToItems.has(name)) {
				this._destroyItem(newItem);
				throw new Error(`There is already an item named ${name}`);
			}
			this._namesToItems.set(name, newItem);
		}
		// Add it to the itemsToNames map.
		this._itemsToNames.set(newItem, name);
	}

	/** The destroy item function. */
	private _destroyItem: (item: Item) => void;

	/** The mapping from items to names. The name may be undefined. */
	private _itemsToNames = new FastMap<Item, string | undefined>();

	/** The mapping from names to items. Not every item has a name. */
	private _namesToItems = new Map<string, Item>();
}
