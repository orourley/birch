import { Throttle } from '@orourley/pine-lib';
import type { Controller } from '../internal';

/** The type of menu input directions. */
export type MenuInputDirection = 'up' | 'right' | 'down' | 'left';

/** The types of menu events. */
export type MenuInputEvent = MenuInputDirection | 'submit' | 'cancel';

/** A class for helping handle menu input. */
export class MenuInput {

	/** Constructs the menu input. @param axisIndex The even index of the pair of indices for movement. */
	constructor(controller: Controller, axisIndex: number, callback: (event: MenuInputEvent) => void, options?: { deadZone?: number; }) {
		this._controller = controller;
		this._axisIndex = axisIndex;
		this._callback = callback;
		this._deadZone = options?.deadZone ?? 0.25;

		// Bind callbacks to this.
		this._buttonCallback = this._buttonCallback.bind(this);
		this._axisCallback = this._axisCallback.bind(this);
		this._repeatAxisCallback = this._repeatAxisCallback.bind(this);

		this._controller.buttonCallbacks.add(this._buttonCallback);
		this._controller.axisCallbacks.add(this._axisCallback);

		this._throttle = new Throttle(this._repeatAxisCallback, this._throttleInitialInterval);
	}

	/** Destroys the menu input. */
	destroy() {
		this._throttle.destroy();
		this._controller.buttonCallbacks.remove(this._buttonCallback);
		this._controller.axisCallbacks.remove(this._axisCallback);
	}

	/** Called by the controller whenever any button changes its value. */
	private _buttonCallback(controller: Controller, buttonIndex: number, value: number) {

		// If the controller is disconnected, don't do anything more.
		if (!controller.isActive()) {
			return;
		}

		if (value === 1) {
			if (buttonIndex === 0) {
				this._callback('submit');
			}
			else if (buttonIndex === 1) {
				this._callback('cancel');
			}
		}
	}

	/** Called by the controller whenever any axis changes its value. */
	private _axisCallback(controller: Controller, axisIndex: number) {

		// Ignore the indices of any other axes.
		if (axisIndex !== this._axisIndex && axisIndex !== this._axisIndex + 1) {
			return;
		}

		// If we're in the dead zone, stop doing anything.
		let value0 = controller.getAxisValue(this._axisIndex)!;
		let value1 = controller.getAxisValue(this._axisIndex + 1)!;
		if (Math.abs(value0) <= this._deadZone) {
			value0 = 0;
		}
		if (Math.abs(value1) <= this._deadZone) {
			value1 = 0;
		}

		// Get the direction for non-zero values.
		let direction: MenuInputDirection | 'none';
		if (value0 !== 0) {
			direction = value0 < 0 ? 'left' : 'right';
		}
		else if (value1 !== 0) {
			direction = value1 < 0 ? 'up' : 'down';
		}
		else {
			direction = 'none';
		}

		// If the direction has changed, change the repeat.
		if (this._direction !== direction) {
			this._direction = direction;
			this._throttle.cancelCall();
			this._throttle.interval = this._throttleInitialInterval;
		}

		// Call the repeating callback.
		if (this._direction !== 'none') {
			this._throttle.call();
		}
	}

	private _repeatAxisCallback() {

		// Check if we're still going.
		if (!this._controller.isActive() || this._direction === 'none') {
			return;
		}

		// Call the callback.
		this._callback(this._direction);

		// Gradually decrease the interval so that the repeating goes faster over time.
		this._throttle.interval = Math.max(0.1, this._throttle.interval / 1.1);

		// Call this again. It'll wait until the next throttle interval.
		this._throttle.call();
	}

	/** The controller. */
	private _controller: Controller;

	/** The axis index. */
	private _axisIndex: number;

	/** The throttle. */
	private _throttle: Throttle;

	/** The initial interval when the throttle is reset. */
	private _throttleInitialInterval = 0.25;

	/** The callback. */
	private _callback: (event: MenuInputEvent) => void;

	/** The current direction. */
	private _direction: MenuInputDirection | 'none' = 'none';

	/** The dead zone where all values are zero. */
	private _deadZone: number;
}
