import { Callbacks } from '../internal';

/** The keyboard input system. */
export class KeyboardInput {

	/** Callbacks per key for when the key goes up or down.
	 *  For the keys, refer to https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_key_values. */
	readonly keyCallbacks = new Callbacks<(keyboardInput: KeyboardInput, key: string, down: boolean) => void>();

	/** Constructs the class. */
	constructor() {

		// Bind callbacks to this.
		this._onKeyDown = this._onKeyDown.bind(this);
		this._onKeyUp = this._onKeyDown.bind(this);

		// Add the event listeners to the root element.
		document.addEventListener('keydown', this._onKeyDown);
		document.addEventListener('keyup', this._onKeyUp);
	}

	/** Destroys the class. Called by Input. @internal */
	zzDestroy() {

		// Remove the event listeners from the root element.
		document.removeEventListener('keydown', this._onKeyDown);
		document.removeEventListener('keyup', this._onKeyUp);
	}

	/** Gets whether or not the key is down. For the keys, refer to https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_key_values. */
	isKeyDown(key: string): boolean {
		return this._keysDown.has(key);
	}

	/** When a keyboard key is pressed. */
	private _onKeyDown(event: KeyboardEvent) {

		// Check if the focus is some input or other element.
		if (document.activeElement !== null && document.activeElement !== document.body) {
			return;
		}

		// Record the key as down.
		this._keysDown.add(event.key);

		// Call the callbacks.
		this.keyCallbacks.call(this, event.key, true);
	}

	/** When a keyboard key is released. */
	private _onKeyUp(event: KeyboardEvent) {

		// Check if the focus is some input or other element.
		if (document.activeElement !== null && document.activeElement !== document.body) {
			return;
		}

		// Record the key as up.
		this._keysDown.delete(event.key);

		// Call the callbacks.
		this.keyCallbacks.call(this, event.key, false);
	}

	/** Which keys are down. */
	private _keysDown = new Set<string>();
}
