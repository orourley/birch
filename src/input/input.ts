import type { Engine } from '../internal';
import { ControllersInput } from './controllers_input';
import { MouseInput } from './mouse_input';
import { KeyboardInput } from './keyboard_input';

/** The main class for keyboard, mouse, and controller input. */
export class Input {

	/** The keyboard input. */
	readonly keyboard: KeyboardInput;

	/** The mouse input. */
	readonly mouse: MouseInput;

	/** The controller input. */
	readonly controllers: ControllersInput;

	/** Constructs this. */
	constructor(engine: Engine) {
		this.keyboard = new KeyboardInput();
		this.mouse = new MouseInput(engine);
		this.controllers = new ControllersInput();
	}

	/** Destroys this. Called by Engine. @internal */
	zzDestroy() {
		this.keyboard.zzDestroy();
		this.mouse.zzDestroy();
		this.controllers.zzDestroy();
	}
}
