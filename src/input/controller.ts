import { FastSet } from '../internal';

/** The button callback. The value is 0 (fully up) to 1 (fully down)). */
export type ButtonCallback = (controller: Controller, buttonIndex: number, value: number) => void;

/** The axis callback. The value is from -1 (left or down) to +1 (right or up). */
export type AxisCallback = (controller: Controller, axisIndex: number, value: number) => void;

/** A gamepad controller. */
export class Controller {

	/** The button callbacks. The value is 0 (fully up) to 1 (fully down)). */
	readonly buttonCallbacks = new FastSet<ButtonCallback>();

	/** The axis callbacks. The value is from -1 (left or down) to +1 (right or up). */
	readonly axisCallbacks = new FastSet<AxisCallback>();

	/** Constructs the controller. */
	constructor(gamepad: Gamepad, activeStateChangedCallback: (index: number, active: boolean) => void) {

		// Save the parameters.
		this._gamepad = gamepad;
		this._activeStateChangedCallback = activeStateChangedCallback;

		// Set the initial status of the buttons.
		for (let i = 0; i < this._gamepad.buttons.length; i++) {
			const value = this._gamepad.buttons[i]!.value;
			this._buttons.push(value);

			// If a button is already down, make it immediately active.
			if (value === 1) {
				this._active = true;
			}
		}

		// Set the initial status of the axes.
		for (let i = 0; i < this._gamepad.axes.length; i++) {
			const value = this._gamepad.axes[i]!;
			this._axes.push(value);
		}
	}

	/** Destroys the controller. */
	zzDestroy() {
		this._active = false;
	}

	/** Sets the controller as inactive. It can be reactivated again with a button press. */
	deactivate() {
		if (this._active) {
			this._active = false;
			this._activeStateChangedCallback(this._gamepad.index, false);
		}
	}

	/** Gets the controller index. */
	getIndex(): number {
		return this._gamepad.index;
	}

	/** Gets whether or not the controller is active (a button has been pressed and it hasn't been disconnected). */
	isActive(): boolean {
		return this._active;
	}

	/** Gets the value of the *index* button. 0 means unpressed, 1 means fully pressed, undefined means there is no button. */
	getButtonValue(index: number): number | undefined {
		return this._gamepad.buttons[index]?.value;
	}

	/** Gets the number of buttons. */
	getNumButtons(): number {
		return this._gamepad.buttons.length;
	}

	/** Gets the value of the *index* axis, ranging from -1 to +1, undefined means there is no axis. */
	getAxisValue(index: number): number | undefined {
		return this._gamepad.axes[index];
	}

	/** Gets the number of axes. */
	getNumAxes(): number {
		return this._gamepad.axes.length;
	}

	/** Updates the controller, sending button events to any listeners. @internal */
	zzUpdate() {

		// Go through each button state and call any callbacks.
		for (let buttonIndex = 0; buttonIndex < this._gamepad.buttons.length; buttonIndex++) {
			if (this._buttons[buttonIndex] !== this._gamepad.buttons[buttonIndex]!.value) {
				this._buttons[buttonIndex] = this._gamepad.buttons[buttonIndex]!.value;

				// If the controller is active, call the callbacks.
				if (this._active) {
					const array = this.buttonCallbacks.getArray();
					for (let i = 0; i < array.length; i++) {
						array[i]!(this, buttonIndex, this._buttons[buttonIndex]!);
					}
				}

				// If not, just mark the controller as now active, so it can be used on the next button press.
				else {
					if (this._buttons[buttonIndex] === 1) {
						this._active = true;
						this._activeStateChangedCallback(this._gamepad.index, true);
					}
				}
			}
		}

		// Go through each axis and call the callback if its value has changed.
		for (let axisIndex = 0; axisIndex < this._gamepad.axes.length; axisIndex++) {
			if (this._axes[axisIndex] !== this._gamepad.axes[axisIndex]) {
				this._axes[axisIndex] = this._gamepad.axes[axisIndex]!;

				// If the controller is active, call the callbacks.
				if (this._active) {
					const array = this.axisCallbacks.getArray();
					for (let i = 0; i < array.length; i++) {
						array[i]!(this, axisIndex, this._axes[axisIndex]!);
					}
				}
			}
		}
	}

	/** The browser gamepad object. */
	private _gamepad: Gamepad;

	/** Whether or not the controller is active (a button has been pressed and it hasn't been disconnected). */
	private _active: boolean = false;

	private _activeStateChangedCallback: (index: number, active: boolean) => void;

	/** The current button states. */
	private _buttons: number[] = [];

	/** The current axis states. */
	private _axes: number[] = [];
}
