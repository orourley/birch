import { Callbacks, Controller, FastMap, type FastMapReadOnly } from '../internal';

/** The input system for controllers. */
export class ControllersInput {

	/** Callbacks called when a controller was activated. */
	readonly controllerActivatedCallbacks = new Callbacks<(controllersInput: ControllersInput, controller: Controller) => void>();

	/** Callbacks called when a controller was deactivated. */
	readonly controllerDeactivatedCallbacks = new Callbacks<(controllersInput: ControllersInput, controllerIndex: number) => void>();

	/** Constructs the input system. */
	constructor() {

		// Bind callbacks to this.
		this._gamepadConnected = this._gamepadConnected.bind(this);
		this._gamepadDisconnected = this._gamepadDisconnected.bind(this);

		// Set up the event listeners for the gamepad API connections and disconnections.
		window.addEventListener('gamepadconnected', this._gamepadConnected);
		window.addEventListener('gamepaddisconnected', this._gamepadDisconnected);

		// Find any gamepads already connected before this point.
		const gamepads = navigator.getGamepads();
		for (let i = 0; i < gamepads.length; i++) {
			const gamepad = gamepads[i];
			if (gamepad) {
				const controller = new Controller(gamepad, this._controllerActiveStateChanged.bind(this));
				this._connectedControllers.set(controller.getIndex(), controller);
				if (controller.isActive()) {
					this._activeControllers.set(controller.getIndex(), controller);
				}
			}
		}
	}

	/** Gets the active controllers. */
	getControllers(): FastMapReadOnly<number, Controller> {
		return this._activeControllers;
	}

	/** Destroys the input system. @internal */
	zzDestroy() {

		// Remove the event listeners.
		window.removeEventListener('gamepadconnected', this._gamepadConnected);
		window.removeEventListener('gamepaddisconnected', this._gamepadDisconnected);
	}

	/** Updates the input system. @internal */
	zzUpdate() {

		// Update each of the connected controllers.
		const connectedControllersArray = this._connectedControllers.getArray();
		for (let i = 0, l = connectedControllersArray.length; i < l; i++) {
			const entry = connectedControllersArray[i]!;
			const controller = entry.value;
			controller.zzUpdate();
		}
	}

	/** The 'gamepadconnected' event handler. */
	private _gamepadConnected(event: GamepadEvent) {

		// Add it to the connected controllers.
		if (!this._connectedControllers.has(event.gamepad.index)) {
			const controller = new Controller(event.gamepad, this._controllerActiveStateChanged.bind(this));
			this._connectedControllers.set(controller.getIndex(), controller);
			if (controller.isActive()) {
				this._activeControllers.set(controller.getIndex(), controller);
				this.controllerActivatedCallbacks.call(this, controller);
			}
		}
	}

	/** The 'gamepaddisconnected' event handler. */
	private _gamepadDisconnected(event: GamepadEvent) {

		// Call the deactivated callbacks to notify that they are inactive.
		if (this._activeControllers.has(event.gamepad.index)) {
			this.controllerDeactivatedCallbacks.call(this, event.gamepad.index);
		}

		// Remove any controller entries.
		this._activeControllers.remove(event.gamepad.index);
		this._connectedControllers.remove(event.gamepad.index);
	}

	/** When a controller's active state changes. */
	private _controllerActiveStateChanged(index: number, active: boolean) {
		if (active) {
			const controller = this._connectedControllers.get(index)!;
			this._activeControllers.set(index, controller);
			this.controllerActivatedCallbacks.call(this, controller);
		}
		else {
			this.controllerDeactivatedCallbacks.call(this, index);
			this._activeControllers.remove(index);
		}
	}

	/** The mapping of connected controller indices to controllers. */
	private _connectedControllers = new FastMap<number, Controller>();

	/** The mapping of active controller indices to controllers. Once a button has been pressed, they are in this list. */
	private _activeControllers = new FastMap<number, Controller>();
}
