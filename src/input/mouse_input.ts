import { Callbacks, type Engine, FastMap, Vec2D, type Vec2DReadOnly, type Viewport } from '../internal';

/** The mouse input system.
 *  One caveat: on Firefox, the side mouse buttons often linked to back and forward
 *  don't trigger mousedown and mouseup and so can't be used, but work on chrome. */
export class MouseInput {

	/** The radius of the dead zone. If the mouse stays within this circle after the button is held,
	 *  it won't be considered dragging. Defaults to 5. */
	dragDeadZoneRadius: number = 5;

	/** Callbacks for when the cursor position changed. The cursorPosition is relative to the Birch root element. */
	readonly cursorPositionChangedCallbacks = new Callbacks<(mouseInput: MouseInput, cursorPosition: Vec2DReadOnly) => void>();

	/** Callbacks for when the mouse button goes down.
	 *  For the buttonIndex, usually 0 is the left button, 1 is the middle, and 2 is the right.
	 *  There may be further numbers for extra buttons on the mouse. */
	readonly buttonCallbacks = new Callbacks<(mouseInput: MouseInput, buttonIndex: number, down: boolean) => void>();

	/** Callbacks for when the mouse is clicked (but not dragged).
	 *  For the buttonIndex, usually 0 is the left button, 1 is the middle, and 2 is the right.
	 *  There may be further numbers for extra buttons on the mouse. */
	readonly clickCallbacks = new Callbacks<(mouseInput: MouseInput, buttonIndex: number) => void>();

	/** Callbacks for when the mouse is moved further than the drag dead zone radius while down.
	 *  For the buttonIndex, usually 0 is the left button, 1 is the middle, and 2 is the right.
	 *  There may be further numbers for extra buttons on the mouse. */
	readonly dragStartCallbacks = new Callbacks<(mouseInput: MouseInput, buttonIndex: number) => void>();

	/** Callbacks for when the mouse is moved while dragging.
	 *  For the buttonIndex, usually 0 is the left button, 1 is the middle, and 2 is the right.
	 *  There may be further numbers for extra buttons on the mouse. */
	readonly dragCallbacks = new Callbacks<(mouseInput: MouseInput, buttonIndex: number) => void>();

	/** Callbacks for when the mouse button goes up after a drag. */
	readonly dragEndCallbacks = new Callbacks<(mouseInput: MouseInput, buttonIndex: number) => void>();

	/** Callbacks for when the wheel rotates. The value is either +1 or -1 for up or down. */
	readonly wheelCallbacks = new Callbacks<(mouseInput: MouseInput, value: number) => void>();

	/** Constructs the class. */
	constructor(engine: Engine) {

		// Save the engine.
		this._engine = engine;

		// Bind callbacks to this.
		this._mouseDown = this._mouseDown.bind(this);
		this._mouseUp = this._mouseUp.bind(this);
		this._mouseMove = this._mouseMove.bind(this);
		this._wheel = this._wheel.bind(this);

		// Add the event listeners to the root element.
		this._engine.rootElement.addEventListener('mousedown', this._mouseDown);
		this._engine.rootElement.addEventListener('mouseup', this._mouseUp);
		this._engine.rootElement.addEventListener('mousemove', this._mouseMove);
		this._engine.rootElement.addEventListener('wheel', this._wheel);
		this._engine.rootElement.addEventListener('click', MouseInput._preventDefault);
		this._engine.rootElement.addEventListener('contextmenu', MouseInput._preventDefault);
	}

	/** Destroys the system. Called by Input. */
	zzDestroy() {

		// Remove the event listeners from the root element.
		this._engine.rootElement.removeEventListener('mousedown', this._mouseDown);
		this._engine.rootElement.removeEventListener('mouseup', this._mouseUp);
		this._engine.rootElement.removeEventListener('mousemove', this._mouseMove);
		this._engine.rootElement.removeEventListener('wheel', this._wheel);
		this._engine.rootElement.removeEventListener('click', MouseInput._preventDefault);
		this._engine.rootElement.removeEventListener('contextmenu', MouseInput._preventDefault);
	}

	/** Gets the cursor position relative to the root element. */
	getCursorPosition(): Vec2DReadOnly {
		return this._cursorPosition;
	}

	/** Gets the viewport in which the cursor resides. */
	getCursorViewport(): Viewport | undefined {
		return this._cursorViewport;
	}

	/** Gets the cursor position relative to the viewport in which it resides. */
	getCursorPositionRelViewport(): Vec2DReadOnly {
		return this._cursorPositionRelViewport;
	}

	/** Gets the viewport in which the button down happened.
	 *  Returns undefined if the button was not pressed over a viewport. */
	getButtonDownViewport(buttonIndex: number): Viewport | undefined {
		return this._buttonsDown.get(buttonIndex)?.viewport;
	}

	/** Gets the cursor position of when the button press occurred, relative to the viewport in which the button was pressed.
	 *  Returns undefined if the button was not pressed over a viewport. */
	getButtonDownPressedPosition(buttonIndex: number): Vec2DReadOnly | undefined {
		return this._buttonsDown.get(buttonIndex)?.pressedPosition;
	}

	/** Gets the current cursor position, relative to the viewport in which the button was pressed.
	 *  Returns undefined if the button was not pressed over a viewport. */
	getButtonDownPosition(buttonIndex: number): Vec2DReadOnly | undefined {
		return this._buttonsDown.get(buttonIndex)?.position;
	}

	/** Returns true if the button is currently being dragged.
	 * 	Returns undefined if the button was not pressed over a viewport. */
	isButtonDragging(buttonIndex: number): boolean | undefined {
		return this._buttonsDown.get(buttonIndex)?.dragging;
	}

	/** When a mousedown event occurs on the root element. */
	private _mouseDown(event: MouseEvent) {

		// Make sure the browser things don't happen.
		event.preventDefault();

		// Update the cursor position.
		this._updateCursorPosition(event);

		// Find if it is in the bounds of a viewport,
		let buttonDownInfo;
		const viewports = this._engine.viewports.getArray();
		for (let i = viewports.length - 1; i >= 0; i--) {
			const bounds = viewports[i]!.getBounds();
			if (bounds.contains(this._cursorPosition)) {

				// Save the viewport, pressed position, and position.
				buttonDownInfo = {
					viewport: viewports[i]!,
					position: new Vec2D(),
					pressedPosition: new Vec2D(),
					dragging: false
				};
				buttonDownInfo.pressedPosition.sub(this._cursorPosition, bounds.min);
				buttonDownInfo.position.copy(buttonDownInfo.pressedPosition);

				// Add a window callback so the mouse can leave the root element and still work.
				window.addEventListener('mousemove', this._mouseMove);
				window.addEventListener('mouseup', this._mouseUp);

				// Prevent viewports behind this one from being called.
				break;
			}
		}

		// Record the button as down.
		this._buttonsDown.set(event.button, buttonDownInfo);

		// Call the button down callbacks.
		this.buttonCallbacks.call(this, event.button, true);
	}

	/** When a mouseup event occurs on the root element. */
	private _mouseUp(event: MouseEvent) {

		// Prevent the default browser actions.
		event.preventDefault();

		// Update the cursor position.
		this._updateCursorPosition(event);

		// Get the button down info.
		const buttonDownInfo = this._buttonsDown.get(event.button);

		// If we're currently in mouse down on a viewport,
		if (buttonDownInfo) {

			// Remove the event listeners added when the button was pressed.
			window.removeEventListener('mousemove', this._mouseMove);
			window.removeEventListener('mouseup', this._mouseUp);

			// Make sure the button down viewport still is valid (may have disappeared between down and up).
			// We save the dragging for the callback conditional below.
			let dragging = false;
			if (this._engine.viewports.has(buttonDownInfo.viewport)) {

				// Update the cursor position.
				buttonDownInfo.position.sub(this._cursorPosition, buttonDownInfo.viewport.getBounds().min);
				dragging = buttonDownInfo.dragging;
				buttonDownInfo.dragging = false;
			}
			else {

				// Clear out the viewport-related variables.
				this._buttonsDown.set(event.button, undefined);
			}

			// Call the drag end and click callbacks.
			if (dragging) {
				this.dragEndCallbacks.call(this, event.button);
			}
			else {
				this.clickCallbacks.call(this, event.button);
			}
		}

		// Call the button up callbacks.
		this.buttonCallbacks.call(this, event.button, false);

		// Record the button as up.
		this._buttonsDown.remove(event.button);
	}

	/** When a mousemove event occurs on the root element. */
	private _mouseMove(event: MouseEvent) {

		// Prevent the default browser actions.
		event.preventDefault();

		// Update the cursor position.
		this._updateCursorPosition(event);

		// Go through the button infos for each button currently pressed to update them.
		const buttonsDownArray = this._buttonsDown.getArray();
		for (let i = 0, l = buttonsDownArray.length; i < l; i++) {
			const entry = buttonsDownArray[i]!;
			const button = entry.key;
			const buttonDownInfo = entry.value;

			// If we're currently in mouse down on a viewport,
			if (buttonDownInfo) {

				// Make sure the button down viewport still is valid (may have disappeared between down and up).
				if (this._engine.viewports.has(buttonDownInfo.viewport)) {

					// Update the cursor position.
					buttonDownInfo.position.sub(this._cursorPosition, buttonDownInfo.viewport.getBounds().min);

					// If we're not dragging, check if we should start.
					if (!buttonDownInfo.dragging) {

						// If the drag offset is large enough, then start dragging.
						const dragOffset = Vec2D.pool.get();
						dragOffset.sub(buttonDownInfo.position, buttonDownInfo.pressedPosition);
						if (dragOffset.normSq() >= this.dragDeadZoneRadius * this.dragDeadZoneRadius) {
							buttonDownInfo.dragging = true;
							this.dragStartCallbacks.call(this, button);
						}
						Vec2D.pool.release(dragOffset);
					}
					else {
						this.dragCallbacks.call(this, button);
					}
				}

				// Not a valid viewport.
				else {

					// Clear out the viewport-related variables.
					this._buttonsDown.set(event.button, undefined);

					// Call the drag end and click callbacks, since this button down is no longer valid.
					if (buttonDownInfo.dragging) {
						this.dragEndCallbacks.call(this, event.button);
					}
					else {
						this.clickCallbacks.call(this, event.button);
					}

				}
			}
		}
	}

	/** When a wheel event occurs on the root element. */
	private _wheel(event: WheelEvent) {

		// Prevent the default browser actions.
		event.preventDefault();

		// Call the callbacks.
		this.wheelCallbacks.call(this, -Math.sign(event.deltaY));
	}

	/** Updates the cursor position, calling the cursorChanged callback if necessary. */
	private _updateCursorPosition(event: MouseEvent) {

		// Get the position.
		const x = event.pageX - this._engine.rootElement.offsetLeft;
		const y = event.pageY - this._engine.rootElement.offsetTop;
		if (x !== this._cursorPosition.x || y !== this._cursorPosition.y) {
			this._cursorPosition.set(x, y);
			this._cursorPosition.mult(this._cursorPosition, devicePixelRatio);
		}

		// Find if it is in the bounds of a viewport.
		const viewports = this._engine.viewports.getArray();
		this._cursorViewport = undefined;
		this._cursorPositionRelViewport.set(NaN, NaN);
		for (let i = viewports.length - 1; i >= 0; i--) {
			const bounds = viewports[i]!.getBounds();
			if (bounds.contains(this._cursorPosition)) {

				// Save the viewport and cursor position relative to the viewport.
				this._cursorViewport = viewports[i];
				this._cursorPositionRelViewport.sub(this._cursorPosition, bounds.min);
			}
		}

		// Call the callbacks.
		this.cursorPositionChangedCallbacks.call(this, this._cursorPosition);
	}

	/** Prevents the default browser action of any event. */
	private static _preventDefault(event: Event) {
		event.preventDefault();
	}

	/** The root element of the engine. */
	private _engine: Engine;

	/** The cursor position relative to the root element. */
	private _cursorPosition = new Vec2D(NaN, NaN);

	/** The viewport in which the cursor resides. */
	private _cursorViewport: Viewport | undefined;

	/** The cursor position relative to the viewport in which it resides. */
	private _cursorPositionRelViewport = new Vec2D(NaN, NaN);

	/** Which keys are down, and the viewport info in which they were pressed, if any. The positions are relative to the viewport. */
	private _buttonsDown = new FastMap<number, { viewport: Viewport; pressedPosition: Vec2D; position: Vec2D; dragging: boolean; } | undefined>();
}
