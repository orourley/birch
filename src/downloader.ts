import type { JsonType } from '@orourley/pine-lib';
import * as JSON5 from 'json5';

/** The interface of a download. */
export interface Download<Type> {
	content: Type;
	type: string | undefined;
}

/** A downloader. */
export class Downloader {

	/** Downloads json from the url. */
	async getJson5(url: string): Promise<Download<JsonType>> {
		const download = await this.getText(url);
		return {
			content: JSON5.parse(download.content),
			type: download.type
		};
	}

	/** Downloads text from the url. */
	async getText(url: string): Promise<Download<string>> {
		const download = await this.getBinary(url);
		const textDecoder = new TextDecoder('utf-8');
		return {
			content: textDecoder.decode(download.content),
			type: download.type
		};
	}

	/** Downloads data from the url and returns the response if successful. */
	async getBinary(url: string): Promise<Download<ArrayBuffer>> {
		if (this._downloads.has(url)) {
			return this._downloads.get(url)!;
		}
		else {
			const promise = fetch(url).then(async (response: Response) => {
				this._downloads.delete(url);
				if (200 <= response.status && response.status < 300) { // Any 2xx response
					const content = await response.arrayBuffer();
					return {
						content,
						type: response.headers.get('Content-Type') ?? undefined
					};
				}
				else {
					throw new Error(`Download of ${url} failed. ${response.status} ${response.statusText}.`);
				}
			});
			this._downloads.set(url, promise);
			return promise;
		}
	}

	// The set of active downloads.
	private _downloads = new Map<string, Promise<Download<ArrayBuffer>>>();
}
