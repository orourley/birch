import {
	Downloader,
	FPS,
	Input,
	Renderer,
	Audio,
	Viewport,
	World,
	Items
} from './internal';

/** A type for when we add component types to window.UI. */
type WindowWithBirch = Window & typeof globalThis & { birch: Engine; };

/** The Birch engine. */
export class Engine {

	/** The root element. */
	readonly rootElement: HTMLElement;

	/** The FPS system. */
	readonly fps: FPS = new FPS();

	/** The render system. */
	readonly renderer: Renderer;

	/** The downloader. */
	readonly downloader: Downloader;

	/** The audio system. */
	readonly audio: Audio;

	/** The input system. */
	readonly input: Input;

	/** The worlds. */
	readonly worlds: Items<World>;

	/** The viewports. They are ordered. */
	readonly viewports: Items<Viewport>;

	/** Constructs the engine. */
	constructor(rootElement: HTMLElement) {

		// Set and prepare the root element.
		this.rootElement = rootElement;
		this._prepareRootElement();

		// Create the downloader.
		this.downloader = new Downloader();

		// Create the renderer.
		this.renderer = new Renderer(this._canvas, true, this);

		// Create the input system.
		this.input = new Input(this);

		// Create the sound system.
		this.audio = new Audio();

		// Set up the viewports.
		this.viewports = new Items(() => new Viewport(this.renderer, this._viewportsElement), (viewport: Viewport) => { viewport.zzDestroy(); });
		this.viewports.setOrdering('ordered');

		// Set up the worlds.
		this.worlds = new Items(() => new World(this), (world: World) => { world.zzDestroy(); });

		// Run the engine.
		this._running = true;
		this._run = this._run.bind(this);
		this._lastTime = Date.now();
		this._run();

		(window as WindowWithBirch).birch = this;
	}

	/** Destroys the engine. */
	destroy() {
		this.audio.zzDestroy();
		this.input.zzDestroy();
		this.worlds.zzDestroy();
		this.viewports.zzDestroy();
		this.renderer.zzDestroy();
		this.rootElement.innerHTML = '';
	}

	/** Gets the time elapsed since the last frame. */
	getDeltaTime(): number {
		return this._deltaTime;
	}

	/** Stops the engine. */
	stop() {
		this._running = false;
	}

	// /** Adds an update callback. */
	// addUpdateCallback(callback: (deltaTime: number) => void) {
	// 	this._updateCallbacks.push(callback);
	// }

	// /** Removes an updated callback. */
	// removeUpdateCallback(callback: (deltaTime: number) => void) {
	// 	for (let i = 0; i < this._updateCallbacks.length; i++) {
	// 		if (this._updateCallbacks[i] === callback) {
	// 			this._updateCallbacks.splice(i, 1);
	// 			break;
	// 		}
	// 	}
	// }

	/** Runs the main engine loop. */
	private _run() {

		// Check if not running, and if not, destroy the engine and wrap it all up.
		if (!this._running) {
			this.destroy();
			return;
		}

		// Get the time elapsed since the last frame.
		const timeNow = Date.now();
		this._deltaTime = (timeNow - this._lastTime) / 1000.0;
		this._lastTime = timeNow;
		this.fps.add(this._deltaTime);

		// Check the canvas size and resize if needed.
		const canvas = this._canvas;
		if (canvas.width !== canvas.clientWidth * devicePixelRatio) {
			canvas.width = canvas.clientWidth * devicePixelRatio;
		}
		if (canvas.height !== canvas.clientHeight * devicePixelRatio) {
			canvas.height = canvas.clientHeight * devicePixelRatio;
		}

		// Update the bounds of the viewports.
		const viewportsArray = this.viewports.getArray();
		for (let i = 0, l = viewportsArray.length; i < l; i++) {
			viewportsArray[i]!.zzUpdateBounds();
		}

		// Update the controller inputs.
		this.input.controllers.zzUpdate();

		// Update the worlds.
		const worldsArray = this.worlds.getArray();
		for (let i = 0, l = worldsArray.length; i < l; i++) {
			worldsArray[i]!.zzUpdate();
		}

		// // Call the update callbacks.
		// for (let i = 0; i < this._updateCallbacks.length; i++) {
		// 	this._updateCallbacks[i](this.deltaTime);
		// }

		// Render all of the stages.
		this.renderer.zzRender();

		// Ask the browser for another frame.
		requestAnimationFrame(this._run);
	}

	/** Prepares the root element, adding styles and child elements. */
	private _prepareRootElement() {

		// Remove all children.
		while (this.rootElement.lastChild !== null) {
			this.rootElement.removeChild(this.rootElement.lastChild);
		}

		// Make sure it is a positioned element so that absolutely positioned children will work.
		if (this.rootElement.style.position === '' || this.rootElement.style.position === 'static') {
			this.rootElement.style.position = 'relative';
		}

		// Make it have no user interaction so child elements can specify the user interaction directly.
		this.rootElement.style.userSelect = 'none';
		this.rootElement.style.touchAction = 'none';

		// Add a canvas.
		this._canvas = document.createElement('canvas');
		this._canvas.style.position = 'absolute';
		this._canvas.style.left = '0';
		this._canvas.style.top = '0';
		this._canvas.style.width = '100%';
		this._canvas.style.height = '100%';
		this._canvas.style.imageRendering = 'crisp-edges';
		this._canvas.style.imageRendering = 'pixelated';
		this._canvas.width = this._canvas.clientWidth * devicePixelRatio;
		this._canvas.height = this._canvas.clientHeight * devicePixelRatio;
		this.rootElement.appendChild(this._canvas);

		// Add a viewports div.
		this._viewportsElement = document.createElement('div');
		this._viewportsElement.classList.add('viewports');
		this._viewportsElement.style.position = 'absolute';
		this._viewportsElement.style.left = '0';
		this._viewportsElement.style.top = '0';
		this._viewportsElement.style.width = '100%';
		this._viewportsElement.style.height = '100%';
		this._viewportsElement.style.overflow = 'hidden';
		this.rootElement.appendChild(this._viewportsElement);
	}

	/** The canvas. */
	private _canvas!: HTMLCanvasElement;

	/** The viewports element. */
	private _viewportsElement!: HTMLDivElement;

	/** A flag that says whether or not the engine is running. */
	private _running: boolean = false;

	/** The time last frame. */
	private _lastTime: number = Date.now();

	/** The time elapsed since the last frame. */
	private _deltaTime: number = 0;

	// /** The update callbacks. */
	// private _updateCallbacks: ((deltaTime: number) => void)[] = [];

}
