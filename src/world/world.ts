import {
	type Component,
	type ComponentType,
	type Engine,
	Entity,
	FastMap,
	FastSet,
	Query,
	type Scene,
	type System,
	type SystemType
} from '../internal';

export class World {

	/** The engine that contains this. */
	readonly engine: Engine;

	/** The render scene. */
	readonly scene: Scene;

	/** Constructor. */
	constructor(engine: Engine) {

		// Set the engine that contains this.
		this.engine = engine;

		// Get a new scene.
		this.scene = engine.renderer.scenes.create();
	}

	/** Destroys the world. */
	zzDestroy() {

		// Destroy the entities.
		const entitiesArray = this._entities.getArray();
		for (let i = 0, l = entitiesArray.length; i < l; i++) {
			entitiesArray[i]!.value.zzDestroy();
		}

		// Destroy the systems.
		for (const system of this._systemOrder) {
			system.destroy();
		}

		// Release the scene.
		this.engine.renderer.scenes.destroy(this.scene);
	}

	/** Gets an entity with the given name. */
	getEntity(name: string): Entity | undefined {
		return this._entities.get(name);
	}

	/** Adds an entity with the given name. It will be added to the world in the next frame. */
	addEntity(name: string): Entity {

		// Check to see if the name already exists.
		if (this._entities.has(name)) {
			throw new Error(`While adding an entity, the name "${name}" already exists.`);
		}

		// Create the new entity.
		const entity = new Entity(this, name);

		// Add it to the list of entities to be added to the world.
		this._entities.set(name, entity);

		// Return it.
		return entity;
	}

	/** Removes an entity. */
	removeEntity(entity: Entity) {

		// Check to see if the name exists.
		if (!this._entities.has(entity.name)) {
			throw new Error(`While removing a entity, the name "${entity.name}" does not exist.`);
		}

		// Remove it.
		this._entities.remove(entity.name);
		entity.zzDestroy();
	}

	/** Gets a system. */
	getSystem<Type extends System>(systemType: SystemType<Type>): Type | undefined {
		return this._systems.get(systemType) as Type | undefined;
	}

	/** Adds a system. */
	addSystem<Type extends System>(SystemType: SystemType<Type>, beforeSystem?: System): Type {

		// Check if it already exists.
		if (this._systems.has(SystemType)) {
			throw new Error(`While adding a system, the system of type "${SystemType.name}" already exists.`);
		}

		// Get the beforeSystem's index or the last index.
		let beforeSystemIndex = this._systemOrder.length;
		if (beforeSystem !== undefined) {
			beforeSystemIndex = this._systemOrder.indexOf(beforeSystem);
			if (beforeSystemIndex === -1) {
				throw new Error(`While adding a system of type "${SystemType.name}", the beforeSystem of type "${beforeSystem.constructor.name}" does not exist.`);
			}
		}

		// Create and add the system.
		const system = new SystemType(this);
		this._systems.set(SystemType, system);

		// Add it to the right spot in the order.
		this._systemOrder.splice(beforeSystemIndex, 0);

		// Return it.
		return system;
	}

	/** Removes a system. */
	removeSystem(systemType: SystemType) {

		// Check that the system exists.
		const system = this._systems.get(systemType);
		if (system === undefined) {
			throw new Error(`While removing a system, the system of type "${systemType.name}" does not exist.`);
		}

		// Remove the system from the order.
		for (let i = this._systemOrder.length - 1; i >= 0; i--) {
			if (this._systemOrder[i] === system) {
				this._systemOrder.splice(i, 1);
				break;
			}
		}

		// Remove the system.
		this._systems.delete(systemType);
		system.destroy();
	}

	/** Adds a new query. The test, if undefined, defaults to an AND of all types. */
	addQuery(params: {
		types: readonly ComponentType[];
		test?: (entity: Entity) => boolean;
		added?: (entity: Entity) => void;
		removed?: (entity: Entity) => void;
	}): Query {

		// Create the query.
		const query: Query = {
			types: new FastSet(params.types),
			entities: new FastSet(),
			test: params.test ?? Query.defaultTest.bind(undefined, params.types),
			added: params.added,
			removed: params.removed
		};

		// Go through each watched component type and populate the queries.
		const queryTypesArray = query.types.getArray();
		for (let i = 0, l = queryTypesArray.length; i < l; i++) {
			const componentType = queryTypesArray[i]!;

			// Get or add the queries for that component type.
			let queries = this._queries.get(componentType);
			if (!queries) {
				queries = new FastSet();
				this._queries.set(componentType, queries);
			}

			// Add the query to the queries for that component type.
			queries.add(query);
		}

		// Go through each entity to see if it should be added to the query.
		const entitiesArray = this._entities.getArray();
		for (let i = 0, l = entitiesArray.length; i < l; i++) {
			const entity = entitiesArray[i]!.value;
			if (query.test(entity)) {
				query.entities.add(entity);
				if (query.added !== undefined) {
					query.added(entity);
				}
			}
		}

		// Return the query.
		return query;
	}

	/** Removes a query. */
	removeQuery(query: Query) {

		// Go through each watched type.
		const queryTypesArray = query.types.getArray();
		for (let i = 0, l = queryTypesArray.length; i < l; i++) {
			const componentType = queryTypesArray[i]!;

			// Get the query set of the component type.
			const queries = this._queries.get(componentType);
			if (!queries) {
				continue;
			}

			// Remove it.
			queries.remove(query);
		}
	}

	/** @internal Notifies relevant queries that a component was added. */
	zzComponentAdded(component: Component) {

		// Get the list of queries for the component type.
		const queries = this._queries.get(component.type);
		if (!queries) {
			return;
		}

		// Go through each query.
		const entity = component.entity;
		const queriesArray = queries.getArray();
		for (let i = 0, l = queriesArray.length; i < l; i++) {
			const query = queriesArray[i]!;

			// If it's not in the query's entity list and it passes the test,
			// Add it to the entities and mark it as added.
			if (!query.entities.has(entity) && query.test(entity)) {
				query.entities.add(entity);
				if (query.added) {
					query.added(entity);
				}
			}
		}
	}

	/** @internal Notifies relevant queries that a component will be removed. */
	zzComponentRemoved(entity: Entity, componentType: ComponentType) {

		// Get the list of queries for the component type.
		const queries = this._queries.get(componentType);
		if (!queries) {
			return;
		}

		// Go through each query.
		const queriesArray = queries.getArray();
		for (let i = 0, l = queriesArray.length; i < l; i++) {
			const query = queriesArray[i]!;

			// If it's not in the query's entity list and it fails the test,
			// Add it to the entities and mark it as added.
			if (query.entities.has(entity) && !query.test(entity)) {
				query.entities.remove(entity);
				if (query.removed) {
					query.removed(entity);
				}
			}
		}
	}

	/** @internal Updates the world. Called by Engine. */
	zzUpdate() {

		// Run update on all of the systems.
		for (let i = 0, l = this._systemOrder.length; i < l; i++) {
			this._systemOrder[i]!.update();
		}
	}

	/** The entities, mapped by name to their component types. */
	private _entities = new FastMap<string, Entity>();

	/** A mapping of component types to the queries that use them. */
	private _queries = new FastMap<ComponentType, FastSet<Query>>();

	/** The systems. */
	private _systems = new Map<SystemType, System>();

	/** The system update order. */
	private _systemOrder: System[] = [];
}
