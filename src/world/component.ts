import type { Callbacks, Entity } from '../internal';

/** A type of a component. */
export type ComponentType<Type extends Component = Component> = new (entity: Entity) => Type;

/** A component, which contains data and can send changed events. */
export abstract class Component {

	/** The entity. */
	readonly entity: Entity;

	/** The type. */
	readonly type: ComponentType;

	/** The callbacks to call when an event it sent. */
	readonly callbacks = new Map<number, Callbacks<(component: Component, event: number) => void>>();

	static readonly AnyEvent = -1;

	/** The constructor. */
	constructor(entity: Entity) {
		this.entity = entity;
		this.type = this.constructor as ComponentType;
	}

	/** The destructor. Called by Entity. */
	zzDestroy() {
	}

	/** Notifies subscribed systems of the event. Can be called by any component's set function. */
	protected sendEvent(event: number) {
		const callbacks = this.callbacks.get(event);
		if (callbacks) {
			const callbacksArray = callbacks.getArray();
			for (let i = 0, l = callbacksArray.length; i < l; i++) {
				callbacksArray[i]!(this, event);
			}
		}
		// Also send the "any" callbacks.
		const anyCallbacks = this.callbacks.get(Component.AnyEvent);
		if (anyCallbacks) {
			const anyCallbacksArray = anyCallbacks.getArray();
			for (let i = 0, l = anyCallbacksArray.length; i < l; i++) {
				anyCallbacksArray[i]!(this, event);
			}
		}
	}
}
