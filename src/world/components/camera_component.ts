import { CallbackValue, Component, type Stage } from '../../internal';

export class CameraComponent extends Component {

	readonly stage = new CallbackValue<this, Stage | undefined>(this, undefined);

	readonly near = new CallbackValue<this, number>(this, 1);

	readonly far = new CallbackValue<this, number>(this, 2);

	readonly fov = new CallbackValue<this, number>(this, Math.PI / 2);
}
