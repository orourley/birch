import { CallbackValue, CallbackValueCopy, Color, type ColorReadOnly, Component } from '../../internal';

export class LightComponent extends Component {

	/** The color of the light. */
	readonly color = new CallbackValueCopy<this, Color, ColorReadOnly>(this, new Color(1, 1, 1));

	/** The attenuation of the light. */
	readonly attentuation = new CallbackValue<this, number>(this, 0.0);
}
