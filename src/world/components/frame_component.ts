import { CallbackValueCopy, Component, Quat, type QuatReadOnly, Vec3D, type Vec3DReadOnly } from '../../internal';

/** A component that contains a position and orientation. */
export class FrameComponent extends Component {

	readonly position = new CallbackValueCopy<this, Vec3D, Vec3DReadOnly>(this, new Vec3D());

	readonly orientation = new CallbackValueCopy<this, Quat, QuatReadOnly>(this, new Quat());

	/** Converts a local position to a world position. *Local space* is the position with respect to the position
	 *  and orientation of the camera's frame component. *World space* is the general position with respect to the
	 *  world's origin at [0, 0, 0]. */
	convertLocalToWorldPosition(worldPosition: Vec3D, localPosition: Vec3DReadOnly) {
		worldPosition.rotate(this.orientation.getValue(), localPosition);
		worldPosition.add(worldPosition, this.position.getValue());
	}

	/** Converts a world position to a local position. *Local space* is the position with respect to the position
	 *  and orientation of the camera's frame component. *World space* is the general position with respect to the
	 *  world's origin at [0, 0, 0]. */
	convertWorldToLocalPosition(localPosition: Vec3D, worldPosition: Vec3DReadOnly) {
		localPosition.sub(worldPosition, this.position.getValue());
		localPosition.rotateInverse(this.orientation.getValue(), localPosition);
	}
}
