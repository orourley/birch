import { Component, type Entity, type Model } from '../../internal';

export class ModelComponent extends Component {

	/** The model. */
	readonly model: Model;

	/** Constructs the component. */
	constructor(entity: Entity) {
		super(entity);

		this.model = entity.world.scene.models.create();
	}

	/** Destroys the component. */
	override zzDestroy() {
		this.entity.world.scene.models.destroy(this.model);
	}
}
