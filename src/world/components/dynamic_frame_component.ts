import { Component, Quat, Vec3D } from '../../internal';

export class DynamicFrameComponent extends Component {

	/** The position. */
	position = new Vec3D();

	/** The orientation. */
	orientation = new Quat();
}
