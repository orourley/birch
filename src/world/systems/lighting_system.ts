import { type Entity, FrameComponent, LightComponent, type Query, System, Vec3D, type World } from '../../internal';

/** A system for handling lighting. */
export class LightingSystem extends System {

	/** Constructs the system. */
	constructor(world: World) {
		super(world);

		world.scene.getUniformGroup().setUpUniformBlock(`
			struct Light {
				mediump vec3 position;
				mediump vec3 direction;
				mediump vec3 color;
				mediump float dotSpreadMin;
				mediump float dotSpreadMax;
				mediump float attenuation;
			};
			uniform scene {
				Light lights[3];
			};`);

		// if (!world.scene.uniformGroup.hasUniform('lightColors')) {
		// 	throw new Error(`Scene uniform group must have 'lightColors: vec3[5].`);
		// }
		// if (!world.scene.uniformGroup.hasUniform('lightPositions')) {
		// 	throw new Error(`Scene uniform group must have 'lightPositions: vec3[5].`);
		// }

		// Add a query to monitor camera and frame components.
		this._lightQuery = this.world.addQuery({
			types: [FrameComponent, LightComponent],
			added: (entity: Entity) => {
				const frame = entity.get(FrameComponent)!;
				frame.position.addCallback(this._frameChanged);
				frame.orientation.addCallback(this._frameChanged);
				const light = entity.get(LightComponent)!;
				light.color.addCallback(this._lightChanged);
				light.attentuation.addCallback(this._lightChanged);
				this._frameChanged(frame);
				this._lightChanged(light);
			},
			removed: (entity: Entity) => {
				const frame = entity.get(FrameComponent);
				if (frame) {
					frame.position.removeCallback(this._frameChanged);
					frame.orientation.removeCallback(this._frameChanged);
				}
				const light = entity.get(LightComponent);
				if (light) {
					light.color.removeCallback(this._lightChanged);
				}
			}
		});
	}

	override destroy() {
		this.world.removeQuery(this._lightQuery);
	}

	private _frameChanged = (frame: FrameComponent) => {
		const sceneUniformGroup = this.world.scene.getUniformGroup();
		sceneUniformGroup.setUniform('lights[0].position', frame.position.getValue().array);
		const forward = Vec3D.pool.get();
		forward.setFromQuatAxis(frame.orientation.getValue(), 1);
		sceneUniformGroup.setUniform('lights[0].direction', forward.array);
		Vec3D.pool.release(forward);

		/** Two things:
		 * I need to be able to set a single index in an array of light positions.
		 * I need to in the constructor add on uniform type. Right now there is nothing that sets the uniform types, but if there are multiple systems that do,
		 * this needs to be able to modify the types. And destroy would remove the uniform type.
		 */
	};

	private _lightChanged = (light: LightComponent) => {
		const sceneUniformGroup = this.world.scene.getUniformGroup();
		sceneUniformGroup.setUniform('lights[0].color', light.color.getValue().array);
		sceneUniformGroup.setUniform('lights[0].attenuation', light.attentuation.getValue());
	};

	private _lightQuery: Query;
}
