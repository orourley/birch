import { type Entity, FrameComponent, Mat44, ModelComponent, type Query, System, Transforms, type World } from '../../internal';
import { DynamicFrameComponent } from '../components/dynamic_frame_component';

/** The system for handling models. */
export class ModelSystem extends System {

	/** Constructs the system. */
	constructor(world: World) {
		super(world);

		// Add a query to monitor camera and frame components.
		this._modelQuery = this.world.addQuery({
			types: [FrameComponent, ModelComponent],
			added: (entity: Entity) => {
				const frame = entity.get(FrameComponent)!;
				frame.position.addCallback(this._frameOrModelChanged);
				frame.orientation.addCallback(this._frameOrModelChanged);
				this._frameOrModelChanged(frame);
			},
			removed: (entity: Entity) => {
				const frame = entity.get(FrameComponent);
				if (frame) {
					frame.position.removeCallback(this._frameOrModelChanged);
					frame.orientation.removeCallback(this._frameOrModelChanged);
				}
			}
		});

		// Add a query to monitor camera and frame components.
		this._dynamicModelQuery = this.world.addQuery({
			types: [DynamicFrameComponent, ModelComponent]
		});
	}

	/** Destroys the system. */
	override destroy() {
		this.world.removeQuery(this._dynamicModelQuery);
		this.world.removeQuery(this._modelQuery);
	}

	/** Updates the system. */
	override update() {

		// Go through each of the dynamic frames to update the localToWorld uniform on the model.
		const entitiesArray = this._dynamicModelQuery.entities.getArray();
		for (let i = 0, l = entitiesArray.length; i < l; i++) {
			const entity = entitiesArray[i]!;
			const dynFrame = entity.get(DynamicFrameComponent)!;
			const model = entity.get(ModelComponent)!;
			const localToWorld = Mat44.pool.get();
			Transforms.localToWorld(localToWorld, dynFrame.position, dynFrame.orientation);
			model.model.getUniformGroup().setUniform('localToWorld', localToWorld.array);
			Mat44.pool.release(localToWorld);
		}
	}

	private _frameOrModelChanged = (component: FrameComponent | ModelComponent) => {
		const frame = component.entity.get(FrameComponent)!;
		const model = component.entity.get(ModelComponent)!;
		const localToWorld = Mat44.pool.get();
		Transforms.localToWorld(localToWorld, frame.position.getValue(), frame.orientation.getValue());
		model.model.getUniformGroup().setUniform('localToWorld', localToWorld.array);
		Mat44.pool.release(localToWorld);
	};

	private _modelQuery: Query;

	private _dynamicModelQuery: Query;
}
