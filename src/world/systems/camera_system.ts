import { CameraComponent, type Entity, FrameComponent, Mat44, type Query, System, Transforms, Vec3D, type Vec3DReadOnly, type World } from '../../internal';

/** The system for handling cameras. */
export class CameraSystem extends System {

	/** Constructs the system. */
	constructor(world: World) {
		super(world);

		// Add a query to monitor camera and frame components.
		this._cameraQuery = this.world.addQuery({
			types: [FrameComponent, CameraComponent],
			added: (entity: Entity) => {
				const frame = entity.get(FrameComponent)!;
				const camera = entity.get(CameraComponent)!;
				this._cameraToNdcTransforms.set(entity, new Mat44());
				this._worldToCameraTransforms.set(entity, new Mat44());
				frame.position.addCallback(this._frameChanged);
				frame.orientation.addCallback(this._frameChanged);
				camera.near.addCallback(this._cameraChanged);
				camera.far.addCallback(this._cameraChanged);
				camera.fov.addCallback(this._cameraChanged);
				camera.stage.addCallback(this._stageChanged);
				this._stageChanged(camera);
				this._frameChanged(frame);
				this._cameraChanged(camera);
			},
			removed: (entity: Entity) => {
				const frame = entity.get(FrameComponent);
				if (frame) {
					frame.position.addCallback(this._frameChanged);
					frame.orientation.addCallback(this._frameChanged);
				}
				const camera = entity.get(CameraComponent);
				if (camera) {
					camera.near.addCallback(this._cameraChanged);
					camera.far.addCallback(this._cameraChanged);
					camera.fov.addCallback(this._cameraChanged);
					camera.stage.addCallback(this._stageChanged);
				}
				this._cameraToNdcTransforms.delete(entity);
				this._worldToCameraTransforms.delete(entity);
			}
		});
	}

	/** Destroys the system. */
	override destroy() {
		this.world.removeQuery(this._cameraQuery);
	}

	/** Converts a local position to an NDC position. *NDC space* is [-1, -1] in the bottom left corner,
	 *  [+1, +1] in the top right corner, with the z component being +1 on the near plane and -1 on the far plane.
	 *  *Local space* is the position with respect to the position and orientation of the camera's frame component.
	 *  Right is *x*, forward is *y*, and up is *z*. */
	convertLocalToNdcPosition(cameraEntity: Entity, ndcPosition: Vec3D, localPosition: Vec3DReadOnly) {
		const localToNdc = this._cameraToNdcTransforms.get(cameraEntity);
		if (localToNdc !== undefined) {
			const wNdcInv = 1 / localPosition.y;
			ndcPosition.x = localToNdc.get(0, 0)! * localPosition.x * wNdcInv;
			ndcPosition.y = localToNdc.get(1, 2)! * localPosition.z * wNdcInv;
			ndcPosition.z = localToNdc.get(2, 1)! + localToNdc.get(2, 3)! * wNdcInv;
		}
		else {
			ndcPosition.copy(Vec3D.NaN);
		}
	}

	/** Converts an NDC position to a local position. *NDC space* is [-1, -1] in the bottom left corner,
	 *  [+1, +1] in the top right corner, with the z component being +1 on the near plane and -1 on the far plane.
	 *  *Local space* is the position with respect to the position and orientation of the camera's frame component.
	 *  Right is *x*, forward is *y*, and up is *z*. */
	convertNdcToLocalPosition(cameraEntity: Entity, localPosition: Vec3D, ndcPosition: Vec3DReadOnly) {
		const localToNdc = this._cameraToNdcTransforms.get(cameraEntity);
		if (localToNdc) {
			const wLocalInv = localToNdc.get(2, 3)! / (ndcPosition.z - localToNdc.get(2, 1)!);
			localPosition.x = ndcPosition.x / localToNdc.get(0, 0)! * wLocalInv;
			localPosition.z = ndcPosition.y / localToNdc.get(1, 2)! * wLocalInv;
			localPosition.y = wLocalInv; // Needs to be last since it might be the same as ndcPosition.
		}
		else {
			localPosition.copy(Vec3D.NaN);
		}
	}

	private _frameChanged = (frame: FrameComponent) => {
		const camera = frame.entity.get(CameraComponent)!;
		const stage = camera.stage.getValue();
		if (stage) {
			const worldToCamera = this._worldToCameraTransforms.get(camera.entity)!;
			Transforms.worldToLocal(worldToCamera, frame.position.getValue(), frame.orientation.getValue());
			stage.getUniformGroup().setUniform('worldToCamera', worldToCamera.array);
		}
	};

	private _cameraChanged = (camera: CameraComponent) => {
		const stage = camera.stage.getValue();
		if (stage !== undefined) {
			const cameraToNdc = this._cameraToNdcTransforms.get(camera.entity)!;
			Transforms.localToNdcPerspective(cameraToNdc, camera.fov.getValue(), stage.getBounds().size.x / stage.getBounds().size.y,
				camera.near.getValue(), camera.far.getValue(), true);
			stage.getUniformGroup().setUniform('cameraToNdc', cameraToNdc.array);
		}
	};

	private _stageChanged = (camera: CameraComponent) => {
		const stage = camera.stage.getValue();
		if (stage !== undefined) {
			const uniformGroup = stage.getUniformGroup();
			stage.setScene(camera.entity.world.scene);
			uniformGroup.setUpUniformBlock(`
				uniform stage {
					mat4 worldToCamera;
					mat4 cameraToNdc;
					vec2 renderSize;
				};
			`);

			// Set the renderSize uniform whenever the stage bounds changes.
			stage.setBoundsChangedCallback((aStage) => {
				uniformGroup.setUniform('renderSize', aStage.getBounds().size.array);
				this._cameraChanged(camera);
			});

			const localToNdc = this._cameraToNdcTransforms.get(camera.entity)!;
			Transforms.localToNdcPerspective(localToNdc, camera.fov.getValue(), stage.getBounds().size.x / stage.getBounds().size.y,
				camera.near.getValue(), camera.far.getValue(), false);
			uniformGroup.setUniform('cameraToNdc', localToNdc.array);
			const frame = camera.entity.get(FrameComponent)!;
			const worldToCamera = this._worldToCameraTransforms.get(camera.entity)!;
			Transforms.worldToLocal(worldToCamera, frame.position.getValue(), frame.orientation.getValue());
			uniformGroup.setUniform('worldToCamera', worldToCamera.array);
			uniformGroup.setUniform('renderSize', stage.getBounds().size.array);
		}
	};

	private _cameraQuery: Query;

	private _cameraToNdcTransforms = new Map<Entity, Mat44>();

	private _worldToCameraTransforms = new Map<Entity, Mat44>();
}
