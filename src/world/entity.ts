import type { Component, ComponentType, World } from '../internal';

/** The entity. */
export class Entity {

	/** The world. */
	readonly world: World;

	/** The name. */
	readonly name: string;

	/** Constructs the entity. */
	constructor(world: World, name: string) {
		this.world = world;
		this.name = name;
	}

	/** Destroys the entity. */
	zzDestroy() {
		const components = Array.from(this._components.entries());
		for (const [componentType, component] of components) {
			this._components.delete(componentType);
			component.zzDestroy();
			this.world.zzComponentRemoved(this, componentType);
		}
	}

	/** Returns true if the entity has the component. */
	has<Type extends Component>(componentType: ComponentType<Type>): boolean {
		return this._components.has(componentType);
	}

	/** Gets the components. */
	get<Type extends Component>(componentType: ComponentType<Type>): Type | undefined {
		return this._components.get(componentType) as Type;
	}

	/** Adds a new component. */
	add<Type extends Component>(ComponentType: ComponentType<Type>): Type {
		if (this._components.has(ComponentType)) {
			throw new Error(`While adding a component, the component type "${ComponentType.name}" already exists on the entity "${this.name}".`);
		}
		const component = new ComponentType(this);
		this._components.set(ComponentType, component);
		this.world.zzComponentAdded(component);
		return component;
	}

	/** Removes a component. */
	remove<Type extends Component>(componentType: ComponentType<Type>) {
		const component = this._components.get(componentType);
		if (component === undefined) {
			throw new Error(`While removing a component, the component type "${componentType.name}" does not exist on the entity "${this.name}".`);
		}
		this._components.delete(componentType);
		component.zzDestroy();
		this.world.zzComponentRemoved(this, componentType);
	}

	/** The components. */
	private _components = new Map<ComponentType, Component>();
}

// Interface ComponentMap extends Map<ComponentType>, Partial<Component>> {
// 	get<Type extends Component<Type>>(componentType: ComponentType<Type>): Type | undefined;
// }

// import { NamedTypedCollection } from '../utils/named_typed_collection';
// import {
// 	Component,
// 	ComponentType,
// 	World
// } from '../internal';

// /*
// When a component is removed, it shouldn't be removed immediately but at the beginning of the frame.
// This is because System functions should never be called except in their correct phases.
// So there should be a World.added queue and a World.removed queue.

// How will a component add be processed? Should the new component be created and added to the queue, and
// then just before the systems process the component-added event, it will be added to the entity? This
// seems to work well.

// So then only World will actually add and remove components from entities. Systems will call for them to be
// removed, but this just adds them to the right queue, to be processed at the beginning of the next frame.

// /** A entity, which has a bunch of components. */
// export class Entity {

// 	/** Constructs the entity. */
// 	constructor(world: World) {

// 		// Set the world that contains this.
// 		this.world = world;
// 	}

// 	/** Gets the world that contains this. */
// 	getWorld(): World {
// 		return this.world;
// 	}

// 	/** Gets the component of the given name. */
// 	getComponentByName<Type extends Component>(name: string): Type | undefined {
// 		return this.componentsByName.get(name) as Type | undefined;
// 	}

// 	/** Gets the components of the given type. */
// 	getComponentsByType<Type extends Component>(type: typeof Component): ReadonlyArray<Type> | undefined {
// 		return this.componentsByType.get(type) as (ReadonlyArray<Type> | undefined);
// 	}

// 	/** Adds a component. */
// 	addComponent<Type extends Component>(type: ComponentType<Type>, name?: string): Type {

// 		// If no name is given, use the kebab-case of the type.
// 		if (name === undefined) {
// 			name = type.name.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);
// 		}

// 		// Check if the name doesn't already exist.
// 		if (this.componentsByName.has(name)) {
// 			throw new Error(`A component with the name "${name}" already exists.`);
// 		}

// 		// Check if the name doesn't already exist in the pending adds.
// 		for (let i = 0, l = this.pendingAdds.length; i < l; i++) {
// 			if (this.pendingAdds[i].name === name) {
// 				throw new Error(`A ${this.typeAsString} with name "${name}" already exists in the pending additions.`);
// 			}
// 		}

// 		// Add the component.
// 		const component = this.components.add(type, this, name);

// 		// Notify the world that the new component has been added.
// 		this.world.sendComponentWasAddedEvent(this, component);

// 		// Return the component.
// 		return component;
// 	}

// 	/** Removes a component. */
// 	removeComponent(component: Component) {

// 		I NEED TO MAKE THIS CALL SOMETHING IN WORLD
// 		IN WORLD THERE NEEDS TO BE A REMOVE COMPONENTS LOOP
// 		  THAT REMOVES ALL COMPONENTS AT THE BEGINNING OF THE FRAME.
// 		THEN NO SYSTEM WILL HAVE TO WORRY ABOUT THE CHANGED COMPONENTS LISTS
// 		  CONTAINING REMOVED ELEMENTS, SINCE THEY ARE CLEAR AT THE BEGINNING OF EACH FRAME.
// 		THIS ALSO MEANS I POSSIBLY NEED TO STANDARDIZE THE CHANGED ELEMENTS LISTS.

// 		// Notify the world that the component will be removed.
// 		if (this.components.has(component)) {
// 			this.world.sendComponentWillBeRemovedEvent(this, component);
// 		}

// 		// Remove the component.
// 		this.components.remove(component);
// 	}

// 	/** Clears all of the components. */
// 	clearComponents() {

// 		// Notify the world that these components are being removed.
// 		for (const component of this.components.getAllItemsIterator()) {
// 			this.world.sendComponentWillBeRemovedEvent(this, component);
// 		}

// 		// Clear the map.
// 		this.components.clear();
// 	}

// 	/** The world that contains this. */
// 	private world: World;

// 	// /** The component name-type map. */
// 	// private components: NamedTypedCollection<Entity, Component>;

// 	/** The items keyed by name. */
// 	private componentsByName: Map<string, Component> = new Map();

// 	/** The items keyed by type (there may be multiple). */
// 	private componentsByType: Map<typeof Component, Component[]> = new Map();

// }
