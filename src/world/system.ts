import type {
	World
} from '../internal';

/** A type of a system. */
export type SystemType<S extends System = System> = new (world: World) => S;

/** A system, which controls various components. */
export class System {

	/** The world that contains this. */
	readonly world: World;

	/** Constructor. */
	constructor(world: World) {
		this.world = world;
	}

	/** Destroys the system. */
	destroy() {
	}

	/** Updates the system. Subsystems can implement this. Called by World. */
	update() {
	}
}
