import type { ComponentType, Entity, FastSet } from '../internal';

/** A query for gathering various components. */
export interface Query {

	/** The components that are being watched for adds and removes. */
	types: FastSet<ComponentType>;

	/** The entities in the query. */
	entities: FastSet<Entity>;

	/** The function that tests whether an entity should be in the query. */
	test: (entity: Entity) => boolean;

	/** The function that is called when an entity was added to the entities list. */
	added?: (entity: Entity) => void;

	/** The function that is called when an entity was removed form the entities list. */
	removed?: (entity: Entity) => void;
}

export namespace Query {

	/** A default test. Returns true if the entity has every type. */
	export function defaultTest(types: readonly ComponentType[], entity: Entity): boolean {
		for (const type of types) {
			if (!entity.has(type)) {
				return false;
			}
		}
		return true;
	}
}
