import { ObjectWithUniqueId } from '../internal';

/** A mesh. */
export class Mesh extends ObjectWithUniqueId {

	/** Constructs the mesh. */
	constructor(gl: WebGL2RenderingContext) {
		super();

		// Save the WebGL context.
		this._gl = gl;

		// Create the vertex array object.
		this._vertexArrayObject = this._gl.createVertexArray();

		// Setup the vertex buffers array.
		this._vertexBuffers = [];

		// Create the index buffer.
		this._indexBuffer = this._gl.createBuffer();

		// Since there are no indices, set the number of indices to zero.
		this._numIndices = 0;

		// Set the initial indices type to 16-bit.
		this._glIndicesType = this._gl.UNSIGNED_SHORT;

		// Set the initial number of instances to 1.
		this._numInstances = 1;

		// Set the initial mode to triangles.
		this._mode = this._gl.TRIANGLES;
	}

	/** Destroys the mesh. Called by Renderer. */
	zzDestroy() {

		// Check if it's still being used by any models.
		if (this._numModelsUsing > 0) {
			throw new Error('Models are still using this mesh.');
		}

		// Clear out all of the gl objects.
		for (let i = 0; i < this._vertexBuffers.length; i++) {
			this._gl.deleteBuffer(this._vertexBuffers[i]!);
		}
		this._gl.deleteBuffer(this._indexBuffer);
		this._gl.deleteVertexArray(this._vertexArrayObject);

		// Call super.
		super.destroy();
	}

	/** Sets the number of vertices per primitive. Defaults to 'triangles' */
	setPrimitiveType(primitiveType: 'points' | 'lines' | 'triangles') {
		if (primitiveType === 'points') {
			this._mode = this._gl.POINTS;
		}
		else if (primitiveType === 'lines') {
			this._mode = this._gl.LINES;
		}
		else /* 'triangles' */ {
			this._mode = this._gl.TRIANGLES;
		}
	}

	/** Sets the vertex format. All vertices are cleared.
	 *  @param vertexFormat - The vertex format. Each array refers to a separate vertex buffer,
	 *  and each sub-array refers to the interleaved format within that buffer. */
	setVertexFormat(vertexFormat: VertexComponent[][]) {

		// Deletes any existing buffers.
		for (let i = 0; i < this._vertexBuffers.length; i++) {
			this._gl.deleteBuffer(this._vertexBuffers[i]!);
		}
		this._vertexBuffers.splice(0, this._vertexBuffers.length);

		// Bind the vertex array.
		this._gl.bindVertexArray(this._vertexArrayObject);

		// Go through each vertex buffer.
		for (let i = 0, l = vertexFormat.length; i < l; i++) {
			const components = vertexFormat[i]!;

			// Create and bind the vertex buffer.
			const vertexBuffer = this._gl.createBuffer();
			this._vertexBuffers.push(vertexBuffer);
			this._gl.bindBuffer(this._gl.ARRAY_BUFFER, vertexBuffer);

			// Calculate the bytes per vertex.
			let bytesPerVertex = 0;
			const bytesPerComponent: number[] = [];
			for (let j = 0, m = components.length; j < m; j++) {
				const component = components[j]!;

				// Calculate the number of bytes for the component.
				let bytesPerDimension = 1;
				switch (component.type) {
					case 'byte': bytesPerDimension = 1; break;
					case 'ubyte': bytesPerDimension = 1; break;
					case 'short': bytesPerDimension = 2; break;
					case 'ushort': bytesPerDimension = 2; break;
					case 'int': bytesPerDimension = 4; break;
					case 'uint': bytesPerDimension = 4; break;
					case 'float': bytesPerDimension = 4; break;
				}
				const componentBytes = bytesPerDimension * component.dimensions;
				bytesPerComponent.push(componentBytes);
				bytesPerVertex += componentBytes;
			}

			// Setup the vertex array object.
			let offset = 0;
			for (let j = 0, m = components.length; j < m; j++) {
				const component = components[j]!;

				// Enable the attribute location.
				this._gl.enableVertexAttribArray(component.location);

				// Get the WebGL type.
				let glType: number = this._gl.FLOAT;
				switch (component.type) {
					case 'byte': glType = this._gl.BYTE; break;
					case 'ubyte': glType = this._gl.UNSIGNED_BYTE; break;
					case 'short': glType = this._gl.SHORT; break;
					case 'ushort': glType = this._gl.UNSIGNED_SHORT; break;
					case 'int': glType = this._gl.INT; break;
					case 'uint': glType = this._gl.UNSIGNED_INT; break;
					case 'float': glType = this._gl.FLOAT; break;
				}

				// Assign the currently bound vertex buffer to the attribute location with the given format.
				this._gl.vertexAttribPointer(component.location, component.dimensions, glType, false, bytesPerVertex, offset);

				// Set whether this component is instanced or not.
				if (component.instanced === true) {
					this._gl.vertexAttribDivisor(component.location, 1);
				}

				// Increase the offset.
				offset += bytesPerComponent[j]!;
			}
		}
	}

	/** Sets the vertices for a particular buffer.
	 *  @param index - The buffer index to use.
	 *  @param vertices - The actual vertex data. It must be in the same format as specified in the setVertexFormat() call.
	 *  @param dynamic - Specifies if the data will be changed often or not. If you are unsure, set it to false. */
	setVertices(index: number, vertices: Uint8Array, dynamic: boolean) {

		// Make sure the index is valid.
		if (index < 0 || this._vertexBuffers.length <= index) {
			throw new Error('Index out of bounds');
		}

		// Set the glUsage.
		let glUsage: number = this._gl.STATIC_DRAW;
		if (dynamic) {
			glUsage = this._gl.DYNAMIC_DRAW;
		}

		// Bind and upload the data to the buffer.
		this._gl.bindBuffer(this._gl.ARRAY_BUFFER, this._vertexBuffers[index]!);
		this._gl.bufferData(this._gl.ARRAY_BUFFER, vertices, glUsage);
	}

	/** Sets the indices. If use32BitIndices is true, each index will be 32 bits so that they
	 *  can refer to more than 2^16 vertices (false implies 16 bits). */
	setIndices(indices: Uint8Array, use32BitIndices: boolean) {

		// Save the state for use in the zzRender() function.
		this._glIndicesType = use32BitIndices ? this._gl.UNSIGNED_INT : this._gl.UNSIGNED_SHORT;
		this._numIndices = indices.length / (use32BitIndices ? 4 : 2);

		// Bind the buffer and send the data.
		this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
		this._gl.bufferData(this._gl.ELEMENT_ARRAY_BUFFER, indices, this._gl.STATIC_DRAW);
	}

	/** Sets the number of instances. Defaults to 1. */
	setNumInstances(numInstances: number) {
		this._numInstances = numInstances;
	}

	/** Renders the mesh. Called by Model. */
	zzRender() {

		// Bind the vertex array object.
		this._gl.bindVertexArray(this._vertexArrayObject);

		// Bind the index buffer.
		this._gl.bindBuffer(this._gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);

		// Draw the mesh.
		this._gl.drawElementsInstanced(this._mode, this._numIndices, this._glIndicesType, 0, this._numInstances);
	}

	/** Offsets the number of models using this. Called by Model. */
	zzOffsetNumModelsUsing(offset: number) {
		this._numModelsUsing += offset;
	}

	/**  The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The vertex array object. */
	private _vertexArrayObject: WebGLVertexArrayObject;

	/** The vertex buffers. */
	private _vertexBuffers: WebGLBuffer[];

	/** The WebGL index buffer that will be rendered. */
	private _indexBuffer: WebGLBuffer;

	/** The type of primitive to render. It can be points, lines, or triangles. */
	private _mode: GLenum;

	/** The number of indices to render, calculated in `setIndices`. */
	private _numIndices: number;

	/** Whether or not the indices are 32-bits or 16-bits. */
	private _glIndicesType: GLenum;

	/** The number of instances to render. */
	private _numInstances: number;

	/** The number of models using this. */
	private _numModelsUsing: number = 0;
}

/** A component of a Mesh vertex. */
export interface VertexComponent {

	/** The WebGL location of the component. It should be a consecutive number within the vertex buffer. */
	location: number;

	/** The type of component. */
	type: 'byte' | 'ubyte' | 'short' | 'ushort' | 'int' | 'uint' | 'float';

	/** The number of dimensions of the component. */
	dimensions: number;

	/** If true, the component is instanced and will be used on a per instance basis. */
	instanced?: boolean;
}
