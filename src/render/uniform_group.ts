import { FastMap, type FastMapReadOnly, type Texture } from '../internal';

/** A group of uniforms that can be used by a stage, scene, model, or model group. */
export class UniformGroup {

	/** Constructs the uniform group. */
	constructor(gl: WebGL2RenderingContext) {

		// Save the WebGL context.
		this._gl = gl;
	}

	/** Destroys the uniform group. Called by Model, Scene, and Stage. */
	zzDestroy() {

		// Decrement the textures use counts.
		const texturesArray = this._samplerNamesToTextures.getArray();
		for (let i = 0, l = texturesArray.length; i < l; i++) {
			const texture = texturesArray[i]!.value;
			texture.zzOffsetNumUniformGroupsUsing(-1);
		}

		// Delete the buffer.
		if (this._buffer) {
			this._gl.deleteBuffer(this._buffer);
		}
	}

	/** Gets the code used for setting up the uniforms. */
	getCode(): string {
		return this._code;
	}

	/** Gets the textures. */
	getTextures(): FastMapReadOnly<string, Texture | undefined> {
		return this._samplerNamesToTextures;
	}

	/** Sets up the samplers. */
	setUpSamplers(names: string[]) {

		// Clear out the existing names.
		this._samplerNames.clear();

		// Go through each name and add them.
		for (let i = 0, l = names.length; i < l; i++) {
			const name = names[i]!;
			if (this._samplerNames.has(name)) {
				throw new Error(`The uniform ${name} already exists as a sampler2D.`);
			}
			this._samplerNames.add(name);
		}

		const samplerNamesToBeRemoved: string[] = [];
		const samplerNamesToTexturesArray = this._samplerNamesToTextures.getArray();
		for (let i = 0, l = samplerNamesToTexturesArray.length; i < l; i++) {
			const samplerName = samplerNamesToTexturesArray[i]!.key;
			if (!this._samplerNames.has(samplerName)) {
				samplerNamesToBeRemoved.push(samplerName);
			}
		}
		for (let i = 0, l = samplerNamesToBeRemoved.length; i < l; i++) {
			const samplerNameToBeRemoved = samplerNamesToBeRemoved[i]!;
			const texture = this._samplerNamesToTextures.get(samplerNameToBeRemoved)!;
			texture.zzOffsetNumUniformGroupsUsing(-1);
			this._samplerNamesToTextures.remove(samplerNameToBeRemoved);
		}
	}

	/** Sets up the uniform block. It can include any helper structs. */
	setUpUniformBlock(code: string) {

		// Set the code.
		this._code = code;

		// Setup the offsets.
		this._setUpUniformInfos(code);

		// Create or delete the uniform buffer object as needed.
		if (!this._buffer && this._uniformNames.length > 0) {
			// Create the uniform buffer.
			this._buffer = this._gl.createBuffer();
		}
		else if (this._buffer) {
			this._gl.deleteBuffer(this._buffer);
			this._buffer = undefined;
		}

		// Set the values.
		for (const name of this._uniformNames) {
			const value = this._uniformValues.get(name);
			if (value !== undefined) {
				this._updateUniformValue(name, value);
			}
		}
	}

	/** Returns true if *this* has the sampler of the given name. */
	hasSampler(name: string): boolean {
		return this._samplerNames.has(name);
	}

	/** Returns true if *this* has the uniform of the given name. */
	hasUniform(name: string): boolean {
		return this._uniformNames.includes(name);
	}

	/** Sets the texture. */
	setTexture(name: string, value: Texture | undefined) {
		if (!this._samplerNames.has(name)) {
			return;
		}
		const oldValue = this._samplerNamesToTextures.get(name);
		if (oldValue) {
			oldValue.zzOffsetNumUniformGroupsUsing(-1);
		}
		if (value) {
			this._samplerNamesToTextures.set(name, value);
			value.zzOffsetNumUniformGroupsUsing(+1);
		}
		else if (oldValue) {
			this._samplerNamesToTextures.remove(name);
		}
	}

	/** Sets the uniform to a value. */
	setUniform(name: string, value: number | readonly number[]) {
		if (Array.isArray(value)) {
			let uniformValue = this._uniformValues.get(name);
			if (!Array.isArray(uniformValue) || uniformValue.length !== value.length) {
				uniformValue = new Array(value.length);
				this._uniformValues.set(name, uniformValue);
			}
			for (let i = 0, l = value.length; i < l; i++) {
				uniformValue[i] = value[i]!;
			}
		}
		else {
			this._uniformValues.set(name, value as number);
		}
		this._updateUniformValue(name, value);
	}

	/** Sends any changed uniforms to WebGL. Called by Model. */
	zzSendChangedUniforms() {
		if (this._dataNeedsSend && this._buffer !== undefined) {
			// Console.log(`Sending uniforms...`);
			// for (const u of this._uniformValues) {
			// 	console.log(`  ${u[0]} ${u[1]}`);
			// }
			this._gl.bindBuffer(this._gl.UNIFORM_BUFFER, this._buffer);
			this._gl.bufferData(this._gl.UNIFORM_BUFFER, this._data, this._gl.DYNAMIC_READ);
			this._dataNeedsSend = false;
		}
	}

	/** Binds the uniform buffer to a binding point. Sends changed data to WebGL if needed. Called by Model. */
	zzBindUniformBuffer(bindingPoint: number) {
		if (this._buffer !== undefined) {
			this._gl.bindBufferBase(this._gl.UNIFORM_BUFFER, bindingPoint, this._buffer);
		}
	}

	/** Unbinds the uniform buffer from the binding point. Called by Model. */
	zzUnbindUniformBuffer(bindingPoint: number) {
		this._gl.bindBufferBase(this._gl.UNIFORM_BUFFER, bindingPoint, null);
	}

	/** Updates the value of a uniform in the dataView. */
	private _updateUniformValue(name: string, value: boolean | number | readonly boolean[] | readonly number[]) {

		// Get the info for the uniform.
		const uniformBlockInfo = this._uniformInfos.get(name);
		if (uniformBlockInfo === undefined) {
			return;
		}
		const numComponents = uniformBlockInfo.numComponents;
		const componentType = uniformBlockInfo.componentType;
		const offset = uniformBlockInfo.offset;

		// It's a single primitive.
		if (numComponents === 1) {

			// Set the data. Assuming hardware is little-endian. For speed we aren't doing any type checking.
			if (componentType === ComponentType.Float) {
				this._dataView.setFloat32(offset, value as number, true);
			}
			else if (componentType === ComponentType.Int) {
				this._dataView.setInt32(offset, value as number, true);
			}
			else if (componentType === ComponentType.UnsignedInt) {
				this._dataView.setUint32(offset, value as number, true);
			}
			else {
				this._dataView.setUint32(offset, value as boolean ? 1 : 0);
			}
		}

		// A number array such as vec4 or mat4x4.
		else {

			// Make sure the value is a array. We don't check the individual members for efficiency.
			if (!Array.isArray(value)) {
				throw new Error(`The uniform type of ${name} does not match the type of ${value}`);
			}

			// Set the data. Assuming hardware is little-endian. For speed we aren't doing any type checking.
			const minLength = Math.min(value.length, numComponents);
			if (componentType === ComponentType.Float) {
				for (let i = 0, l = minLength; i < l; i++) {
					this._dataView.setFloat32(uniformBlockInfo.offset + i * 4, value[i] as number, true);
				}
			}
			else if (componentType === ComponentType.Int) {
				for (let i = 0, l = minLength; i < l; i++) {
					this._dataView.setInt32(uniformBlockInfo.offset + i * 4, value[i] as number, true);
				}
			}
			else if (componentType === ComponentType.UnsignedInt) {
				for (let i = 0, l = minLength; i < l; i++) {
					this._dataView.setUint32(uniformBlockInfo.offset + i * 4, value[i] as number, true);
				}
			}
			else {
				for (let i = 0, l = minLength; i < l; i++) {
					this._dataView.setUint32(uniformBlockInfo.offset + i * 4, value[i] as boolean ? 1 : 0, true);
				}
			}
		}
		this._dataNeedsSend = true;
	}

	/** Calculates the uniform block offsets by creating a temporary shader and then querying the uniform offsets of that shader. */
	private _setUpUniformInfos(code: string) {

		// Create the vertex shader object.
		const vertexObject = this._gl.createShader(this._gl.VERTEX_SHADER);
		if (vertexObject === null) {
			throw new Error('Could not create a new shader object.');
		}
		this._gl.shaderSource(vertexObject, `#version 300 es\n${code}void main() {}`);

		// Compile the vertex shader objects.
		this._gl.compileShader(vertexObject);
		const success = this._gl.getShaderParameter(vertexObject, this._gl.COMPILE_STATUS) as GLboolean;
		if (!success) {
			const error = this._gl.getShaderInfoLog(vertexObject);
			this._gl.deleteShader(vertexObject);
			throw new Error(`The uniform block's shader object did not compile correctly: ${error}`);
		}

		// Create the fragment shader object.
		const fragmentObject = this._gl.createShader(this._gl.FRAGMENT_SHADER);
		if (fragmentObject === null) {
			this._gl.deleteShader(vertexObject);
			throw new Error('Could not create a new shader object.');
		}
		this._gl.shaderSource(fragmentObject, '#version 300 es\nvoid main() {}');

		// Compile the fragment shader object.
		this._gl.compileShader(fragmentObject);

		// Create the program, attach the shader objects, and link.
		const program = this._gl.createProgram();
		this._gl.attachShader(program, vertexObject);
		this._gl.attachShader(program, fragmentObject);
		this._gl.linkProgram(program);

		// Get the names of the uniforms.
		this._uniformNames = [];
		const indices: number[] = [];
		const numUniforms = this._gl.getProgramParameter(program, this._gl.ACTIVE_UNIFORMS) as number;
		for (let i = 0; i < numUniforms; i++) {
			const info = this._gl.getActiveUniform(program, i);
			this._uniformNames.push(info!.name);
			indices.push(i);
		}

		// Get the size in bytes of the uniform block, and create the data array.
		const dataSizeInBytes = this._gl.getActiveUniformBlockParameter(program, 0, this._gl.UNIFORM_BLOCK_DATA_SIZE) as number;
		this._data = new ArrayBuffer(dataSizeInBytes);
		this._dataView = new DataView(this._data);

		// Get the offsets and types for each of the uniforms in the uniform block.
		const uniformOffsets = this._gl.getActiveUniforms(program, indices, this._gl.UNIFORM_OFFSET) as number[];
		const uniformTypes = this._gl.getActiveUniforms(program, indices, this._gl.UNIFORM_TYPE) as number[];

		// Populate the uniform block infos.
		for (let i = 0; i < numUniforms; i++) {
			const uniformName = this._uniformNames[i]!;
			const uniformOffset = uniformOffsets[i]!;
			const uniformType = uniformTypes[i]!;
			this._uniformInfos.set(uniformName, {
				offset: uniformOffset,
				numComponents: TypeToNumComponents.get(uniformType)!,
				componentType: TypeToComponentType.get(uniformType)!
			});
		}
		this._dataNeedsSend = true;

		// Clean up the shader objects and program.
		this._gl.deleteProgram(program);
		this._gl.deleteShader(vertexObject);
		this._gl.deleteShader(fragmentObject);
	}

	/**  The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The WebGL buffer. It will hold all non-opaque variables. */
	private _buffer: WebGLBuffer | undefined;

	/** The code for setting up the uniforms. */
	private _code: string = '';

	/** The names of the uniforms in the uniform block in the order they appear. */
	private _uniformNames: string[] = [];

	/** The names of the samplers. */
	private _samplerNames = new Set<string>();

	/** A mapping from uniform names to infos. */
	private _uniformInfos = new Map<string, UniformInfo>();

	/** The data. */
	private _data: ArrayBuffer = new ArrayBuffer(0);

	/** A mapping from uniform names to values. */
	private _uniformValues = new Map<string, boolean | number | boolean[] | number[]>();

	/** A mapping from sampler names to textures. */
	private _samplerNamesToTextures = new FastMap<string, Texture>();

	/** A DataView of the data. */
	private _dataView: DataView = new DataView(this._data);

	/** If true, the data needs to be sent to the buffer. */
	private _dataNeedsSend: boolean = true;
}

interface UniformInfo {
	// Type: GLenum;
	offset: number;
	numComponents: number;
	componentType: ComponentType;
}

const TypeToNumComponents = new Map<GLenum, number>([
	[WebGL2RenderingContext.BOOL, 1],
	[WebGL2RenderingContext.INT, 1],
	[WebGL2RenderingContext.UNSIGNED_INT, 1],
	[WebGL2RenderingContext.FLOAT, 1],
	[WebGL2RenderingContext.BOOL_VEC2, 2],
	[WebGL2RenderingContext.INT_VEC2, 2],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC2, 2],
	[WebGL2RenderingContext.FLOAT_VEC2, 2],
	[WebGL2RenderingContext.BOOL_VEC3, 3],
	[WebGL2RenderingContext.INT_VEC3, 3],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC3, 3],
	[WebGL2RenderingContext.FLOAT_VEC3, 3],
	[WebGL2RenderingContext.BOOL_VEC4, 4],
	[WebGL2RenderingContext.INT_VEC4, 4],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC4, 4],
	[WebGL2RenderingContext.FLOAT_VEC4, 4],
	[WebGL2RenderingContext.FLOAT_MAT2, 4],
	[WebGL2RenderingContext.FLOAT_MAT2x3, 6],
	[WebGL2RenderingContext.FLOAT_MAT3x2, 6],
	[WebGL2RenderingContext.FLOAT_MAT2x4, 8],
	[WebGL2RenderingContext.FLOAT_MAT4x2, 8],
	[WebGL2RenderingContext.FLOAT_MAT3, 9],
	[WebGL2RenderingContext.FLOAT_MAT3x4, 12],
	[WebGL2RenderingContext.FLOAT_MAT4x3, 12],
	[WebGL2RenderingContext.FLOAT_MAT4, 16]
]);

enum ComponentType {
	Bool,
	Int,
	UnsignedInt,
	Float
}

const TypeToComponentType = new Map<GLenum, number>([
	[WebGL2RenderingContext.BOOL, ComponentType.Bool],
	[WebGL2RenderingContext.INT, ComponentType.Int],
	[WebGL2RenderingContext.UNSIGNED_INT, ComponentType.UnsignedInt],
	[WebGL2RenderingContext.FLOAT, ComponentType.Float],
	[WebGL2RenderingContext.BOOL_VEC2, ComponentType.Bool],
	[WebGL2RenderingContext.INT_VEC2, ComponentType.Int],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC2, ComponentType.UnsignedInt],
	[WebGL2RenderingContext.FLOAT_VEC2, ComponentType.Float],
	[WebGL2RenderingContext.BOOL_VEC3, ComponentType.Bool],
	[WebGL2RenderingContext.INT_VEC3, ComponentType.Int],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC3, ComponentType.UnsignedInt],
	[WebGL2RenderingContext.FLOAT_VEC3, ComponentType.Float],
	[WebGL2RenderingContext.BOOL_VEC4, ComponentType.Bool],
	[WebGL2RenderingContext.INT_VEC4, ComponentType.Int],
	[WebGL2RenderingContext.UNSIGNED_INT_VEC4, ComponentType.UnsignedInt],
	[WebGL2RenderingContext.FLOAT_VEC4, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT2, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT2x3, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT3x2, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT2x4, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT4x2, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT3, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT3x4, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT4x3, ComponentType.Float],
	[WebGL2RenderingContext.FLOAT_MAT4, ComponentType.Float]
]);
