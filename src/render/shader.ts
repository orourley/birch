import { type Downloader, ObjectWithUniqueId } from '../internal';
import { JsonHelper } from '@orourley/pine-lib';

/** A shader. */
export class Shader extends ObjectWithUniqueId {

	/** The blending mode. */
	blending: Shader.Blending = Shader.Blending.None;

	/** The depth test. */
	depthTest: Shader.DepthTest = Shader.DepthTest.LessOrEqual;

	/** The constructor. */
	constructor(gl: WebGL2RenderingContext, downloader: Downloader) {
		super();

		// Save the WebGL context and downloader.
		this._gl = gl;
		this._downloader = downloader;

		// Create the program.
		this._program = this._gl.createProgram();
	}

	/** Destroys the shader. Called by Renderer. */
	zzDestroy() {

		// Check if it's still being used by any models.
		if (this._numModelsUsing > 0) {
			throw new Error('Models are still using this shader.');
		}

		// Delete the program.
		this._gl.deleteProgram(this._program);

		// Call super.
		super.destroy();
	}

	/** Whether or not the shader is ready (has been compiled and linked). */
	isReady(): boolean {
		return this._ready;
	}

	/** Sets the code and attributes for the shader. */
	setAttributesAndCode(params: { attributeLocations: Record<string, number>; vertex: string; fragment: string; }) {

		// Compile and link the code into the program.
		let vertexObject: WebGLShader | undefined;
		let fragmentObject: WebGLShader | undefined;

		// Compile the shader stages and link the program.
		try {
			vertexObject = this._compile(params.vertex, this._gl.VERTEX_SHADER);
			fragmentObject = this._compile(params.fragment, this._gl.FRAGMENT_SHADER);
			this._link(vertexObject, fragmentObject, params.attributeLocations);
		}

		// The shader objects are no longer needed, as they are linked into the program, so delete them.
		finally {
			if (vertexObject) {
				this._gl.deleteShader(vertexObject);
			}
			if (fragmentObject) {
				this._gl.deleteShader(fragmentObject);
			}
		}

		// Initialize the uniform blocks, uniforms, and attributes.
		this._initializeUniformBlocks();
		this._initializeUniforms();
		this._initializeAttributes();

		// Make the shader as ready.
		this._ready = true;
	}

	/** Loads the shader from a file. */
	async load(url: string): Promise<void> {

		// Download the options from JSON5.
		const options = (await this._downloader.getJson5(url)).content;

		// Validate the JSON options.
		if (!JsonHelper.isObject(options)) {
			throw new Error(`While parsing ${url}: JSON is not an object.`);
		}
		if (!JsonHelper.isObject(options['code'])) {
			throw new Error(`While parsing ${url}: Missing code property.`);
		}
		if (!JsonHelper.isString(options['code']['vertex'])) {
			throw new Error(`While parsing ${url}: Missing code.vertex property.`);
		}
		if (!JsonHelper.isString(options['code']['fragment'])) {
			throw new Error(`While parsing ${url}: Missing code.fragment property.`);
		}
		if (!JsonHelper.isObjectOfNumbers(options['attributeLocations'])) {
			throw new Error(`While parsing ${url}: Missing attributeLocations property.`);
		}
		if (!JsonHelper.isObject(options['properties'])) {
			throw new Error(`While parsing ${url}: Missing properties property.`);
		}
		if (!JsonHelper.isString(options['properties']['blending'])) {
			throw new Error(`While parsing ${url}: Missing properties.blending.`);
		}
		if (!Shader.BlendingOptions.has(options['properties']['blending'])) {
			throw new Error(`While parsing ${url}: The value of properties.blending is not a valid value. Valid values are:
				${Array.from(Shader.BlendingOptions.keys()).map((v) => `"${v}"`).join(', ')}`);
		}
		if (!JsonHelper.isString(options['properties']['depthTest'])) {
			throw new Error(`While parsing ${url}: Missing properties.depthTest.`);
		}
		if (!Shader.DepthTestOptions.has(options['properties']['depthTest'])) {
			throw new Error(`While parsing ${url}: The value of properties.depthTest is not a valid value. Valid values are
				${Array.from(Shader.DepthTestOptions.keys()).map((v) => `"${v}"`).join(', ')}`);
		}

		// Set the render options.
		this.blending = Shader.BlendingOptions.get(options['properties']['blending'])!;
		this.depthTest = Shader.DepthTestOptions.get(options['properties']['depthTest'])!;

		// Setup the shader from the options.
		this.setAttributesAndCode({
			attributeLocations: options['attributeLocations'],
			vertex: options['code']['vertex'],
			fragment: options['code']['fragment']
		});
	}

	/** Gets the uniform location from its name, or undefined if not found. */
	getUniformLocation(name: string): WebGLUniformLocation | undefined {
		return this._uniformNamesToLocations.get(name);
	}

	/** Gets the attribute location from its name, or undefined if not found. */
	getAttributeLocation(name: string): number | undefined {
		return this._attributeNamesToLocations.get(name);
	}

	/** Gets the binding index for the uniform block name, or undefined if not found. */
	getUniformBlockBindingPoint(uniformBlockName: string): number | undefined {
		return this._uniformBlockNamesToBindingPoints.get(uniformBlockName);
	}

	/** Gets the binding index for the sampler name. */
	getSamplerTextureUnit(samplerName: string): number | undefined {
		return this._samplerNamesToTextureUnits.get(samplerName);
	}

	/** Offset the number of models using this. Called by Model. */
	zzOffsetNumModelsUsing(offset: number) {
		this._numModelsUsing += offset;
	}

	/** Activates the shader for use in rendering. Called by Model. */
	zzActivate() {
		this._gl.useProgram(this._program);
		if (this.blending === Shader.Blending.Add) {
			this._gl.blendFunc(this._gl.SRC_ALPHA, this._gl.ONE);
		}
		else if (this.blending === Shader.Blending.Mix) {
			this._gl.blendFunc(this._gl.SRC_ALPHA, this._gl.ONE_MINUS_SRC_ALPHA);
		}
		else /* Shader.Blending.None */ {
			this._gl.blendFunc(this._gl.ONE, this._gl.ZERO);
		}
	}

	/** Compiles some shader code to create a shader object. */
	private _compile(shaderCode: string, shaderType: number): WebGLShader {

		// Create the shader object.
		const shaderObject = this._gl.createShader(shaderType);
		if (shaderObject === null) {
			throw new Error('Could not create a new shader object.');
		}

		// Supply the source.
		this._gl.shaderSource(shaderObject, shaderCode);

		// Compile the shader object.
		this._gl.compileShader(shaderObject);

		// Check for success.
		const success = this._gl.getShaderParameter(shaderObject, this._gl.COMPILE_STATUS) as GLboolean;
		if (!success) {
			const error = this._gl.getShaderInfoLog(shaderObject);
			this._gl.deleteShader(shaderObject);
			throw new Error(`The shader object did not compile correctly: ${error}`);
		}
		return shaderObject;
	}

	/** Links shader objects to create a shader program. */
	private _link(vertexObject: WebGLShader, fragmentObject: WebGLShader, attributeLocations: Record<string, number>) {

		// Attach the shader objects.
		this._gl.attachShader(this._program, vertexObject);
		this._gl.attachShader(this._program, fragmentObject);

		// Bind the given attrbute locations.
		for (const [key, value] of Object.entries(attributeLocations)) {
			this._gl.bindAttribLocation(this._program, value, key);
		}

		// Link the shaders to the program.
		this._gl.linkProgram(this._program);

		// Detach the shader objects.
		this._gl.detachShader(this._program, vertexObject);
		this._gl.detachShader(this._program, fragmentObject);

		// Check for success.
		const success = this._gl.getProgramParameter(this._program, this._gl.LINK_STATUS) as GLboolean;
		if (!success) {
			const error = this._gl.getProgramInfoLog(this._program);
			throw new Error(`The shader program did not link correctly: ${error}`);
		}
	}

	/** Gets the mapping from uniform blocks to indices. */
	private _initializeUniformBlocks() {

		// Clear out the existing mappings.
		this._uniformBlockNamesToBindingPoints.clear();

		// Get the number of uniform blocks.
		let nextOpenBindingPoint = 3;
		const numUniformBlocks = this._gl.getProgramParameter(this._program, this._gl.ACTIVE_UNIFORM_BLOCKS);

		// Go through each uniform block.
		for (let uniformBlockIndex = 0; uniformBlockIndex < numUniformBlocks; uniformBlockIndex++) {

			// Get the name.
			const uniformBlockName = this._gl.getActiveUniformBlockName(this._program, uniformBlockIndex);
			if (uniformBlockName === null) {
				continue;
			}

			// The uniform blocks stage, scene, and model always have constant binding indices.
			if (uniformBlockName === 'stage') {
				this._gl.uniformBlockBinding(this._program, uniformBlockIndex, 0);
			}
			else if (uniformBlockName === 'scene') {
				this._gl.uniformBlockBinding(this._program, uniformBlockIndex, 1);
			}
			else if (uniformBlockName === 'model') {
				this._gl.uniformBlockBinding(this._program, uniformBlockIndex, 2);
			}

			// Otherwise use the next available binding points.
			else {
				this._gl.uniformBlockBinding(this._program, uniformBlockIndex, nextOpenBindingPoint);
				this._uniformBlockNamesToBindingPoints.set(uniformBlockName, nextOpenBindingPoint);
				nextOpenBindingPoint += 1;
			}
		}
	}

	/** Gets the mapping from uniform names to locations. */
	private _initializeUniforms() {

		// Clear out the existing mappings.
		this._uniformNamesToLocations.clear();
		this._samplerNamesToTextureUnits.clear();
		this._uniformLocationsToValues.clear();

		// Get the number of uniforms and max texture units. We know they return numbers.
		this._gl.useProgram(this._program);
		const numUniforms = this._gl.getProgramParameter(this._program, this._gl.ACTIVE_UNIFORMS) as number;
		const maxTextureUnits = this._gl.getParameter(this._gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS) as number;

		// Go through each uniform.
		let nextTextureUnit = 0;
		for (let i = 0; i < numUniforms; i++) {

			// Get the info and location of the uniform.
			const uniformInfo = this._gl.getActiveUniform(this._program, i);
			if (uniformInfo === null) {
				throw new Error(`Error getting the uniform info ${i} for the shader.`);
			}
			const location = this._gl.getUniformLocation(this._program, uniformInfo.name);
			if (!location) {
				continue;
			}

			// Save the uniform name to location mapping.
			this._uniformNamesToLocations.set(uniformInfo.name, location);

			// Set the initial value to 0, or an empty array if it's an array value.
			let initialValue: number | number[] = 0;
			const primitiveValues: number[] = [this._gl.BOOL, this._gl.INT, this._gl.FLOAT, this._gl.SAMPLER_2D, this._gl.SAMPLER_CUBE];
			if (!primitiveValues.includes(uniformInfo.type)) {
				initialValue = [];
			}

			// If it's a texture, set up the texture unit.
			if (uniformInfo.type === this._gl.SAMPLER_2D) {
				if (nextTextureUnit >= maxTextureUnits) {
					throw new Error(`At least ${nextTextureUnit} textures, but the maximum supported is ${maxTextureUnits}.`);
				}
				this._gl.uniform1i(location, nextTextureUnit);
				this._samplerNamesToTextureUnits.set(uniformInfo.name, nextTextureUnit);
				nextTextureUnit += 1;
			}

			// Save the uniform location to value mapping.
			this._uniformLocationsToValues.set(location, initialValue);
		}
	}

	/** Gets the mapping from attribute names to locations. */
	private _initializeAttributes() {

		// Clear out the existing mappings.
		this._attributeNamesToLocations.clear();

		// Get the number of attributes.
		const numAttributes = this._gl.getProgramParameter(this._program, this._gl.ACTIVE_ATTRIBUTES);

		// Go through each attribute.
		for (let i = 0; i < numAttributes; i++) {

			// Get the name and location.
			const activeAttrib = this._gl.getActiveAttrib(this._program, i);
			if (activeAttrib === null) {
				throw new Error(`Error getting the attribute ${i} on the shader.`);
			}
			const location = this._gl.getAttribLocation(this._program, activeAttrib.name);
			if (location === -1) {
				return;
			}

			// Save the attribute name to location mapping.
			this._attributeNamesToLocations.set(activeAttrib.name, location);
		}
	}

	/** The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The downloader. */
	private _downloader: Downloader;

	/** A mapping from uniform block names to binding indices. */
	private _uniformBlockNamesToBindingPoints = new Map<string, number>();

	/** A mapping from sampler names to binding indices. */
	private _samplerNamesToTextureUnits = new Map<string, number>();

	/** A mapping from uniform names to locations. */
	private _uniformNamesToLocations = new Map<string, WebGLUniformLocation>();

	/** A mapping from uniform locations to values. */
	private _uniformLocationsToValues = new Map<WebGLUniformLocation, number | number[]>();

	/** A mapping from attribute names to locations. */
	private _attributeNamesToLocations = new Map<string, number>();

	/** The Gl shader program. */
	private _program: WebGLProgram;

	/** Whether or not the shader is ready (has been compiled and linked). */
	private _ready: boolean = false;

	/** The number of models using this. */
	private _numModelsUsing: number = 0;
}

export namespace Shader {

	/** The blending modes. */
	export enum Blending { None, Mix, Add }

	/** The blending modes as options. */
	export const BlendingOptions = new Map([
		['none', Blending.None],
		['mix', Blending.Mix],
		['add', Blending.Add]
	]);

	/** The depth test modes. */
	export enum DepthTest { Never, Always, Less, Greater, Equal, NotEqual, LessOrEqual, GreaterOrEqual }

	/** The depth test modes as options. */
	export const DepthTestOptions = new Map([
		['never', DepthTest.Never],
		['always', DepthTest.Always],
		['less', DepthTest.Less],
		['greater', DepthTest.Greater],
		['equal', DepthTest.Equal],
		['not-equal', DepthTest.NotEqual],
		['less-or-equal', DepthTest.LessOrEqual],
		['greater-or-equal', DepthTest.GreaterOrEqual]
	]);
}
