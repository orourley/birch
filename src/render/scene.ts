import { Shader, Model, UniformGroup, Items } from '../internal';

export class Scene {

	/** The models, sorted. */
	readonly models: Items<Model>;

	/** Constructs the scene. */
	constructor(gl: WebGL2RenderingContext) {

		// Create the scene uniform group.
		this._uniformGroup = new UniformGroup(gl);

		// Create the model cache.
		this.models = new Items(() => new Model(this, gl), (model) => { model.zzDestroy(); });
		this.models.setOrdering('sorted', Scene._isModelLess);
	}

	/** Destroys the scene. Called by Renderer. */
	zzDestroy() {

		// Check if it's still being used by any stages.
		if (this._numStagesUsing > 0) {
			throw new Error('Stages are still using this scene.');
		}

		// Destroy the models.
		this.models.zzDestroy();

		// Destroy the scene uniform group.
		this._uniformGroup.zzDestroy();
	}

	/** Gets the uniform group associated with this scene. */
	getUniformGroup(): UniformGroup {
		return this._uniformGroup;
	}

	/** Renders the scene. Called by Stage. */
	zzRender(stageUniforms: UniformGroup) {

		// Sort the models to be optimal in terms of state changes and blending.
		this.models.resort();

		// Render each model.
		const modelsArray = this.models.getArray();
		for (let i = 0, l = modelsArray.length; i < l; i++) {
			modelsArray[i]!.zzRender(stageUniforms, this._uniformGroup);
		}
	}

	/** Offset the number of stages using this. Called by Stage. */
	zzOffsetNumStagesUsing(offset: number) {
		this._numStagesUsing += offset;
	}

	/** Compares two models for sorting into the optimal order to reduce WebGL calls. */
	private static _isModelLess(a: Model, b: Model): boolean {
		const aShader = a.getShader();
		const bShader = b.getShader();
		const aMesh = a.getMesh();
		const bMesh = b.getMesh();
		if (!aShader || !aMesh) {
			return true;
		}
		if (!bShader || !bMesh) {
			return false;
		}
		if (aShader.blending === Shader.Blending.None && bShader.blending !== Shader.Blending.None) {
			return true;
		}
		if (aShader.blending !== Shader.Blending.None && bShader.blending === Shader.Blending.None) {
			return false;
		}
		if (aShader.blending === Shader.Blending.None) { // Sort by shader id and mesh id.
			if (aShader !== bShader) {
				return aShader.id < bShader.id;
			}
			return aMesh.id < bMesh.id;
		}
		else if (a.depth < b.depth) { // Since there is some blending, sort by depth.
			return true;
		}
		else if (a.depth > b.depth) {
			return false;
		}
		else { // Just do the id to guarantee an explicit ordering.
			return a.id < b.id;
		}
	}

	/** The scene-specific uniform block. */
	private _uniformGroup: UniformGroup;

	/** The number of stages using this. */
	private _numStagesUsing: number = 0;
}
