import { type Engine, Mesh, Scene, Shader, Stage, Texture, Items } from '../internal';

/** The renderer. */
export class Renderer {

	/** The scenes. */
	readonly scenes: Items<Scene>;

	/** The stages. They are ordered. */
	readonly stages: Items<Stage>;

	/** The textures. */
	readonly textures: Items<Texture>;

	/** The shaders. */
	readonly shaders: Items<Shader>;

	/** The meshes. */
	readonly meshes: Items<Mesh>;

	/** Constructs the render system. */
	constructor(canvas: HTMLCanvasElement, antialias: boolean, engine: Engine) {

		// Save the engine and canvas.
		this._canvas = canvas;

		// Create the WebGL context.
		const gl = this._canvas.getContext('webgl2', { antialias, alpha: false });
		if (gl === null) {
			throw new Error('Could not get a WebGL 2.0 context. Your browser may not support WebGL 2.0.');
		}
		this._gl = gl;

		// Setup some defaults.
		this._gl.enable(this._gl.BLEND);
		this._gl.enable(this._gl.DEPTH_TEST);
		this._gl.depthFunc(this._gl.GREATER);

		// Setup the resources.
		this.textures = new Items(() => new Texture(this._gl), (texture) => { texture.zzDestroy(); });
		this.shaders = new Items(() => new Shader(this._gl, engine.downloader), (shader) => { shader.zzDestroy(); });
		this.meshes = new Items(() => new Mesh(this._gl), (mesh) => { mesh.zzDestroy(); });

		// Setup the scenes and stages.
		this.scenes = new Items(() => new Scene(this._gl), (scene: Scene) => { scene.zzDestroy(); });
		this.stages = new Items(() => new Stage(this._gl), (stage: Stage) => { stage.zzDestroy(); });
		this.stages.setOrdering('ordered');
	}

	/** Destroys this. Called by Engine. */
	zzDestroy() {

		// Destroy the scenes and stages.
		this.scenes.zzDestroy();
		this.stages.zzDestroy();

		// Destroy the resources.
		this.textures.zzDestroy();
		this.shaders.zzDestroy();
		this.meshes.zzDestroy();

		// Destroy the WebGL context.
		const loseContextExtension = this._gl.getExtension('WEBGL_lose_context');
		if (loseContextExtension !== null) {
			loseContextExtension.loseContext();
		}
	}

	/** Gets the WebGL context. Called by resource objects. */
	zzGetGL(): WebGL2RenderingContext {
		return this._gl;
	}

	/** Renders the stages. Called by Engine. */
	zzRender() {

		// Render the stages in order.
		if (this.stages.getArray().length > 0) {
			const stagesArray = this.stages.getArray();
			for (let i = 0, l = stagesArray.length; i < l; i++) {
				stagesArray[i]!.zzRender(this._canvas.height);
			}
		}
		// If there are no stages, just clear the screen.
		else {
			this._gl.clearColor(0, 0, 0, 1);
			this._gl.clear(this._gl.COLOR_BUFFER_BIT);
		}
	}

	/** The canvas element. */
	private _canvas: HTMLCanvasElement;

	/** The WebGL context. */
	private _gl: WebGL2RenderingContext;
}
