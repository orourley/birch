import { Vec2D, type Vec2DReadOnly } from '../internal';

export class Texture {

	/** Constructs the texture. */
	constructor(gl: WebGL2RenderingContext) {

		// Save the WebGL context.
		this._gl = gl;

		// Create the texture.
		this._handle = this._gl.createTexture();
	}

	/** Destroys the texture. Called by Renderer. */
	zzDestroy() {

		// Check if it's still being used by any uniform groups.
		if (this._numUniformGroupsUsing > 0) {
			throw new Error('Uniform groups are still using this texture.');
		}

		// Check if it's still being used by any stages.
		if (this._numStagesUsing > 0) {
			throw new Error('Stages are still using this texture.');
		}

		// Delete the texture.
		this._gl.deleteTexture(this._handle);

		// Flag that the texture is destroyed.
		this._destroyed = true;
	}

	/** Gets the format. */
	getFormat(): Texture.Format {
		return this._format;
	}

	/** Gets the size in pixels. */
	getSize(): Vec2DReadOnly {
		return this._size;
	}

	/** Loads the source of the texture. */
	async loadSource(source: undefined | ArrayBuffer | Blob | TexImageSource | Uint8Array | Uint16Array | Uint32Array,
		width?: number, height?: number, format?: Texture.Format): Promise<void> {

		// Blob or ArrayBuffer
		// Depending on the type of source, set the formats, size, and apply the source.
		if (source instanceof Blob || source instanceof ArrayBuffer) {

			// Make sure we have a format.
			if (format === undefined) {
				throw new Error('A format must be specified when creating a texture with a blob.');
			}

			// Create the image bitmap from the source.
			const imageBitmap = await createImageBitmap(source instanceof Blob ? source : new Blob([source]), {
				premultiplyAlpha: 'none',
				colorSpaceConversion: 'none'
			});
			if (this._destroyed) {
				return;
			}

			// Set the format and size.
			this._format = format;
			this._size.set(imageBitmap.width, imageBitmap.height);

			// Set the texture.
			this._setGLTexture(imageBitmap);
		}

		// Unit8Array
		else if (source instanceof Uint8Array) {

			// Make sure we have a width and height.
			if (width === undefined || height === undefined) {
				throw new Error('A width and height must be specified when creating a texture with a Uint8Array.');
			}

			// Set the format and size. Default to a UINT8 since it is a Uint8Array.
			if (format === Texture.Format.RGB || format === Texture.Format.RGBA) {
				this._format = format;
			}
			else if (format === undefined) {
				this._format = Texture.Format.UINT8;
			}
			else {
				throw new Error('An invalid format was specified.');
			}
			this._size.set(width, height);

			// Set the texture.
			this._setGLTexture(source);
		}

		// Unit16Array
		else if (source instanceof Uint16Array) {

			// Make sure we have a width and height.
			if (width === undefined || height === undefined) {
				throw new Error('A width and height must be specified when creating a texture with a Uint16Array.');
			}

			// Set the format and size. Use a UINT16 since it is a Uint16Array.
			this._format = Texture.Format.UINT16;
			this._size.set(width, height);

			// Set the texture.
			this._setGLTexture(source);
		}

		// Unit32Array
		else if (source instanceof Uint32Array) {

			// Make sure we have a width and height.
			if (width === undefined || height === undefined) {
				throw new Error('A width and height must be specified when creating a texture with a Uint32Array.');
			}

			// Set the format and size. Use a UINT32 since it is a Uint32Array.
			this._format = Texture.Format.UINT32;
			this._size.set(width, height);

			// Set the texture.
			this._setGLTexture(source);
		}

		// No source
		else if (source === undefined) {

			// Make sure we have a width, height, and format.
			if (width === undefined || height === undefined || format === undefined) {
				throw new Error('A width, height, and format must be specified when creating a blank texture.');
			}

			// Set the format and size.
			this._format = format;
			this._size.set(width, height);

			// Set the texture. In this case, we are just clearing the texture from WebGL.
			this._setGLTexture(source);
		}

		// Canvas or image or video
		else {

			// Set the format and size.
			this._format = Texture.Format.RGBA;
			if ('width' in source) {
				this._size.set(source.width, source.height);
			}
			else {
				this._size.set(source.displayWidth, source.displayHeight);
			}

			// Set the texture.
			this._setGLTexture(source);
		}
	}

	/** Sets the wrapping mode of the texture's UV coordinates. */
	wrapModes(wrapU: 'repeat' | 'clamp' | 'mirror-repeat', wrapV: 'repeat' | 'clamp' | 'mirror-repeat') {

		// Bind the texture.
		this._gl.bindTexture(this._gl.TEXTURE_2D, this._handle);

		// Set the wrapping mode.
		if (wrapU === 'repeat') {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_S, this._gl.REPEAT);
		}
		else if (wrapU === 'clamp') {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_S, this._gl.CLAMP_TO_EDGE);
		}
		else /* 'mirror-repeat' */ {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_S, this._gl.MIRRORED_REPEAT);
		}
		if (wrapV === 'repeat') {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_T, this._gl.REPEAT);
		}
		else if (wrapV === 'clamp') {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_T, this._gl.CLAMP_TO_EDGE);
		}
		else /* 'mirror-repeat' */ {
			this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_T, this._gl.MIRRORED_REPEAT);
		}
	}

	/** Binds the texture to the given bindingIndex. */
	bind(bindingIndex: number) {
		this._gl.activeTexture(this._gl.TEXTURE0 + bindingIndex);
		this._gl.bindTexture(this._gl.TEXTURE_2D, this._handle);
	}

	/** Unbinds any texture at the given bindingIndex. */
	unbind(bindingIndex: number) {
		this._gl.activeTexture(this._gl.TEXTURE0 + bindingIndex);
		this._gl.bindTexture(this._gl.TEXTURE_2D, null);
	}

	/** Gets a string describing the texture. */
	toString(): string {
		return `${Texture.Format[this._format]} ${this._size}`;
	}

	/** Offsets the number of uniform groups using this. Called by UniformGroup. */
	zzOffsetNumUniformGroupsUsing(offset: number) {
		this._numUniformGroupsUsing += offset;
	}

	/** Offsets the number of stages using this. Called by Stage. */
	zzOffsetNumStagesUsing(offset: number) {
		this._numStagesUsing += offset;
	}

	/** Creates a WebGL texture given a source. */
	private _setGLTexture(source: TexImageSource | Uint8Array | Uint16Array | Uint32Array | undefined) {

		// Get the internal format (the format on the GPU), the format (what will be passed to WebGL), and the type (the size of each component).
		const glInternalFormat = this._formatToGLInternalFormat(this._format);
		const glFormat = this._formatToGLFormat(this._format);
		const glType = this._formatToGLType(this._format);

		// Bind the handle and load in the texture.
		this._gl.bindTexture(this._gl.TEXTURE_2D, this._handle);
		if (source instanceof Uint8Array || source instanceof Uint16Array || source instanceof Uint32Array || source === undefined) {
			this._gl.texImage2D(this._gl.TEXTURE_2D, 0, glInternalFormat, this._size.x, this._size.y, 0, glFormat, glType, source ?? null);
		}
		else {
			this._gl.texImage2D(this._gl.TEXTURE_2D, 0, glInternalFormat, this._size.x, this._size.y, 0, glFormat, glType, source);
		}

		// Set some of the texture options.
		this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_S, this._gl.CLAMP_TO_EDGE);
		this._gl.texParameteri(this._gl.TEXTURE_2D, this._gl.TEXTURE_WRAP_T, this._gl.CLAMP_TO_EDGE);
		this._gl.generateMipmap(this._gl.TEXTURE_2D);
	}

	/** Converts a Birch format to a WebGL internal format (the format on the GPU). */
	private _formatToGLInternalFormat(format: Texture.Format): number {
		switch (format) {
			case Texture.Format.RGB: return this._gl.RGB;
			case Texture.Format.RGBA: return this._gl.RGBA;
			case Texture.Format.UINT8: return this._gl.R8UI;
			case Texture.Format.UINT16: return this._gl.R16UI;
			case Texture.Format.UINT32: return this._gl.R32UI;
			case Texture.Format.DEPTH_STENCIL: return this._gl.DEPTH24_STENCIL8;
			default: return NaN; // Will never get here.
		}
	}

	/** Converts a Birch format to a WebGL texture format that will be passed to WebGL. */
	private _formatToGLFormat(format: Texture.Format): number {
		switch (format) {
			case Texture.Format.RGB: return this._gl.RGB;
			case Texture.Format.RGBA: return this._gl.RGBA;
			case Texture.Format.UINT8:
			case Texture.Format.UINT16:
			case Texture.Format.UINT32: return this._gl.RED;
			case Texture.Format.DEPTH_STENCIL: return this._gl.DEPTH_STENCIL;
			default: return NaN; // Will never get here.
		}
	}

	private _formatToGLType(format: Texture.Format): number {
		switch (format) {
			case Texture.Format.RGB:
			case Texture.Format.RGBA:
			case Texture.Format.UINT8: return this._gl.UNSIGNED_BYTE;
			case Texture.Format.UINT16: return this._gl.UNSIGNED_SHORT;
			case Texture.Format.UINT32:
			case Texture.Format.DEPTH_STENCIL: return this._gl.UNSIGNED_INT;
			default: return NaN; // Will never get here.
		}
	}

	/**  The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The WebGL handle. */
	private _handle: WebGLTexture;

	/** The size in pixels. */
	private _size: Vec2D = new Vec2D();

	/** The format. */
	private _format: Texture.Format = Texture.Format.RGB;

	/** True if the object has been destroyed. Needed because of async loading. */
	private _destroyed: boolean = false;

	/** The number of uniform groups using this. */
	private _numUniformGroupsUsing: number = 0;

	/** The number of stages using this. */
	private _numStagesUsing: number = 0;
}

export namespace Texture {

	/** The possible formats of a texture. */
	export enum Format { RGB, RGBA, UINT8, UINT16, UINT32, DEPTH_STENCIL }
}
