import { type Mesh, ObjectWithUniqueId, type Scene, type Shader, type Texture, UniformGroup } from '../internal';

/** A model that contains a mesh, shader, and model-uniform group.
 *  It has a unique id so that it can be sorted in the render queue. */
export class Model extends ObjectWithUniqueId {

	/** The depth used for sorting. */
	depth: number = 0;

	/** Constructs the model. */
	constructor(scene: Scene, gl: WebGL2RenderingContext) {
		super();

		// Save the scene and WebGL context.
		this._scene = scene;
		this._gl = gl;

		// Create the model uniform group.
		this._uniformGroup = new UniformGroup(this._gl);

		// If the render state object hasn't been created, create it.
		let state = Model._state.get(this._gl);
		if (state === undefined) {
			state = new RenderState();
			Model._state.set(this._gl, state);
		}
		state.useCount += 1;
	}

	/** Destroys the model. Called by Scene. */
	zzDestroy() {

		// Decrement the mesh and shader use counts.
		if (this._mesh !== undefined) {
			this._mesh.zzOffsetNumModelsUsing(-1);
		}
		if (this._shader !== undefined) {
			this._shader.zzOffsetNumModelsUsing(-1);
		}

		// Remove the render state if this is the last model using it.
		const state = Model._state.get(this._gl)!;
		state.useCount -= 1;
		if (state.useCount === 0) {
			Model._state.delete(this._gl);
		}

		// Destroy the uniform group.
		this._uniformGroup.zzDestroy();

		// Call super.
		super.destroy();
	}

	/** Gets the scene. */
	getScene(): Scene {
		return this._scene;
	}

	/** Gets the mesh. */
	getMesh(): Mesh | undefined {
		return this._mesh;
	}

	/** Sets the mesh. */
	setMesh(mesh: Mesh | undefined) {
		if (this._mesh !== undefined) {
			this._mesh.zzOffsetNumModelsUsing(-1);
		}
		this._mesh = mesh;
		if (this._mesh !== undefined) {
			this._mesh.zzOffsetNumModelsUsing(+1);
		}
	}

	/** Gets the shader. */
	getShader(): Shader | undefined {
		return this._shader;
	}

	/** Sets the shader. */
	setShader(shader: Shader | undefined) {
		if (this._shader !== undefined) {
			this._shader.zzOffsetNumModelsUsing(-1);
		}
		this._shader = shader;
		if (this._shader !== undefined) {
			this._shader.zzOffsetNumModelsUsing(+1);
		}
	}

	/** Gets the uniform group associated with this model. */
	getUniformGroup(): UniformGroup {
		return this._uniformGroup;
	}

	/** Renders the model. Called by Scene. */
	zzRender(stageUniforms: UniformGroup, sceneUniforms: UniformGroup) {
		if (!this._shader || !this._shader.isReady() || !this._mesh) {
			return;
		}
		// Get the state for this WebGL context. Used by other functions to keep track of things.
		const state = Model._state.get(this._gl)!;
		// Initialize the texture units used this frame to 0.
		for (let i = 0; i < state.textureUnitsUsedThisFrame.length; i++) {
			state.textureUnitsUsedThisFrame[i] = false;
		}
		// Bind the stage uniform buffer to 0 if the stage has changed.
		if (stageUniforms !== state.activeStageUniforms) {
			stageUniforms.zzBindUniformBuffer(0);
		}
		stageUniforms.zzSendChangedUniforms();
		// Bind the scene uniform buffer to 1 if the scene has changed.
		if (sceneUniforms !== state.activeSceneUniforms) {
			sceneUniforms.zzBindUniformBuffer(1);
		}
		sceneUniforms.zzSendChangedUniforms();
		if (stageUniforms !== state.activeStageUniforms || this._shader !== state.activeShader) {
			this._bindTextures(stageUniforms, this._shader, state);
		}
		if (sceneUniforms !== state.activeSceneUniforms || this._shader !== state.activeShader) {
			this._bindTextures(sceneUniforms, this._shader, state);
		}
		// Bind the textures if the shader or stage changed.
		state.activeStageUniforms = stageUniforms;
		state.activeSceneUniforms = sceneUniforms;
		// Bind the model uniform buffer to 2.
		this._uniformGroup.zzBindUniformBuffer(2);
		this._uniformGroup.zzSendChangedUniforms();
		this._bindTextures(this._uniformGroup, this._shader, state);
		// Unbind the unused uniform buffers and texture slots.
		this._unbindUnusedUniformBuffers(state);
		this._unbindUnusedTextures(state);
		// Activate the shader if it changed.
		if (state.activeShader !== this._shader) {
			state.activeShader = this._shader;
			this._shader.zzActivate();
		}
		// Render the mesh.
		this._mesh.zzRender();
	}

	/** Binds the textures of uniform group and shaders. */
	private _bindTextures(uniformGroup: UniformGroup, shader: Shader, state: RenderState) {
		const texturesArray = uniformGroup.getTextures().getArray();
		for (let i = 0, l = texturesArray.length; i < l; i++) {
			const entry = texturesArray[i]!;
			const texture = entry.value;
			if (texture !== undefined) {
				const textureUnit = shader.getSamplerTextureUnit(entry.key);
				if (textureUnit !== undefined) {
					texture.bind(textureUnit);
					state.activeTextures[textureUnit] = texture;
					state.textureUnitsUsedThisFrame[textureUnit] = true;
				}
			}
		}
	}

	/** Unbinds any unused textures. */
	private _unbindUnusedTextures(state: RenderState) {
		for (let i = 0; i < state.activeTextures.length; i++) {
			const activeTexture = state.activeTextures[i];
			if (activeTexture !== undefined && state.textureUnitsUsedThisFrame[i] === false) {
				activeTexture.unbind(i);
				state.activeTextures[i] = undefined;
			}
		}
	}

	/** Unbinds any unused uniform buffers. */
	private _unbindUnusedUniformBuffers(state: RenderState) {
		// Start at 3 since 0, 1, and 2 are reserved.
		for (let i = 3; i < state.activeUniformBuffers.length; i++) {
			const activeUniformBuffer = state.activeUniformBuffers[i];
			if (activeUniformBuffer !== undefined && state.uniformBufferBindingPointsUsedThisFrame[i] === false) {
				activeUniformBuffer.zzUnbindUniformBuffer(i);
				state.activeUniformBuffers[i] = undefined;
			}
		}
	}

	/** The scene. */
	private _scene: Scene;

	/**  The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The scene-specific uniform block. */
	private _uniformGroup: UniformGroup;

	/** The mesh. */
	private _mesh: Mesh | undefined = undefined;

	/** The shader. */
	private _shader: Shader | undefined = undefined;

	/** The WebGL state, one for each WebGL context. */
	private static readonly _state = new Map<WebGL2RenderingContext, RenderState>();
}

export class RenderState {

	/** The active stage. */
	activeStageUniforms: UniformGroup | undefined = undefined;

	/** The active scene. */
	activeSceneUniforms: UniformGroup | undefined = undefined;

	/** The active shader. */
	activeShader: Shader | undefined = undefined;

	/** The active uniform buffers in their binding points. */
	activeUniformBuffers: (UniformGroup | undefined)[] = [];

	/** Uniform buffers used this frame. Used to know which of the activeUniformBuffer binding points to unbind. */
	uniformBufferBindingPointsUsedThisFrame: boolean[] = [];

	/** The active textures in their texture units. */
	activeTextures: (Texture | undefined)[] = [];

	/** Texture units used this frame. Used to know which of the activeTexture texture units to unbind. */
	textureUnitsUsedThisFrame: boolean[] = [];

	/** The times this state is used in a particular GL context. */
	useCount: number = 0;
}

export type UniformsFunction = ((shader: Shader) => void) | undefined;
