import { Color, type ColorReadOnly, Rect, type RectReadOnly, type Scene, Texture, UniformGroup, type Vec2D, type Vec2DReadOnly,
	type Vec3D, type Vec3DReadOnly } from '../internal';

/** A render stage. It either renders to the canvas or to textures. */
export class Stage {

	/** The constructor. */
	constructor(gl: WebGL2RenderingContext) {

		// Save the WebGL context.
		this._gl = gl;

		// Create the stage uniform group.
		this._uniformGroup = new UniformGroup(this._gl);

		// Enable the scissor test.
		this._gl.enable(this._gl.SCISSOR_TEST);
	}

	/** Destroys the stage. Called by Renderer. */
	zzDestroy() {

		// Decrement the textures use counts.
		for (const entry of this._outputColorTextures) {
			entry[1].zzOffsetNumStagesUsing(-1);
		}
		if (this._outputDepthStencilTexture) {
			this._outputDepthStencilTexture.zzOffsetNumStagesUsing(-1);
		}

		// Delete the frame buffer, if needed.
		if (this._frameBuffer) {
			this._gl.deleteFramebuffer(this._frameBuffer);
		}

		// Destroy the stage uniform group.
		this._uniformGroup.zzDestroy();
	}

	/** Gets the uniform group associated with this stage. */
	getUniformGroup(): UniformGroup {
		return this._uniformGroup;
	}

	/** Gets the bounds in pixel-space. It determines where in the canvas or textures the stage will be rendered. */
	getBounds(): RectReadOnly {
		return this._bounds;
	}

	/** Sets the bounds. */
	setBounds(bounds: RectReadOnly) {
		this._bounds.copy(bounds);

		// Call any bounds changed callback so that uniforms can be set properly.
		if (this._boundsChangedCallback !== undefined) {
			this._boundsChangedCallback(this);
		}
	}

	/** Gets the scene. */
	getScene(): Scene | undefined {
		return this._scene;
	}

	/** Sets the scene. */
	setScene(scene: Scene | undefined) {
		if (this._scene !== undefined) {
			this._scene.zzOffsetNumStagesUsing(-1);
		}
		this._scene = scene;
		if (this._scene !== undefined) {
			this._scene.zzOffsetNumStagesUsing(+1);
		}
	}

	/** Gets whether it will render to textures (true) or to the canvas (false). */
	getRenderToTexture(): boolean {
		return this._frameBuffer !== undefined;
	}

	/** Set whether it will render to textures (true) or to the canvas (false). Defaults to false. */
	setRenderToTexture(renderToTexture: boolean) {

		// If it's changing to be render to texture.
		if (!this._frameBuffer && renderToTexture) {
			// Create the frame buffer.
			this._frameBuffer = this._gl.createFramebuffer();
		}

		// If it's changing to be render to canvas.
		if (this._frameBuffer && !renderToTexture) {
			// Destroy the frame buffer.
			this._gl.deleteFramebuffer(this._frameBuffer);
			this._frameBuffer = undefined;
		}
	}

	/** Sets the color output texture for the *index*. */
	setOutputColorTexture(index: number, texture: Texture | undefined) {
		if (!this._frameBuffer) {
			throw new Error('You must first set the renderToTexture to true.');
		}
		if (texture && texture.getFormat() !== Texture.Format.RGBA) {
			throw new Error('The texture must be in the RGBA format.');
		}
		// Bind the texture to the color attachment of the frame buffer.
		this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, this._frameBuffer);
		this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.COLOR_ATTACHMENT0 + index, this._gl.TEXTURE_2D, texture ?? null, 0);
		if (this._gl.checkFramebufferStatus(this._gl.FRAMEBUFFER) !== this._gl.FRAMEBUFFER_COMPLETE) {
			throw new Error(`The color frame buffer at index ${index} is not supported by your system.`);
		}
		// Save the texture and its index.
		const oldTexture = this._outputColorTextures.get(index);
		if (oldTexture) {
			oldTexture.zzOffsetNumStagesUsing(-1);
			this._outputColorTextures.delete(index);
		}
		if (texture) {
			this._outputColorTextures.set(index, texture);
			texture.zzOffsetNumStagesUsing(+1);
		}
	}

	/** Sets the depth & stencil texture. The depth part is 24 bits and the stencil part is 8 bits. */
	setOutputDepthStencilTexture(texture: Texture | undefined) {
		if (this._frameBuffer === undefined) {
			throw new Error('You must first set the renderToTexture to true.');
		}
		if (texture && texture.getFormat() !== Texture.Format.DEPTH_STENCIL) {
			throw new Error('The texture must be in the DEPTH_STENCIL format.');
		}
		// Bind the texture to the depth & stencil attachment of the frame buffer.
		this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, this._frameBuffer);
		this._gl.framebufferTexture2D(this._gl.FRAMEBUFFER, this._gl.DEPTH_STENCIL_ATTACHMENT, this._gl.TEXTURE_2D, texture ?? null, 0);
		if (this._gl.checkFramebufferStatus(this._gl.FRAMEBUFFER) !== this._gl.FRAMEBUFFER_COMPLETE) {
			throw new Error('The depth & stencil frame buffer is not supported by your system.');
		}
		// Save the texture and its index.
		if (this._outputDepthStencilTexture !== undefined) {
			this._outputDepthStencilTexture.zzOffsetNumStagesUsing(-1);
		}
		this._outputDepthStencilTexture = texture;
		if (texture !== undefined) {
			texture.zzOffsetNumStagesUsing(+1);
		}
	}

	/** Sets the clear color. It does not clear if it is undefined. */
	setClearColor(color: ColorReadOnly | undefined) {
		if (color !== undefined) {
			if (this._clearColor === undefined) {
				this._clearColor = new Color();
			}
			this._clearColor.copy(color);
		}
		else {
			this._clearColor = undefined;
		}
	}

	/** Sets the clear depth. It does not clear if it is undefined. */
	setClearDepth(depth: number | undefined) {
		this._clearDepth = depth;
	}

	/** Sets the clear stencil. It does not clear if it is undefined. */
	setClearStencil(stencil: number | undefined) {
		this._clearStencil = stencil;
	}

	/** Gets the bounds changed callback. */
	getBoundsChangedCallback() {
		return this._boundsChangedCallback;
	}

	/** Sets the bounds changed callback. */
	setBoundsChangedCallback(boundsChangedCallback: ((stage: Stage) => void) | undefined) {
		this._boundsChangedCallback = boundsChangedCallback;
	}

	/** Renders the stage. Called by Renderer. */
	zzRender(renderHeight: number) {

		// Setup the frame buffer if necessary.
		if (this._frameBuffer) {
			this._gl.bindFramebuffer(this._gl.FRAMEBUFFER, this._frameBuffer);
		}

		// Setup the viewport.
		this._gl.viewport(this._bounds.min.x, renderHeight - (this._bounds.min.y + this._bounds.size.y), this._bounds.size.x, this._bounds.size.y);
		this._gl.scissor(this._bounds.min.x, renderHeight - (this._bounds.min.y + this._bounds.size.y), this._bounds.size.x, this._bounds.size.y);

		// Clear the buffer, if needed.
		let clearBitMask = 0;
		if (this._clearColor) {
			this._gl.clearColor(this._clearColor.r, this._clearColor.g, this._clearColor.b, this._clearColor.a);
			// eslint-disable-next-line no-bitwise
			clearBitMask |= this._gl.COLOR_BUFFER_BIT;
		}
		if (this._clearDepth !== undefined) {
			this._gl.clearDepth(this._clearDepth);
			// eslint-disable-next-line no-bitwise
			clearBitMask |= this._gl.DEPTH_BUFFER_BIT;
		}
		if (this._clearStencil !== undefined) {
			this._gl.clearStencil(this._clearStencil);
			// eslint-disable-next-line no-bitwise
			clearBitMask |= this._gl.STENCIL_BUFFER_BIT;
		}
		if (clearBitMask !== 0) {
			this._gl.clear(clearBitMask);
		}

		// Render the scene.
		this._scene?.zzRender(this._uniformGroup);
	}

	/** Converts an NDC position to a pixel position. It ignores the z component.
	 *  *Pixel space* is is [0, 0] in the top left corner and the [bounds.size.x - 1, bounds.size.y - 1] in the bottom
	 *  right corner. *NDC space* is [-1, -1] in the bottom left corner, [+1, +1] in the top right corner, with the
	 *  z component being +1 on the near plane and -1 on the far plane. */
	convertNdcToPixelPosition(pixelPosition: Vec2D, ndcPosition: Vec3DReadOnly) {
		pixelPosition.x = this._bounds.min.x + this._bounds.size.x * (1 + ndcPosition.x) / 2;
		pixelPosition.y = this._bounds.min.y + this._bounds.size.y * (1 - ndcPosition.y) / 2;
	}

	/** Converts a pixel position to an NDC position. The z component is set to 1, corresponding to the near plane.
	 *  *Pixel space* is is [0, 0] in the top left corner and the [bounds.size.x - 1, bounds.size.y - 1] in the bottom
	 *  right corner. *NDC space* is [-1, -1] in the bottom left corner, [+1, +1] in the top right corner, with the
	 *  z component being +1 on the near plane and -1 on the far plane. */
	convertPixelToNdcPosition(ndcPosition: Vec3D, pixelPosition: Vec2DReadOnly) {
		ndcPosition.x = 2 * (pixelPosition.x - this._bounds.min.x) / this._bounds.size.x - 1;
		ndcPosition.y = 1 - 2 * (pixelPosition.y - this._bounds.min.y) / this._bounds.size.y;
		ndcPosition.z = 1;
	}

	/** The bounds in pixel-space. It determines where in the canvas or textures the stage will be rendered. */
	private _bounds: Rect = new Rect();

	/**  The WebGL context. */
	private _gl: WebGL2RenderingContext;

	/** The stage-specific uniform block. */
	private _uniformGroup: UniformGroup;

	/** The output color textures (keyed by color attachment indices), if used. */
	private _outputColorTextures = new Map<number, Texture>();

	/** The output depth & stencil texture, if used. */
	private _outputDepthStencilTexture: Texture | undefined = undefined;

	/** The frame buffer. */
	private _frameBuffer: WebGLFramebuffer | undefined = undefined;

	/** The clear color. */
	private _clearColor: Color | undefined = new Color(0, 0, 0, 0);

	/** The clear depth. */
	private _clearDepth: number | undefined = 0;

	/** The clear stencil. */
	private _clearStencil: number | undefined = undefined;

	/** The scene. */
	private _scene: Scene | undefined = undefined;

	/** The bounds changed callback. */
	private _boundsChangedCallback: ((stage: Stage) => void) | undefined;
}
