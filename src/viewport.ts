import { Rect, type RectReadOnly, type Renderer, type Stage } from './internal';

export class Viewport {

	/** The div element for the viewport. */
	readonly div: HTMLDivElement;

	/** The render stage. */
	readonly stage: Stage;

	/** The bounds changed callback. */
	boundsChangedCallback: ((viewport: Viewport) => void) | undefined;

	/** The constructor. The div will dictate the position of the viewport. */
	constructor(renderer: Renderer, viewportsElement: HTMLDivElement) {

		// Save the renderer.
		this._renderer = renderer;

		// Create the render stage.
		this.stage = this._renderer.stages.create();

		// Create the div element.
		this.div = document.createElement('div');
		this.div.style.position = 'absolute';
		this.div.style.overflow = 'hidden';
		viewportsElement.appendChild(this.div);
	}

	/** Destroys the viewport. */
	zzDestroy() {

		// Destroy the div element.
		this.div.remove();

		// Destroy the stage.
		this._renderer.stages.destroy(this.stage);
	}

	/** Gets the aspect ratio as the *width* / *height*. */
	getAspectRatio(): number {
		return this.div.clientWidth / this.div.clientHeight;
	}

	/** Gets whether or not the viewport is enabled. */
	getEnabled(): boolean {
		return this._enabled;
	}

	/** Sets whether or not the viewport is enabled. */
	setEnabled(enabled: boolean) {
		this._enabled = enabled;
		this.div.style.display = this._enabled ? '' : 'none';
	}

	/** Gets the bounds. */
	getBounds(): RectReadOnly {
		return this.stage.getBounds();
	}

	/** Updates the bounds. Called by the engine. */
	zzUpdateBounds() {

		// Get the bounds of the div element.
		divBounds.set(this.div.offsetLeft + this.div.clientLeft, this.div.offsetTop + this.div.clientTop,
			this.div.clientWidth, this.div.clientHeight);
		divBounds.mult(divBounds, devicePixelRatio);

		// If the current bounds is different than the div bounds,
		if (!this.stage.getBounds().equals(divBounds)) {

			// Update the bounds of the viewport to reflect the div.
			this.stage.setBounds(divBounds);

			// Call the callback.
			this.boundsChangedCallback?.(this);
		}
	}

	/** The renderer. */
	private _renderer: Renderer;

	/** Whether or not the viewport is enabled. */
	private _enabled: boolean = true;
}

// A temporary bounds used for the div element bounds calculation.
const divBounds = new Rect();
