/** A sound. */
export class Sound {

	/** Constructs the sound. */
	constructor(audioContext: AudioContext) {

		// Save the audio context.
		this._audioContext = audioContext;
	}

	/** Destroys the sound. Called by Audio. */
	zzDestroy() {

		// If something is playing, stop it.
		if (this._audioNode) {
			this._audioNode.stop();
			this._audioNode.disconnect(this._audioContext.destination);
		}

		// Flag that the sound is destroyed.
		this._destroyed = true;
	}

	/** Gets the url that was loaded or is loading. */
	getUrl(): string {
		return this._url;
	}

	/** Loads the sound given the url. Returns a promise when it is loaded. */
	async load(url: string): Promise<void> {

		// Make sure we're not loading anything else.
		if (this._loading) {
			throw new Error(`The sound is already loading '${this._url}'. Wait until it is done before loading '${url}'.`);
		}

		// Signal that we're loading.
		this._loading = true;

		// Save the url.
		this._url = url;

		// Get the data.
		const response = await fetch(url);
		if (this._destroyed) {
			return;
		}

		// Get the data as an array buffer.
		const arrayBuffer = await response.arrayBuffer();
		if (this._destroyed as boolean) {
			return;
		}

		// Create the audio buffer from the array buffer.
		this._audioBuffer = await this._audioContext.decodeAudioData(arrayBuffer);
		if (this._destroyed as boolean) {
			return;
		}

		// Signal that we're no longer loading.
		this._loading = false;
	}

	/** Plays the sound. */
	play(loop: boolean = false) {

		// Don't do anything if we haven't loaded anything.
		if (!this._audioBuffer) {
			return;
		}

		// If there's already an audio node playing, stop it.
		if (this._audioNode) {
			this._audioNode.stop();
			this._audioNode.disconnect(this._audioContext.destination);
		}

		// Create a new audio node.
		this._audioNode = new AudioBufferSourceNode(this._audioContext);
		this._audioNode.buffer = this._audioBuffer;
		this._audioNode.connect(this._audioContext.destination);

		// Play it, possibly looping.
		if (loop) {
			this._audioNode.loop = true;
		}
		this._audioNode.start();

		// When it's done, remove it.
		this._audioNode.onended = () => {
			if (this._audioNode) {
				this._audioNode.disconnect(this._audioContext.destination);
				this._audioNode = undefined;
			}
		};
	}

	/** Stops the sound. */
	stop() {
		if (this._audioNode) {
			this._audioNode.stop();
			this._audioNode.disconnect(this._audioContext.destination);
			this._audioNode = undefined;
		}
	}

	/** The audio context. */
	private _audioContext: AudioContext;

	/** Whether or not the sound is destroyed. */
	private _destroyed: boolean = false;

	/** Whether or not the sound is loading. */
	private _loading: boolean = false;

	/** The url. */
	private _url: string = '';

	/** The audio buffer. */
	private _audioBuffer: AudioBuffer | undefined;

	/** The currently playing audio node. */
	private _audioNode: AudioBufferSourceNode | undefined;
}
