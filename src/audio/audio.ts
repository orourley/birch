import {
	Items,
	Sound
} from '../internal';

/** The audio system. */
export class Audio {

	/** The sounds. */
	readonly sounds = new Items(
		() => new Sound(this._audioContext),
		(sound: Sound) => { sound.zzDestroy(); });

	/** Destroys the audio system. */
	zzDestroy() {
		this.sounds.zzDestroy();
		void this._audioContext.close();
	}

	/** The audio context. */
	private _audioContext = new AudioContext();
}
