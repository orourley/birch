# Are Getters and Setters Bad?

Firstly, I would like to either use lots of getters and setters, or none at all, prefering get*() and set*() calls.

One of my big worries about setters is this:

```
	/** Gets the bounds in pixel-space. */
	get bounds(): RectReadOnly {
		return this._bounds;
	}

	/** Sets the bounds in pixel-space. */
	set bounds(bounds: RectReadOnly) {
		this._bounds.copy(bounds);
	}
```

The problem is that when I use it:

```
viewport.bounds = newBounds;
```

It appears that I am assigning newBounds to bounds, as in if I modify `newBounds.min.x`, it will change the `viewport.bounds.min.x`, as they are now the same object. What's hidden in the setter is that a copy is occuring.

This is not intuitive and breaks what would be expected. It feels more clear if I do this:

```
	/** Gets the bounds in pixel-space. */
	getBounds(): RectReadOnly {
		return this._bounds;
	}

	/** Sets the bounds in pixel-space. */
	setBounds(bounds: RectReadOnly) {
		this._bounds.copy(bounds);
	}

...

viewport.setBounds(newBounds);
```

Now there is a more explicit knowledge that `viewport.setBounds()` uses newBounds, rather than is just assigned it.

**Conclusion**: I will not use any getters or setters, except in some of the mathematical classes.
