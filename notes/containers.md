# Containers

For different objects created by Birch, how do I handle them? Here are some of the objects:

* Sound
* Texture
* UniformGroup
* Mesh
* Shader
* Model
* Stage
* Scene
* Viewport
* World
* System
* Entity
* Component
* Query

It's a big list, and they have different requirements.

## Resources and Names

Some of the above can be considered resources, in that they are loaded via urls:

* Sound
* Texture
* Mesh (sometimes)
* Shader

Should these have names associated with them, or should the factory functions create the object, which an app can then name?

A common scenario is that most of the resources will be loaded at the beginning of a level, and then used by the level logic. Does this mean I should have names built into birch? If the different pieces of Birch are directly accessible by the different pieces of app, there's nothing to stop a part from creating a texture without a name, if Birch doesn't use names. Maybe this is okay.

If I have names, then every resource will be forced to have a name, including textures that are used for stage outputs.

**Conclusion**: I think I'll opt for having no names. It allows for maximum flexibility and imposes no requirements on an app. If an app wants to name resources, they can create a separate name-to-resource mapping.
