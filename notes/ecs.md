# Entity Component Systems

Each property in a component will be one of two types: poll-based or event-based.

* A poll-based property is just a raw property, such as a Vector3.
* An event-based property is a callback list of a given type (such as Vector3). When the property is changed, the callbacks are triggered. Systems subscribe to the component via an event callback and immediately act on the changed data.

Each system has a few ways of running code.

* It registers with the world a component type list, with a callback that returns true or false. When a component is added, the world looks through those lists, and any systems connected with that component type have their callback called. If it returns true, the entity of the component is added (or kept) in the system. If it returns false, that entity is removed from the system. The system only needs to provide the boolean callback, and the world can take care of the rest.
* When an entity is added, a system can subscribe via a callback to any of its components for change events. When a subscribed component's property is changed, the system's callback is run immediately. When an entity is removed, the system must be sure to unsubscribe from the appropriate components.
* Each system can have an update function which runs every frame. It can do anything, including poll components properties for changes, doing updates within the system as necessary.

Perhaps first two bullet points can be automated a bit. The only thing that really needs to be custom is what an event does when it receives a component change event, or in the update function.

## One Component Type Per Entity or Multiple?

This is a question I keep coming back to. For some components it's a no brainer, like Frame, of which there can be only one. However for other types, such as Model or Light, it becomes more complicated. Clearly an entity can have more than one model. For instance Saturn has it's spheroid as a Model and rings as another Model, both relying on the same Frame.

There are two choices:

* Have multiple components of the same type in a entity.
* Have components like Model actually be arrays of data.

How does option 1 work? It would have to be a mapping from component types to an array or set of components of each type. This is effectively like option 2, except every component type can have multiple instances.

How does option 1 interact with systems and events? When a LightRender system hears that a Light has been added, it has a callback to subscribe to any changed events, such as changing the scene stage's lighting uniforms. If there are multiple Lights, it would go through each Light in the array/set and adjust the uniforms.

However, what if an entity has two Frames and one light? What will the LightRender system do then? It must choose by some heuristic, which Frame to use. It seems better to have option 2, with one component type per entity, with certain components like Model or Light being arrays/sets.

**Conclusion**: It makes sense to have one component type per entity, as in most cases that's all that is needed. It will make the logic for querying in most systems simpler. For components types that might have multiple, we will just have an array of component data.

## Referring To Other Components

Whether we choose multiple or single component types per entity, how does a component refer to a particular Light in the light array? For instance, perhaps there is a Trigger component that says, "Change the color of a Light when triggered." The Trigger component needs a reference to the right Light. Could each component (including in arrays) have a unique id? But a Trigger that references an entity's Frame doesn't need a unique id; it just needs the entity name. Perhaps any component reference could be 'entity-name[.id]'.

**Conclusion**: A system will have a component getter for getting data on any component. It will be:

```
getComponent<T extends Component>(entityName: string, ComponentType: T, id?: string): T | undefined {
	const entity = this.world.entities.get(entityName);
	if (!entity) { return undefined; }
	const component = entity.components.get(ComponentType);
	if (id !== undefined) {
		if (component instanceof FastMap<ComponentType>) {
			component.get(id)
		}
		else {
			throw new Error('Only component that is a FastMap can have an id.');
		}
	}
}
```
