#

World
	entities: Map<string, Map<ComponentType, Component>>
  components: Map<ComponentType, Component[]>
	systems: System[]

Component
	No name or type, just the properties.

Entity
	Doesn't exist. The world just has Map<string, Map<string, Component>>.

# NO ECS

If I'm having event triggering code in components anyway, why do I need to subscribe to ECS? As I see it, ECS provides two main benefits:

* Uses components and not OOP to become much more flexible.
* Code (systems) only use the components they need and don't iterate over every component every frame.

I'm not too concerned about the cache miss/hits, as I don't intend to have so many entities on the screen that this will be a significant issue. In 99% of cases, systems will be operating on multiple types of components and will be jumping around in the cache anyway.

So, then what is the alternative to ECS? Keeping the data in components (rather than OOP model) is definitely a good idea. I think components could also be more functional, so that users could do things like set materials, query different functions of components, etc. This is similar to how Pioneer works with components.

However, I would like to avoid running an update on every component every frame. This means it would be good to have systems that run the overall game loop. They can use the extra code within components to make their functionality easier.

# More on Event Handling

I'm still struggling with event handling.

I've created a system whereby doing "setPosition" on a Frame component triggers an changed event. Those events are stored in queues to be later read by systems.

The problem with this is that components are supposed to be only data and super simple. There is also a lot of complexity added with an event system. An event system also makes code more difficult to debug, since events are going everywhere.

I would like to remove events from the components, at least. How then could I handle the Frame component property changes, for instance? Some Frame components change every frame, and some hardly at all.

If it is handled in the system, I could make an extra flag in the Frame component "dynamic", and if it is off, the system could send a modified event.

## Event Handling Options:

These are the possible ways of handling events that I see:

* Component properties are getters/setters. When a component is set, it triggers an event which gets passed to subscribed systems.
* Systems can add component which are events. Other systems subscribe to the component event types and do stuff when they are added.
* Systems trigger events whenever they modify properties.
* Systems call callback functions which run other system functions.

The problems with all but the first option is that if a system needs to know when a component has changed, it must rely on the other system remembering to call the right event. With the first option it doesn't, as it just needs to call setPosition, which will automatically trigger the changed event. I can't think of any other way around it other than to use the first option.

# More thought out conclusions based on observations.

Each system maintains a list of component bags. A component bag is a tuple of components, one of each type. Whenever a component of any of those types is created or destroyed, the world calls that system's componentChanged(component, added/removed) function, which goes through the entity's components and updates its list of (component1, component2, etc) entries. It may add a new component bag if conditions are finally met, or remove a component bag if conditions are no longer met.

Most components will trigger events when they are changed. Systems can subscribe to the components. Some components that change most frames will not trigger events, and the associated systems can just poll them. An example would be StaticPosition (event-based), DynamicPosition (poll-based). When a component is changed, it adds itself to each subscribed system's event list (use a non-GC method). This event list is processed when the system next runs.

For poll-based properties, the component can just have a regular variable, and for event-based properties, it can be a get/set function, since it needs to send an event when the property changes.

Question: When a component is changed, should its changed-event include the property itself? If so, can systems subscribe to individual properties? This feels to complicated, and maybe if this scenario occurs, the component should be split into multiple components. So the answer is no.

# Observations Based on Examples

* Systems need to know when components are created.
* Systems need to know when components are modified.
* Many components are not modified every frame, so that going through each component of a given type to see what has changed, maybe via a flag, is inefficient. There must be a list of changed components of a given type that can be quickly iterated over.

# Examples

## Quad Tree System

When a frame component is created, it needs be inserted into the quad tree.
When a frame component is modified, it needs to be updated within the quad tree.

## Frame System

When a frame component is modified, the local-to-world component needs to be updated.

## Camera System

When a camera component is modified, the local-to-ndc component needs to be updated.
